package com.winmax.transfer


import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.model.MapModel
import kotlinx.android.synthetic.main.activity_trackin_maps.*

class TrackinMapsActivity : AppCompatActivity() {

    var value = Singleton.instance.addressvalue
    private var addressloc = value.set(0, MapModel()).address
    private val callback = OnMapReadyCallback { googleMap ->
        val address = getLocationFromAddress(this, addressloc)
        googleMap.addMarker(MarkerOptions().position(address!!).title(addressloc))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(address))
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(address, 15f))

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trackin_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(callback)
        fab.setOnClickListener {
            onBackPressed()
        }
    }


  private  fun getLocationFromAddress(context: Context?, strAddress: String?): LatLng? {
        val coder = Geocoder(context)
        val address: List<Address>?
        var p1: LatLng? = null
        try {
            address = coder.getFromLocationName(strAddress, 5)
            if (address == null) {
                return null
            }
            val location: Address = address[0]
            location.latitude
            location.longitude
            p1 = LatLng(location.latitude, location.longitude)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return p1
    }
}