package com.winmax.transfer.constant;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class Preferences {
    public static Preferences mPreferences = new Preferences();
    public static SharedPreferences mSharedPreferences;
    private String defaultString = null;

    public Preferences() {
    }

    public static Preferences getInstance() {
        return mPreferences;
    }

    public static void createOrOpenPreferences(Context mContext, String prefName) {
        mSharedPreferences = mContext.getSharedPreferences(prefName, Context.MODE_PRIVATE);
    }

    public void setValue(String key, String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getStringValue(String key) {
        return mSharedPreferences.getString(key, defaultString);
    }


    public boolean getBooleanValue(String key) {
        return mSharedPreferences.getBoolean(key, false);
    }

    public void setBooleanValue(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void setHashSet(String key, LinkedHashSet<String> contact) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putStringSet(key, contact);
        editor.apply();
    }

    /*public void setFirstTimeLaunch(boolean isFirstTime) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.apply();
    }*/

    public Set<String> getHashSet(String key) {
        return mSharedPreferences.getStringSet(key, null);
    }

   /* public void setArrayList(String key, ArrayList<String> contact) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        try {
            editor.putString(key, ObjectSerializer.serialize(contact));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    /*public ArrayList<String> getArrayList(String key) {
        ArrayList<String> arrayList = null;
        try {
            arrayList = (ArrayList<String>) ObjectSerializer.deserialize(mSharedPreferences.getString(key, ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }*/

    public Map getAll() {
        return mSharedPreferences.getAll();

    }

    public void ClearPreference() {




    }
}
