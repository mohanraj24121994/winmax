package com.winmax.transfer.constant


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.winmax.transfer.MainActivity
import com.winmax.transfer.R
import com.winmax.transfer.SelectWareHouseScreen
import java.util.regex.Matcher
import java.util.regex.Pattern

object Constants {


    const  val isDeleteBtnPressed = false

    const val GetWareHouses = "GetWareHouses"
    const val authenticateUser = "AuthenticateUser"


    const val companyCodeConst ="CompanyCode"
    const val userNameConst ="UserName"
    const val passwordConst ="Password"
    const val webUrlConst ="WebUrl"
    const val versionConst = "Version"
    const val pdfURL = "pdfURL"

    const val userNameAuthConst ="UserNameAuth"
    const val passwordAuthConst ="PasswordAuth"
    const val adminPassword ="AdminPassword"

    const val RESPONSE_CODE_0 = "0"
    const val RESPONSE_CODE_5 = "5"

    const val REQUEST_CODE_SCANNER = 888
    const val REQUEST_CODE_EXIT = 999

    const val EXTRA_KEY_DATA_ARTICLES = "KEY_DATA_ARTICLES"
    const val DIRECTORY_NAME = "Winmax"

    const val EXTRA_KEY_SCREEN_FOR = "KEY_SCREEN_FOR"


    fun  isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }
    fun internetCheck(context: Context): Boolean {
        return isNetworkAvailable(context)
    }
    fun shortToast(context: Context,message: String) {
        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show()
    }
    fun longToast(context: Context,message: String) {
        Toast.makeText(context, "" + message, Toast.LENGTH_LONG).show()
    }

    fun isValidPassword(password: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val passwordPattern = "^(?=.*\\d)(?=.*[@\$!%*#?&])[A-Za-z\\d@\$!%*#?&]{8,}\$"
        //val passwordPattern = "^[a-zA-Z0-9]{8,}\$"
        pattern = Pattern.compile(passwordPattern)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }


    fun goToActivityFinish(context: Context,activity: MainActivity){
        val to = Intent(context, activity::class.java)
        startActivity(context,to,null)
        activity.overridePendingTransition(R.anim.enter, R.anim.exit);
        activity.finish()
    }

    fun goToActivity(context: Context,activity: Activity){
        val to = Intent(context, activity::class.java)
        startActivity(context,to,null)
        activity.overridePendingTransition(R.anim.enter, R.anim.exit);

    }


    val COLOR_BLUE = Color.parseColor("#4285F4")
    val COLOR_RED = Color.parseColor("#DB4437")
    val COLOR_YELLOW = Color.parseColor("#F4B400")
    val COLOR_GREEN = Color.parseColor("#0F9D58")

    const val CIRCLE_COUNT = 4
    const val DEFAULT_CIRCLE_RADIUS = 20f
    const val DEFAULT_CIRCLE_MARGIN = 20f
    const val DEFAULT_ANIM_DISTANCE = 50f
    const val DEFAULT_ANIM_DURATION = 500L
    const val DEFAULT_ANIM_DELAY = 150L
    const val DEFAULT_ANIM_INTERPOLATOR = 0

    val REGISTRATION_COMPLETE = "registrationComplete"
    val PUSH_NOTIFICATION = "pushNotification"
    val NOTIFICATION_ID = 100
    val NOTIFICATION_ID_BIG_IMAGE = 101
    val SHARED_PREF_FIREBASE = "firebaseRegId"
    val TOPIC_GLOBAL = "global"

    const val noStatus = - 1
    const val actualImage = 0
    const val thumbImage = 1
    const val cropImage = 2
    const val completed = 3
    const val signature = 4
    const val audio = 5
    const val damageImage = 6
}