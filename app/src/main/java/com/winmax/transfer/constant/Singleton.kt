package com.winmax.transfer.constant

import android.annotation.SuppressLint
import com.winmax.transfer.model.MapModel
import com.winmax.transfer.model.InvoiceListModel
import com.winmax.transfer.model.ItemListModel
import com.winmax.transfer.model.createArticle.request.ArticleDetail
import com.winmax.transfer.model.createArticle.request.CreateArticleRequest
import com.winmax.transfer.model.getWarehouse.Warehouse
import com.winmax.transfer.model.getentitiesbyphone.Address
import com.winmax.transfer.model.getentitydescription.GetEntityDescription
import com.winmax.transfer.model.payment.Entity
import com.winmax.transfer.model.tracking.TrackingListResponse

class Singleton {
    var warehouseData = ArrayList<Warehouse>()
    var createDocModel: ArrayList<CreateArticleRequest> = ArrayList()
    val itemListSingleton: ArrayList<ItemListModel> = ArrayList()
    val invoiceListSingleton: ArrayList<InvoiceListModel> = ArrayList()
    val articleListSingleton: ArrayList<ArticleDetail> = ArrayList()
    val trackingListSingleton: ArrayList<TrackingListResponse> = ArrayList()
    val showAddress: ArrayList<Address> = ArrayList()
    val articleListRequestSingleton: ArrayList<ArticleDetail> = ArrayList()
    val articleListSingletonInventory: ArrayList<ArticleDetail> = ArrayList()
    var entityData = ArrayList<Entity>()
    var entityDescription = ArrayList<GetEntityDescription>()
    var addressvalue: ArrayList<MapModel> = ArrayList()

    companion object {

        @SuppressLint("StaticFieldLeak")
        private var mInstance: Singleton? = null

        val instance: Singleton
            get() {
                if (mInstance == null) {
                    mInstance = Singleton()
                }
                return mInstance!!
            }
    }
}