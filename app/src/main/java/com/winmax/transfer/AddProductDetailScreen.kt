package com.winmax.transfer

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.zxing.integration.android.IntentIntegrator
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.model.ItemListModel
import com.winmax.transfer.model.createArticle.request.ArticleDetail
import com.winmax.transfer.model.createArticle.request.CreateArticleRequest
import com.winmax.transfer.model.createArticle.request.Credential
import com.winmax.transfer.model.createArticle.request.DocumentDetail
import com.winmax.transfer.model.getArticals.response.ArticleResponse
import com.winmax.transfer.model.getArticals.response.GetArticlesResult
import com.winmax.transfer.model.webserviceConnection.WebTestRequest
import com.winmax.transfer.utils.AnyOrientationCaptureActivity
import com.winmax.transfer.utils.toast
import com.winmax.transfer.webservices.ApiClient
import kotlinx.android.synthetic.main.activity_add_product_detail_screen.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AddProductDetailScreen : AppCompatActivity() , KeyBoardListener , View.OnClickListener {

    private var logTag = "LOG_AddProductDetailScreen"
    var keyboard: MyKeyboard? = null
    var mContext: Context? = null
    var articalRes: GetArticlesResult? = null
    var sendingCode: String? = null
    var receivingCode: String? = null
    var l: LinearLayout? = null
    var whichFocus = "itemCode"
    var apiCalled = false
    var isKeyboardShowing = false
    private var mPreviousHeight = 0
    var delay: Long = 1000
    var handler = Handler()
    var last_text_edit: Long = 0
    var isClickKeyBoard: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_product_detail_screen)
        mContext = this
        keyboard = findViewById(R.id.keyboardview)
        textViewToolBarTitle.text = intent.extras?.getString(Constants.EXTRA_KEY_SCREEN_FOR)
        hideKeyboard()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        onClickListener()

        val inputFinishChecker = Runnable {
            if (System.currentTimeMillis() > last_text_edit + delay - 500) {
                if (whichFocus == "itemCode") {
                    sendingCode = PrefManager(mContext).getStringValue("sendingCode")
                    receivingCode = PrefManager(mContext).getStringValue("receivingCode")
                    if (etItemCode.text.toString().trim().isNotEmpty()) {
                        callApi(etItemCode.text.toString().trim())
                    }
                }
            }
        }

        etItemCode.post {
            etItemCode.requestFocus()
        }

        etItemCode.setOnFocusChangeListener { _ , b ->
            if (b) {
                setFocusToItemCode()
            }
        }

        etQty.setOnFocusChangeListener { view , b ->
            if (b) {

              //  etQty.inputType = InputType.TYPE_CLASS_NUMBER
                etQty.setTextIsSelectable(true)
                etQty.showSoftInputOnFocus = false //no response on keyboard touch
                etQty.requestFocus()

                val ic: InputConnection = etQty.onCreateInputConnection(EditorInfo()) !!
                keyboard?.setInputConnection(ic)

                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(etQty.windowToken , 0)
                whichFocus = "itemQty"
                hideKeyboard()
            }
        }

        etItemCode.setOnEditorActionListener { v , actionId , event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                etItemCode.inputType = InputType.TYPE_CLASS_NUMBER
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(etItemCode.getWindowToken() , 0)
                true
            } else if (actionId == EditorInfo.IME_NULL) {
                etItemCode.inputType = InputType.TYPE_CLASS_NUMBER
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(etItemCode.getWindowToken() , 0)
                true
            } else false
        }

        etItemCode.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(charSequence: CharSequence , i: Int , i1: Int , i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence , i: Int , i1: Int , i2: Int) {
                handler.removeCallbacks(inputFinishChecker)
            }

            override fun afterTextChanged(editable: Editable) {
                if (editable.length > 0 && isClickKeyBoard) {
                    last_text_edit = System.currentTimeMillis();
                    handler.postDelayed(inputFinishChecker , delay);
                    PrefManager(mContext).setValue("ItemCode" , editable.toString())
                } else {
                    PrefManager(mContext).setValue("ItemCode" , editable.toString())
                }
            }
        })

        etQty.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.length > 0) {
                    PrefManager(mContext).setValue("Quantity" , s.toString())
                } else {
                    PrefManager(mContext).setValue("Quantity" , s.toString())
                }
            }

            override fun beforeTextChanged(s: CharSequence? , start: Int , count: Int , after: Int) {
            }

            override fun onTextChanged(s: CharSequence? , start: Int , before: Int , count: Int) {
            }
        })

        etItemDescription.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.length > 0) {
                    PrefManager(mContext).setValue("Description" , s.toString())
                } else {
                    PrefManager(mContext).setValue("Description" , s.toString())
                }
            }

            override fun beforeTextChanged(s: CharSequence? , start: Int , count: Int , after: Int) {
            }

            override fun onTextChanged(s: CharSequence? , start: Int , before: Int , count: Int) {
            }
        })

        setVersion()
    }


    private fun setFocusToItemCode() {
        etItemCode.setRawInputType(InputType.TYPE_CLASS_NUMBER)
        etItemCode.setTextIsSelectable(true)
        etItemCode.showSoftInputOnFocus = false; //no response on keyboard touch
        etItemCode.requestFocus()

        val ic: InputConnection = etItemCode.onCreateInputConnection(EditorInfo()) !!
        keyboard?.setInputConnection(ic)

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(etItemCode.windowToken , 0)
        whichFocus = "itemCode"
        hideKeyboard()
    }

    private fun onClickListener() {
        btnShowItems.setOnClickListener(this)
        ivBack.setOnClickListener(this)
        tvLogout.setOnClickListener(this)
        btnAddProduct.setOnClickListener(this)
        btnUndo.setOnClickListener(this)

        if (! apiCalled) {
            btnAddProduct.alpha = 0.5f
            btnAddProduct.isEnabled = false

        } else {
            btnAddProduct.alpha = 1f
            btnAddProduct.isEnabled = true
        }
    }

    private fun setVersion() {
        tvVersion.text = PrefManager(applicationContext).getStringValue(Constants.versionConst)
    }

    private fun callApi(itemCode: String) {
        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = WebTestRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)

        val call = ApiClient.getService().getArticle(itemCode , request)

        call.enqueue(object : Callback<ArticleResponse> {
            override fun onResponse(call: Call<ArticleResponse> , response: Response<ArticleResponse>) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getArticlesResult?.code.equals("0")) {
                        Constants.shortToast(mContext !! , getString(R.string.success))

                        etItemDescription.setText(response.body()?.getArticlesResult?.articles?.article?.designation.toString())
                        articalRes = response.body()?.getArticlesResult
                        etQty.requestFocus()
                        apiCalled = true
                        btnAddProduct.isEnabled = true
                        btnAddProduct.alpha = 1f
                        isClickKeyBoard = false
                        PrefManager(mContext).setValue("Description" , response.body()?.getArticlesResult?.articles?.article?.designation.toString())
                    } else if (response.body()?.getArticlesResult?.code.equals("5")) {

                        Constants.shortToast(mContext !! , "Invalid Item Code!!")
                        etItemDescription.setText("")
                    }
                } else {
                    Constants.shortToast(mContext !! , getString(R.string.failure))
                }
            }

            override fun onFailure(call: Call<ArticleResponse> , t: Throwable) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
            }
        })
    }

    private fun prepareItemListData() {
        circle_view.visibility = View.VISIBLE

        Handler().postDelayed(Runnable {
            circle_view.visibility = View.GONE
            Constants.longToast(mContext !! , "Product added.")
        } , 2000)

        sendingCode = PrefManager(mContext).getStringValue("sendingCode")
        receivingCode = PrefManager(mContext).getStringValue("receivingCode")

        Singleton.instance.itemListSingleton.add(ItemListModel(articalRes?.articles?.article?.articleCode !! , articalRes?.articles?.article?.designation !! , etQty.text.toString().trim()))
        Singleton.instance.createDocModel.add(generateCreateArticleRequest())

        val screenFrom = intent.extras?.getString(Constants.EXTRA_KEY_SCREEN_FOR)
        if (screenFrom.equals(getString(R.string.inventory))) {
            Singleton.instance.articleListSingletonInventory.add(generateArtDetails())
        } else {
            Singleton.instance.articleListSingleton.add(generateArtDetails())
        }

        //resetView
        resetView()
    }

    private fun generateCreateArticleRequest(): CreateArticleRequest {
        val createArticleRequest = CreateArticleRequest()

        val credential = Credential()
        credential.setCompanyCode(PrefManager(mContext).getStringValue(Constants.companyCodeConst))
        credential.setUserLogin(PrefManager(mContext).getStringValue(Constants.userNameConst))
        credential.setUserPassword(PrefManager(mContext).getStringValue(Constants.passwordConst))
        credential.setWebServiceURL(PrefManager(mContext).getStringValue(Constants.webUrlConst))

        val documentDetail = DocumentDetail()
        documentDetail.setSourceWarehouseCode(sendingCode)
        documentDetail.setTargetWarehouseCode(receivingCode)
        documentDetail.setDocumentTypeCode("TO")
        documentDetail.setEntityCode(receivingCode)

        createArticleRequest.setCredential(credential)
        createArticleRequest.setDocumentDetail(documentDetail)

        return createArticleRequest
    }

    private fun generateArtDetails(): ArticleDetail {
        val articleDetail = ArticleDetail()
        articleDetail.articleCode = articalRes?.articles?.article?.articleCode
        articleDetail.articleDesignation = articalRes?.articles?.article?.designation
        articleDetail.quantity = etQty.text.toString().trim()
        return articleDetail
    }

    private fun resetView() {
        etItemCode.setText("")
        etItemDescription.setText("")
        etQty.setText("")
        etItemCode.requestFocus()
    }

    override fun onResume() {
        super.onResume()
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        val itemCode = PrefManager(mContext).getStringValue("ItemCode")
        val description = PrefManager(mContext).getStringValue("Description")
        val quantity = PrefManager(mContext).getStringValue("Quantity")

        if (! itemCode.isNullOrEmpty()) {
            etItemCode.setText(itemCode)
            callApi(itemCode)
        } else {
            etItemCode.setText("")
        }

        if (! description.isNullOrEmpty()) {
            etItemDescription.setText(description)
            btnAddProduct.isEnabled = true
            btnAddProduct.alpha = 1f
        } else {
            etItemDescription.setText("")
        }

        if (! itemCode.isNullOrEmpty() && ! description.isNullOrEmpty() && quantity.isNullOrEmpty()) {
            etQty.requestFocus()
        }

        if (! quantity.isNullOrEmpty()) {
            etQty.setText(quantity)
            etQty.setSelection(quantity.length)
        } else {
            etQty.setText("")
        }

//        apiCalled = true
    }

    private fun exitByBackKey() {
        val itemCode = PrefManager(mContext).getStringValue("ItemCode")
        val description = PrefManager(mContext).getStringValue("Description")
        val quantity = PrefManager(mContext).getStringValue("Quantity")

        if (! itemCode.isNullOrEmpty() || ! description.isNullOrEmpty() || ! quantity.isNullOrEmpty()) {
            val alertbox: AlertDialog = AlertDialog.Builder(this)
                    .setTitle("Info")
                    .setMessage("Do you want to discard the changes?")
                    .setPositiveButton("Yes") { _ , _ ->
                        PrefManager(mContext).setValue("ItemCode" , "")
                        PrefManager(mContext).setValue("Description" , "")
                        PrefManager(mContext).setValue("Quantity" , "")
                        finish()
                    }
                    .setNegativeButton("No") { _ , _ ->
                        finish()
                    }
                    .show()
        } else {
            finish()
        }
    }

    override fun onKeyDown(keyCode: Int , event: KeyEvent?): Boolean {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitByBackKey()
            return true
        }
        return super.onKeyDown(keyCode , event)
    }

    private fun hideKeyboard() {
        val imm: InputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = this.currentFocus
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken , 0)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnShowItems -> {
                onClickButtonShowItems()
            }
            R.id.ivBack -> {
                onClickButtonBack()
            }
            R.id.tvLogout -> {
                onClickButtonLogout()
            }
            R.id.btnAddProduct -> {
                onClickButtonAddProducts()
            }
            R.id.btnUndo -> {
                onClickButtonUndo()
            }
        }
    }

    //onClick: button showItem
    private fun onClickButtonShowItems() {
        val screenFrom = intent.extras?.getString(Constants.EXTRA_KEY_SCREEN_FOR)
        if (screenFrom.equals(getString(R.string.inventory))) {
            if (Singleton.instance.articleListSingletonInventory.size > 0) {
                val intent = Intent(this , ItemListScreen::class.java)
                val valueScreenFrom = textViewToolBarTitle.text
                intent.putExtra(Constants.EXTRA_KEY_SCREEN_FOR , valueScreenFrom)
                startActivity(intent);
                overridePendingTransition(R.anim.enter , R.anim.exit)
            } else {
                Constants.shortToast(mContext !! , "Please add product")
            }
        } else {//transfer
            if (Singleton.instance.articleListSingleton.size > 0) {
                val intent = Intent(this , ItemListScreen::class.java)
                val valueScreenFrom = textViewToolBarTitle.text
                intent.putExtra(Constants.EXTRA_KEY_SCREEN_FOR , valueScreenFrom)
                startActivity(intent);
                overridePendingTransition(R.anim.enter , R.anim.exit)
            } else {
                Constants.shortToast(mContext !! , "Please add product")
            }
        }
    }

    private var doubleBackToExitPressedOnce = false

    //onClick: button back
    private fun onClickButtonBack() {
        /*finish()
        overridePendingTransition(R.anim.enter , R.anim.exit)*/
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }

    //onClick: button logout
    private fun onClickButtonLogout() {
        PrefManager(mContext).setBooleanValue("isLoggedIn" , false)
        PrefManager(mContext).isFirstTimeLaunch = false
        val to = Intent(this , MainActivity::class.java)
        startActivity(to);
        overridePendingTransition(R.anim.enter , R.anim.exit)
        finish()
    }

    //onClick: button add products
    private fun onClickButtonAddProducts() {
        if (apiCalled) {
            if (etItemCode.text.toString().trim().isEmpty()) {
                Constants.shortToast(mContext !! , "Please enter Item Code")
            } else if (etQty.text.toString().trim().isEmpty()) {
                Constants.shortToast(mContext !! , "Please enter Item Quantity")
            } else {
                if (etQty.text.toString().trim() != "0") {
                    prepareItemListData()
                } else {
                    Constants.shortToast(mContext !! , "Item Quantity cannot be 0!!")
                }

            }
        }
    }

    //onClick: button undo
    private fun onClickButtonUndo() {
        val screenFrom = intent.extras?.getString(Constants.EXTRA_KEY_SCREEN_FOR)
        if (screenFrom.equals(getString(R.string.inventory))) {
            if (Singleton.instance.articleListSingletonInventory.size > 0) {
                Singleton.instance.articleListSingletonInventory.removeAt(Singleton.instance.articleListSingletonInventory.size - 1)
                Constants.shortToast(mContext !! , "Removed last added item")
            } else {
                Constants.shortToast(mContext !! , "No product added!!")
            }

        } else {
            if (Singleton.instance.articleListSingleton.size > 0) {
                Singleton.instance.articleListSingleton.removeAt(Singleton.instance.articleListSingleton.size - 1)
                Constants.shortToast(mContext !! , "Removed last added item")
            } else {
                Constants.shortToast(mContext !! , "No product added!!")
            }
        }

    }

    //Keyboard: action delete
    override fun clickDelete() {

        if (whichFocus == "itemCode") {
            val length: Int = etItemCode.text !!.toString().trim().length
            if (length > 0) {
                etItemCode.text !!.delete(length - 1 , length)
            }
            etItemDescription.setText("")
        } else {
            val length: Int = etQty.text !!.toString().trim().length
            if (length > 0) {
                etQty.text !!.delete(length - 1 , length)
            }
        }

    }

    override fun clickKeyDone() {
        if (whichFocus == "itemCode") {
            sendingCode = PrefManager(mContext).getStringValue("sendingCode")
            receivingCode = PrefManager(mContext).getStringValue("receivingCode")
            if (etItemCode.text.toString().trim().length > 0) {
                callApi(etItemCode.text.toString().trim())
            } else {
                Constants.shortToast(mContext !! , "Please enter Item Code")
            }

        } else {
            if (apiCalled) {
                if (etItemCode.text.toString().trim().isEmpty()) {
                    Constants.shortToast(mContext !! , "Please enter Item Code")
                } else if (etQty.text.toString().trim().isEmpty()) {
                    Constants.shortToast(mContext !! , "Please enter Item Quantity")
                } else {
                    if (etQty.text.toString().trim() != "0") {
                        prepareItemListData()
                    } else {
                        Constants.shortToast(mContext !! , "Item Quantity cannot be 0!!")
                    }

                }
            }
        }
    }

    //keyboard: action done
    override fun clickDone() {
        if (whichFocus == "itemCode") {
            sendingCode = PrefManager(mContext).getStringValue("sendingCode")
            receivingCode = PrefManager(mContext).getStringValue("receivingCode")
            if (etItemCode.text.toString().trim().length > 0) {
                callApi(etItemCode.text.toString().trim())
            } else {
                Constants.shortToast(mContext !! , "Please enter Item Code")
            }

        } else {
            if (apiCalled) {
                if (etItemCode.text.toString().trim().isEmpty()) {
                    Constants.shortToast(mContext !! , "Please enter Item Code")
                } else if (etQty.text.toString().trim().isEmpty()) {
                    Constants.shortToast(mContext !! , "Please enter Item Quantity")
                } else {
                    if (etQty.text.toString().trim() != "0") {
                        prepareItemListData()
                    } else {
                        Constants.shortToast(mContext !! , "Item Quantity cannot be 0!!")
                    }

                }
            }
        }

    }

    //keyboard: action scan
    override fun clickScan() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (whichFocus == "itemCode") {
            etItemCode.inputType = InputType.TYPE_CLASS_TEXT
            imm.showSoftInput(etItemCode , InputMethodManager.SHOW_FORCED)

            val integrator = IntentIntegrator(this)
            integrator.setRequestCode(Constants.REQUEST_CODE_SCANNER)
            integrator.setOrientationLocked(false)
            integrator.captureActivity = AnyOrientationCaptureActivity::class.java
            integrator.initiateScan()
        } else {
            toast("Feature enable for only item code")
        }

    }

    override fun onActivityResult(requestCode: Int , resultCode: Int , data: Intent?) {
        super.onActivityResult(requestCode , resultCode , data)
        try {
            if (requestCode == Constants.REQUEST_CODE_SCANNER) {
                onActivityResultFromScanner(resultCode , data)
            }
        } catch (e: Exception) {
            Log.e(logTag , e.toString())
        }
    }

    //result: from scanner
    private fun onActivityResultFromScanner(resultCode: Int , data: Intent?) {
        try {
            val result = IntentIntegrator.parseActivityResult(resultCode , data)
            if (result != null) {
                if (result.contents == null) {
                    toast("Cancelled: Scanning")
                } else {
                    toast("Result : ${result.contents}")
                    //set result in edit text
                    isClickKeyBoard = true
                    etItemCode.setText(result.contents)
                    etItemCode.setSelection(result.contents.length)

                    //clear description
                    etItemDescription.setText("")
                }
            }
        } catch (e: java.lang.Exception) {
            Log.e(logTag , e.toString())
        }
    }
}
