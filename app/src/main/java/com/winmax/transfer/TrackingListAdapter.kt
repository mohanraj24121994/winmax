package com.winmax.transfer

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.model.MapModel
import com.winmax.transfer.model.tracking.TrackingListResponse
import kotlinx.android.synthetic.main.tracking_list_item_layout.view.*
import kotlinx.android.synthetic.main.tracking_list_item_layout.view.tvDate


class TrackingListAdapter(private var mContext: Context?, private var itemList: ArrayList<TrackingListResponse>, val listener: (ArrayList<TrackingListResponse>, Int, View) -> Unit) : RecyclerView.Adapter<TrackingListAdapter.ViewHolder>() {
    var offer = MapModel()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.tracking_list_item_layout, parent, false)
        return ViewHolder(v, mContext)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList, listener, position)
    }

    inner class ViewHolder(itemView: View, private var mContext: Context?) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(itemListData: ArrayList<TrackingListResponse>, listener: (ArrayList<TrackingListResponse>, Int, View) -> Unit, position: Int) {

            when (itemListData[position].stateOforder) {
                "Drop" -> {
                    itemView.cvTracking.setBackgroundColor(Color.parseColor("#90EE90"))
                }
                "PickUp" -> {
                    itemView.cvTracking.setBackgroundColor(Color.parseColor("#F31515"))
                    itemView.tvItemDescription.setTextColor(Color.parseColor("#FFFFFF"))
                }
                "Exchange" -> {
                    itemView.cvTracking.setBackgroundColor(Color.parseColor("#FFFF66"))
                }
            }
            itemView.tvContent.text = (position + 1).toString().plus(".").plus(itemListData.get(position).entityName)
            itemView.tvDate.text = "(".plus(itemListData.get(position).docDateTime?.replace("-2021", "")).plus(")")
            itemView.tvItemDescription.text = itemListData.get(position).itemDescription
            itemView.tvDriver.text = "Driver : ".plus(itemListData.get(position).driverName)
            itemView.tvmobile.text = itemListData.get(position).phoneNumber
            itemView.tvfulladdress.text = itemListData.get(position).entityAddress.plus("/")
                    .plus(itemListData.get(position).entityLocation).plus("/")
                    .plus(itemListData.get(position).entityZipCode)
            itemView.cvTracking.setOnClickListener {
                listener(itemListData, position, it)
            }
            itemView.tvmobile.setOnClickListener {
                val context = itemView.context
                val redirectDial = Uri.parse("tel:" + itemView.tvmobile.getText().toString())
                val intent = Intent(Intent.ACTION_DIAL, redirectDial)
                context.startActivity(intent)

            }
           /* itemView.tvfulladdress.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    offer.address = itemListData.get(position).entityAddress.plus(",")
                            .plus(itemListData.get(position).entityLocation).plus(",")
                            .plus(itemListData.get(position).entityZipCode)

                    if (Singleton.instance.addressvalue.size == 0) {
                        Singleton.instance.addressvalue.add(offer)
                    } else {
                        Singleton.instance.addressvalue.clear()
                    }
                    Singleton.instance.addressvalue.add(offer)
                    updateDetail()
                }

                fun updateDetail() {
                    val context = itemView.context
                    val intent = Intent(context, TrackinMapsActivity::class.java)
                    context.startActivity(intent)
                }

            })*/
        }

    }

}