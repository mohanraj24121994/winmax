package com.winmax.transfer.model.balance

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class GetEntityCurrentAccount {

    @SerializedName("Code")
    @Expose
     var code: String? = null

    @SerializedName("Message")
    @Expose
     var message: String? = null

    @SerializedName("EntityCurrentAccount")
    @Expose
     var entityCurrentAccount: EntityCurrentAccount? = null
}