package com.winmax.transfer.model.createentity

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class AdditionalAddress {
    @SerializedName("Designation")
    @Expose
    var designation: String? = null

    @SerializedName("Address")
    @Expose
    var address: String? = null

    @SerializedName("Location")
    @Expose
    var location: String? = null

    @SerializedName("ZipCode")
    @Expose
    var zipCode: String? = null
}