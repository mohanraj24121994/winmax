package com.winmax.transfer.model.createentity

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class CreateEntity {
    @SerializedName("Credential")
    @Expose
    var credential: Credential? = null

    @SerializedName("Name")
    @Expose
    var name: String? = null

    @SerializedName("EntityType")
    @Expose
    var entityType: String? = null

    @SerializedName("Address")
    @Expose
    var address: String? = null

    @SerializedName("ZipCode")
    @Expose
    var zipCode: String? = null

    @SerializedName("Phone")
    @Expose
    var phone: String? = null

    @SerializedName("Location")
    @Expose
    var location: String? = null

    @SerializedName("Remarks")
    @Expose
    var remarks: String? = null

    @SerializedName("AdditionalAddress")
    @Expose
    var additionalAddress: List<AdditionalAddress>? = null

}