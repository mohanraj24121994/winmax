package com.winmax.transfer.model.authenticateUser.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class AccessOptions {

    @SerializedName("AccessOption")
    @Expose
    private var accessOption: AccessOption? = null

    fun getAccessOption(): AccessOption? {
        return accessOption
    }

    fun setAccessOption(accessOption: AccessOption?) {
        this.accessOption = accessOption
    }
}