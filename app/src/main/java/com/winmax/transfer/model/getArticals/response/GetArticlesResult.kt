package com.winmax.transfer.model.getArticals.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class GetArticlesResult {

    @SerializedName("Code")
    @Expose
     val code: String? = null
    @SerializedName("Message")
    @Expose
     val message: String? = null
    @SerializedName("Articles")
    @Expose
     val articles: Articles? = null
}