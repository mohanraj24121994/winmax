package com.winmax.transfer.model.getentitiesbyphone

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GetEntitiesByPhone (
    @SerializedName("GetEntitiesResult")
    @Expose
    var getEntitiesResult: GetEntitiesResult? = null
)