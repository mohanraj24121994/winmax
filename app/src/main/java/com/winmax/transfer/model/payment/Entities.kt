package com.winmax.transfer.model.payment

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Entities {

    @SerializedName("Entity")
    @Expose
    private var entity: List<Entity?>? = null

    fun getEntity(): List<Entity?>? {
        return entity
    }

    fun setEntity(entity: List<Entity?>?) {
        this.entity = entity
    }
}