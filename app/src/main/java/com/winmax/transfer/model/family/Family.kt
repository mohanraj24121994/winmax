package com.winmax.transfer.model.family

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Family(@SerializedName("Code")
                  @Expose
                  val code: String? = null,
                  @SerializedName("Designation")
                  @Expose
                  val designation: String? = null) : Parcelable