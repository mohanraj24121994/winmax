package com.winmax.transfer.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.winmax.transfer.model.getWarehouse.GetWarehousesResult


class GetWarehouseData {

    @SerializedName("GetWarehousesResult")
    @Expose
    private var getWarehousesResult: GetWarehousesResult? = null

    fun getGetWarehousesResult(): GetWarehousesResult? {
        return getWarehousesResult
    }

    fun setGetWarehousesResult(getWarehousesResult: GetWarehousesResult?) {
        this.getWarehousesResult = getWarehousesResult
    }
}