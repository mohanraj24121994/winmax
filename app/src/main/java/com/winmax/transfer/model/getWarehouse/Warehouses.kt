package com.winmax.transfer.model.getWarehouse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Warehouses {
    @SerializedName("Warehouse")
    @Expose
    var warehouse: Any? = null
    // var warehouse: List<Warehouse?>? = null
}