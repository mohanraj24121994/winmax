package com.winmax.transfer.model.createArticle.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class DocumentDetail {
    @SerializedName("SourceWarehouseCode")
    @Expose
    private var sourceWarehouseCode: String? = null
    @SerializedName("TargetWarehouseCode")
    @Expose
    private var targetWarehouseCode: String? = null
    @SerializedName("DocumentTypeCode")
    @Expose
    private var documentTypeCode: String? = null
    @SerializedName("EntityCode")
    @Expose
    private var entityCode: String? = null
    @SerializedName("IsPOS")
    @Expose
    var IsPOS: String? = null
    @SerializedName("IsExternal")
    @Expose
    var IsExternal: String? = null

    fun getSourceWarehouseCode(): String? {
        return sourceWarehouseCode
    }

    fun setSourceWarehouseCode(sourceWarehouseCode: String?) {
        this.sourceWarehouseCode = sourceWarehouseCode
    }

    fun getTargetWarehouseCode(): String? {
        return targetWarehouseCode
    }

    fun setTargetWarehouseCode(targetWarehouseCode: String?) {
        this.targetWarehouseCode = targetWarehouseCode
    }

    fun getDocumentTypeCode(): String? {
        return documentTypeCode
    }

    fun setDocumentTypeCode(documentTypeCode: String?) {
        this.documentTypeCode = documentTypeCode
    }

    fun getEntityCode(): String? {
        return entityCode
    }

    fun setEntityCode(entityCode: String?) {
        this.entityCode = entityCode
    }
}