package com.winmax.transfer.model.getentitydescription

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName




class GetEntityDescription {

    @SerializedName("Code")
    @Expose
    var code: Int? = null

    @SerializedName("EntityName")
    @Expose
    var entityName: String? = null

    @SerializedName("PhoneNumber")
    @Expose
    var phoneNumber: Any? = null
}