package com.winmax.transfer.model.getentitiesbyphone

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class AditionalAddresses {
    @SerializedName("Address")
    @Expose
    var address: List<Address?>? = null
}