package com.winmax.transfer.model.getArticals.response

import android.os.Parcelable
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Prices(
        @SerializedName("Price")
        @Expose
        val price: Price? = null
) : Parcelable