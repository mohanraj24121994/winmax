package com.winmax.transfer.model.authenticateUser.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName






class AuthResponse{
    @SerializedName("AuthenticateUserResult")
    @Expose
    private var authenticateUserResult: AuthenticateUserResult? = null

    fun getAuthenticateUserResult(): AuthenticateUserResult? {
        return authenticateUserResult
    }

    fun setAuthenticateUserResult(authenticateUserResult: AuthenticateUserResult?) {
        this.authenticateUserResult = authenticateUserResult
    }
}