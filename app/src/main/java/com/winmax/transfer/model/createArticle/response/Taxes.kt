package com.winmax.transfer.model.createArticle.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Taxes {

    @SerializedName("Tax")
    @Expose
     val tax: Tax? = null
}