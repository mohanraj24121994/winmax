package com.winmax.transfer.model.authenticateUser.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class AccessOption {
    @SerializedName("OptionID")
    @Expose
    private var optionID: String? = null
    @SerializedName("CanRead")
    @Expose
    private var canRead: String? = null
    @SerializedName("CanEdit")
    @Expose
    private var canEdit: String? = null
    @SerializedName("CanInsert")
    @Expose
    private var canInsert: String? = null
    @SerializedName("CanDelete")
    @Expose
    private var canDelete: String? = null

    fun getOptionID(): String? {
        return optionID
    }

    fun setOptionID(optionID: String?) {
        this.optionID = optionID
    }

    fun getCanRead(): String? {
        return canRead
    }

    fun setCanRead(canRead: String?) {
        this.canRead = canRead
    }

    fun getCanEdit(): String? {
        return canEdit
    }

    fun setCanEdit(canEdit: String?) {
        this.canEdit = canEdit
    }

    fun getCanInsert(): String? {
        return canInsert
    }

    fun setCanInsert(canInsert: String?) {
        this.canInsert = canInsert
    }

    fun getCanDelete(): String? {
        return canDelete
    }

    fun setCanDelete(canDelete: String?) {
        this.canDelete = canDelete
    }
}