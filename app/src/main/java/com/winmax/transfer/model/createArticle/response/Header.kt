package com.winmax.transfer.model.createArticle.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Header {
    @SerializedName("ApplicationName")
    @Expose
     val applicationName: String? = null
    @SerializedName("ApplicationVersion")
    @Expose
     val applicationVersion: String? = null
    @SerializedName("CurrencyCode")
    @Expose
     val currencyCode: String? = null
    @SerializedName("TerminalConfigurationCode")
    @Expose
     val terminalConfigurationCode: String? = null
    @SerializedName("DocumentTypeCode")
    @Expose
     val documentTypeCode: String? = null
    @SerializedName("DocumentNumber")
    @Expose
     val documentNumber: String? = null
    @SerializedName("DocumentNumberPartNumber")
    @Expose
     val documentNumberPartNumber: String? = null
    @SerializedName("DocumentNumberPartSerial")
    @Expose
     val documentNumberPartSerial: String? = null
    @SerializedName("DocumentDate")
    @Expose
     val documentDate: String? = null
    @SerializedName("DueDate")
    @Expose
     val dueDate: String? = null
    @SerializedName("PaymentConditionCode")
    @Expose
     val paymentConditionCode: String? = null
    @SerializedName("EntityCode")
    @Expose
     val entityCode: String? = null
    @SerializedName("EntityCountryCode")
    @Expose
     val entityCountryCode: String? = null
    @SerializedName("EntityName")
    @Expose
     val entityName: String? = null
    @SerializedName("EntityTaxPayerID")
    @Expose
     val entityTaxPayerID: String? = null
    @SerializedName("EntityAddress")
    @Expose
     val entityAddress: String? = null
    @SerializedName("EntityZipCode")
    @Expose
     val entityZipCode: String? = null
    @SerializedName("EntityLocation")
    @Expose
     val entityLocation: String? = null
    @SerializedName("SourceWarehouseCode")
    @Expose
     val sourceWarehouseCode: String? = null
    @SerializedName("TargetWarehouseCode")
    @Expose
     val targetWarehouseCode: String? = null
    @SerializedName("LoadAddress")
    @Expose
     val loadAddress: String? = null
    @SerializedName("LoadLocation")
    @Expose
     val loadLocation: String? = null
    @SerializedName("LoadZipCode")
    @Expose
     val loadZipCode: String? = null
    @SerializedName("LoadCountryCode")
    @Expose
     val loadCountryCode: String? = null
    @SerializedName("LoadDateTime")
    @Expose
     val loadDateTime: String? = null
    @SerializedName("LoadVehicleLicensePlate")
    @Expose
     val loadVehicleLicensePlate: String? = null
    @SerializedName("UnloadAddress")
    @Expose
     val unloadAddress: String? = null
    @SerializedName("UnloadCountryCode")
    @Expose
     val unloadCountryCode: String? = null
    @SerializedName("UnloadZipCode")
    @Expose
     val unloadZipCode: String? = null
    @SerializedName("UnloadLocation")
    @Expose
     val unloadLocation: String? = null
    @SerializedName("UnloadDateTime")
    @Expose
     val unloadDateTime: String? = null
    @SerializedName("Remarks")
    @Expose
     val remarks: String? = null
    @SerializedName("DocumentDiscountPercentage")
    @Expose
     val documentDiscountPercentage: String? = null
    @SerializedName("RetemptionAtSourcePercentage")
    @Expose
     val retemptionAtSourcePercentage: String? = null
    @SerializedName("TotalHandlingShippingWithoutTaxes")
    @Expose
     val totalHandlingShippingWithoutTaxes: String? = null
    @SerializedName("TotalWithoutTaxesAfterDiscounts")
    @Expose
     val totalWithoutTaxesAfterDiscounts: String? = null
    @SerializedName("TotalTaxesApplied")
    @Expose
     val totalTaxesApplied: String? = null
    @SerializedName("TotalTaxesExempted")
    @Expose
     val totalTaxesExempted: String? = null
    @SerializedName("TotalTaxesReverted")
    @Expose
     val totalTaxesReverted: String? = null
    @SerializedName("Total")
    @Expose
     val total: String? = null
    @SerializedName("IsPOS")
    @Expose
     val isPOS: String? = null
    @SerializedName("IsClosed")
    @Expose
     val isClosed: String? = null
    @SerializedName("IsDeleted")
    @Expose
     val isDeleted: String? = null
    @SerializedName("IsExternal")
    @Expose
     val isExternal: String? = null
    @SerializedName("PaymentTypes")
    @Expose
     val paymentTypes: String? = null
    @SerializedName("ArchivedDocumentHTTPPath")
    @Expose
     val archivedDocumentHTTPPath: String? = null
    @SerializedName("ExternalDocumentsRelation")
    @Expose
     val externalDocumentsRelation: String? = null
    @SerializedName("TADocCodeID")
    @Expose
     val tADocCodeID: String? = null
    @SerializedName("CurrentAccountTransactionType")
    @Expose
     val currentAccountTransactionType: String? = null
}