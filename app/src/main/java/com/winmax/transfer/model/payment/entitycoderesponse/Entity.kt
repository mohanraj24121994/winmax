package com.winmax.transfer.model.payment.entitycoderesponse

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Entity {
    @SerializedName("Code")
    @Expose
    private var code: String? = null
    @SerializedName("Name")
    @Expose
    private var name: String? = null
    @SerializedName("EntityType")
    @Expose
    private var entityType: String? = null
    @SerializedName("TaxPayerID")
    @Expose
    private var taxPayerID: String? = null
    @SerializedName("DiscountPercentage1")
    @Expose
    private var discountPercentage1: String? = null
    @SerializedName("DiscountPercentage2")
    @Expose
    private var discountPercentage2: String? = null
    @SerializedName("CreditLimit")
    @Expose
    private var creditLimit: String? = null
    @SerializedName("PriceType")
    @Expose
    private var priceType: String? = null
    @SerializedName("SalesPersonCode")
    @Expose
    private var salesPersonCode: String? = null
    @SerializedName("DiscountLevel")
    @Expose
    private var discountLevel: String? = null
    @SerializedName("Fax")
    @Expose
    private var fax: String? = null
    @SerializedName("MobilePhone")
    @Expose
    private var mobilePhone: String? = null
    @SerializedName("Email")
    @Expose
    private var email: String? = null
    @SerializedName("IsBlocked")
    @Expose
    private var isBlocked: String? = null
    @SerializedName("BlockReason")
    @Expose
    private var blockReason: String? = null
    @SerializedName("CountryCode")
    @Expose
    private var countryCode: List<String?>? = null
    @SerializedName("Account")
    @Expose
    private var account: String? = null
    @SerializedName("Address")
    @Expose
    private var address: String? = null
    @SerializedName("ZipCode")
    @Expose
    private var zipCode: String? = null
    @SerializedName("Location")
    @Expose
    private var location: String? = null
    @SerializedName("Phone")
    @Expose
    private var phone: String? = null

    fun getCode(): String? {
        return code
    }

    fun setCode(code: String?) {
        this.code = code
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getEntityType(): String? {
        return entityType
    }

    fun setEntityType(entityType: String?) {
        this.entityType = entityType
    }

    fun getTaxPayerID(): String? {
        return taxPayerID
    }

    fun setTaxPayerID(taxPayerID: String?) {
        this.taxPayerID = taxPayerID
    }

    fun getDiscountPercentage1(): String? {
        return discountPercentage1
    }

    fun setDiscountPercentage1(discountPercentage1: String?) {
        this.discountPercentage1 = discountPercentage1
    }

    fun getDiscountPercentage2(): String? {
        return discountPercentage2
    }

    fun setDiscountPercentage2(discountPercentage2: String?) {
        this.discountPercentage2 = discountPercentage2
    }

    fun getCreditLimit(): String? {
        return creditLimit
    }

    fun setCreditLimit(creditLimit: String?) {
        this.creditLimit = creditLimit
    }

    fun getPriceType(): String? {
        return priceType
    }

    fun setPriceType(priceType: String?) {
        this.priceType = priceType
    }

    fun getSalesPersonCode(): String? {
        return salesPersonCode
    }

    fun setSalesPersonCode(salesPersonCode: String?) {
        this.salesPersonCode = salesPersonCode
    }

    fun getDiscountLevel(): String? {
        return discountLevel
    }

    fun setDiscountLevel(discountLevel: String?) {
        this.discountLevel = discountLevel
    }

    fun getFax(): String? {
        return fax
    }

    fun setFax(fax: String?) {
        this.fax = fax
    }

    fun getMobilePhone(): String? {
        return mobilePhone
    }

    fun setMobilePhone(mobilePhone: String?) {
        this.mobilePhone = mobilePhone
    }

    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String?) {
        this.email = email
    }

    fun getIsBlocked(): String? {
        return isBlocked
    }

    fun setIsBlocked(isBlocked: String?) {
        this.isBlocked = isBlocked
    }

    fun getBlockReason(): String? {
        return blockReason
    }

    fun setBlockReason(blockReason: String?) {
        this.blockReason = blockReason
    }

    fun getCountryCode(): List<String?>? {
        return countryCode
    }

    fun setCountryCode(countryCode: List<String?>?) {
        this.countryCode = countryCode
    }

    fun getAccount(): String? {
        return account
    }

    fun setAccount(account: String?) {
        this.account = account
    }

    fun getAddress(): String? {
        return address
    }

    fun setAddress(address: String?) {
        this.address = address
    }

    fun getZipCode(): String? {
        return zipCode
    }

    fun setZipCode(zipCode: String?) {
        this.zipCode = zipCode
    }

    fun getLocation(): String? {
        return location
    }

    fun setLocation(location: String?) {
        this.location = location
    }

    fun getPhone(): String? {
        return phone
    }

    fun setPhone(phone: String?) {
        this.phone = phone
    }
}