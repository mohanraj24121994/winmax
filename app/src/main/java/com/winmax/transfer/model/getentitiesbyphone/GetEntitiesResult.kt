package com.winmax.transfer.model.getentitiesbyphone

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class GetEntitiesResult (
    @SerializedName("Code")
    @Expose
     var code: String? = null,

    @SerializedName("Message")
    @Expose
     var message: String? = null,

    @SerializedName("Filter")
    @Expose
     var filter: Filter? = null,

    @SerializedName("Entities")
    @Expose
     var entities: Entities? = null
)