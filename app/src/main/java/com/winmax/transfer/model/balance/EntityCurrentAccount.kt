package com.winmax.transfer.model.balance

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class EntityCurrentAccount {
    @SerializedName("EntityCode")
    @Expose
    var entityCode: String? = null

    @SerializedName("Transactions")
    @Expose
    var transactions: Transactions? = null
}