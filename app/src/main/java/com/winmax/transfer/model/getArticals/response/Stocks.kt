package com.winmax.transfer.model.getArticals.response

import android.os.Parcelable
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Stocks(
        @SerializedName("Stock")
        @Expose
        val stock: List<Stock>? = null
) : Parcelable