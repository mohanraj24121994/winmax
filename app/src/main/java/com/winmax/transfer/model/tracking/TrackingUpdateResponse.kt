package com.winmax.transfer.model.tracking

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TrackingUpdateResponse {
    @SerializedName("credential")
    @Expose
    var credential: TrackingCredentials? = null

    @SerializedName("EntityCode")
    @Expose
    var entityCode: Int? = null

    @SerializedName("ShippingTypeCode")
    @Expose
    var shippingTypeCode: String? = null

    @SerializedName("DocumentNumber")
    @Expose
    var documentNumber: String? = null

    @SerializedName("OrderDetails")
    @Expose
    var orderDetails: TrackingOrderDetails? = null

    @SerializedName("Status")
    @Expose
    var status: String? = null

    @SerializedName("Pdf")
    @Expose
    var pdf: String? = null
}