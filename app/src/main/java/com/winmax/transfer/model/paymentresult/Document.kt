package com.winmax.transfer.model.paymentresult

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Document {
    @SerializedName("DocumentTypeCode")
    @Expose
    private var documentTypeCode: String? = null
    @SerializedName("DocumentNumber")
    @Expose
    private var documentNumber: String? = null
    @SerializedName("DocumentNumberPartNumber")
    @Expose
    private var documentNumberPartNumber: String? = null
    @SerializedName("DocumentNumberPartSerial")
    @Expose
    private var documentNumberPartSerial: String? = null
    @SerializedName("DocumentYear")
    @Expose
    private var documentYear: String? = null
    @SerializedName("Total")
    @Expose
    private var total: String? = null
    @SerializedName("ArchivedDocumentHTTPPath")
    @Expose
    private var archivedDocumentHTTPPath: String? = null

    fun getDocumentTypeCode(): String? {
        return documentTypeCode
    }

    fun setDocumentTypeCode(documentTypeCode: String?) {
        this.documentTypeCode = documentTypeCode
    }

    fun getDocumentNumber(): String? {
        return documentNumber
    }

    fun setDocumentNumber(documentNumber: String?) {
        this.documentNumber = documentNumber
    }

    fun getDocumentNumberPartNumber(): String? {
        return documentNumberPartNumber
    }

    fun setDocumentNumberPartNumber(documentNumberPartNumber: String?) {
        this.documentNumberPartNumber = documentNumberPartNumber
    }

    fun getDocumentNumberPartSerial(): String? {
        return documentNumberPartSerial
    }

    fun setDocumentNumberPartSerial(documentNumberPartSerial: String?) {
        this.documentNumberPartSerial = documentNumberPartSerial
    }

    fun getDocumentYear(): String? {
        return documentYear
    }

    fun setDocumentYear(documentYear: String?) {
        this.documentYear = documentYear
    }

    fun getTotal(): String? {
        return total
    }

    fun setTotal(total: String?) {
        this.total = total
    }

    fun getArchivedDocumentHTTPPath(): String? {
        return archivedDocumentHTTPPath
    }

    fun setArchivedDocumentHTTPPath(archivedDocumentHTTPPath: String?) {
        this.archivedDocumentHTTPPath = archivedDocumentHTTPPath
    }

}