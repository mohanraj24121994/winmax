package com.winmax.transfer.model.tracking

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TrackingCredentials {

    @SerializedName("CompanyCode")
    @Expose
    var companyCode: String? = null

    @SerializedName("UserLogin")
    @Expose
    var userLogin: String? = null

    @SerializedName("UserPassword")
    @Expose
    var userPassword: String? = null

    @SerializedName("WebServiceURL")
    @Expose
    var webServiceURL: String? = null

    @SerializedName("PageNumber")
    @Expose
    var pageNumber: Int? = null

    @SerializedName("EntityCode")
    @Expose
    var entityCode: Int? = null

    @SerializedName("ConnectionString")
    @Expose
    var connectionString: Any? = null

    @SerializedName("TerminalNo")
    @Expose
    var terminalNo: Any? = null

    @SerializedName("PhoneNumber")
    @Expose
    var phoneNumber: Any? = null
}