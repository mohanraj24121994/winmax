package com.winmax.transfer.model.createArticle.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class CreateArticleRequest {

    @SerializedName("credential")
    @Expose
    private var credential: Credential? = null
    @SerializedName("DocumentDetail")
    @Expose
    private var documentDetail: DocumentDetail? = null
    @SerializedName("ArticleDetail")
    @Expose
    private var articleDetail: List<ArticleDetail?>? = null

    @SerializedName("ShippingTypeCode")
    @Expose
    var shippingTypeCode: String? = null

    @SerializedName("stateOfOrder")
    @Expose
    var stateOfOrder: String? = null
    @SerializedName("Address")
    @Expose
    var Address: String? = null
    @SerializedName("Location")
    @Expose
    var Location: String? = null
    @SerializedName("ZipCode")
    @Expose
    var ZipCode: String? = null

    fun getCredential(): Credential? {
        return credential
    }

    fun setCredential(credential: Credential?) {
        this.credential = credential
    }

    fun getDocumentDetail(): DocumentDetail? {
        return documentDetail
    }

    fun setDocumentDetail(documentDetail: DocumentDetail?) {
        this.documentDetail = documentDetail
    }

    fun getArticleDetail(): List<ArticleDetail?>? {
        return articleDetail
    }

    fun setArticleDetail(articleDetail: List<ArticleDetail?>?) {
        this.articleDetail = articleDetail
    }


}