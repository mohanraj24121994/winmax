package com.winmax.transfer.model.authenticateUser.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class AuthenticateUserResult {

    @SerializedName("Code")
    @Expose
    private var code: String? = null
    @SerializedName("Message")
    @Expose
    private var message: String? = null
    @SerializedName("UserAccount")
    @Expose
    private var userAccount: UserAccount? = null

    fun getCode(): String? {
        return code
    }

    fun setCode(code: String?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getUserAccount(): UserAccount? {
        return userAccount
    }

    fun setUserAccount(userAccount: UserAccount?) {
        this.userAccount = userAccount
    }

}