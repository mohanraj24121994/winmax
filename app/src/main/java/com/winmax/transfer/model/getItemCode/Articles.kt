package com.winmax.transfer.model.getItemCode

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Articles {
    @SerializedName("Article")
    @Expose
    var article: List<Article?>? = null

}