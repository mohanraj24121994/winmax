package com.winmax.transfer.model.paymentresult

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Documents {

    @SerializedName("Document")
    @Expose
    private var document: Document? = null

    fun getDocument(): Document? {
        return document
    }

    fun setDocument(document: Document?) {
        this.document = document
    }
}