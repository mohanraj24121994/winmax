package com.winmax.transfer.model.getWarehouse

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class GetWarehousesResult {

    @SerializedName("Code")
    @Expose
    private var code: String? = null
    @SerializedName("Message")
    @Expose
    private var message: String? = null
    @SerializedName("Warehouses")
    @Expose
    private var warehouses: Warehouses? = null

    fun getCode(): String? {
        return code
    }

    fun setCode(code: String?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getWarehouses(): Warehouses? {
        return warehouses
    }

    fun setWarehouses(warehouses: Warehouses?) {
        this.warehouses = warehouses
    }
}