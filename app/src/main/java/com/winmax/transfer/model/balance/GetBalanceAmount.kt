package com.winmax.transfer.model.balance

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class GetBalanceAmount {
    @SerializedName("GetEntityCurrentAccount")
    @Expose
     var getEntityCurrentAccount: GetEntityCurrentAccount? = null

}