package com.winmax.transfer.model.createArticle.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Details {

    @SerializedName("Detail")
    @Expose
     val detail: List<Detail>? = null

}