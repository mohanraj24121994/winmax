package com.winmax.transfer.model.itemCode

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ItemCodeList {
    @SerializedName("ArticleCode")
    @Expose
    var articleCode: String? = null

    @SerializedName("Designation")
    @Expose
    var designation: String? = null
}