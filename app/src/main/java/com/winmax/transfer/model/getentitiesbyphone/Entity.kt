package com.winmax.transfer.model.getentitiesbyphone

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Entity(
        @SerializedName("Code")
        @Expose
        var code: String? = null ,

        @SerializedName("Name")
        @Expose
        var name: String? = null ,

        @SerializedName("EntityType")
        @Expose
        var entityType: String? = null ,

        @SerializedName("TaxPayerID")
        @Expose
        var taxPayerID: String? = null ,

        @SerializedName("DiscountPercentage1")
        @Expose
        var discountPercentage1: String? = null ,

        @SerializedName("DiscountPercentage2")
        @Expose
        var discountPercentage2: String? = null ,

        @SerializedName("CreditLimit")
        @Expose
        var creditLimit: String? = null ,

        @SerializedName("PriceType")
        @Expose
        var priceType: String? = null ,

        @SerializedName("SalesPersonCode")
        @Expose
        var salesPersonCode: String? = null ,

        @SerializedName("DiscountLevel")
        @Expose
        var discountLevel: String? = null ,

        @SerializedName("Fax")
        @Expose
        var fax: String? = null ,

        @SerializedName("MobilePhone")
        @Expose
        var mobilePhone: String? = null ,

        @SerializedName("Email")
        @Expose
        var email: String? = null ,

        @SerializedName("IsBlocked")
        @Expose
        var isBlocked: String? = null ,

        @SerializedName("BlockReason")
        @Expose
        var blockReason: String? = null ,

        @SerializedName("CountryCode")
        @Expose
        var countryCode: List<String?>? = null ,

        @SerializedName("Account")
        @Expose
        var account: String? = null ,

        @SerializedName("Address")
        @Expose
        var address: String? = null ,

        @SerializedName("ZipCode")
        @Expose
        var zipCode: String? = null ,

        @SerializedName("Location")
        @Expose
        var location: String? = null ,

        @SerializedName("Phone")
        @Expose
        var phone: String? = null ,

        @SerializedName("Remarks")
        @Expose
        var remarks: String? = null ,

        @SerializedName("AditionalAddresses")
        @Expose
        var aditionalAddresses: AditionalAddresses? = null
)