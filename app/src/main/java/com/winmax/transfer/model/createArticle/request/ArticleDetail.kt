package com.winmax.transfer.model.createArticle.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class ArticleDetail {
    @SerializedName("ArticleCode")
    @Expose
     var articleCode: String? = null
    @SerializedName("ArticleDesignation")
    @Expose
     var articleDesignation: String? = null
    @SerializedName("Quantity")
    @Expose
     var quantity: String? = null
}