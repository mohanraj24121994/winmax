package com.winmax.transfer.model.webserviceConnection.WebTestResponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.winmax.transfer.model.authenticateUser.response.AuthenticateUserResult


class WebTestResponse {

    @SerializedName("AuthenticateUserResult")
    @Expose
     var authenticateUserResult: AuthenticateUserResult? = null


}