package com.winmax.transfer.model.authenticateUser.request



import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Request {

    @SerializedName("Credential")
    @Expose
    private var credential: Credential? = null
    @SerializedName("AuthenticateUser")
    @Expose
    private var authenticateUser: AuthenticateUser? = null

    fun getCredential(): Credential? {
        return credential
    }

    fun setCredential(credential: Credential?) {
        this.credential = credential
    }

    fun getAuthenticateUser(): AuthenticateUser? {
        return authenticateUser
    }

    fun setAuthenticateUser(authenticateUser: AuthenticateUser?) {
        this.authenticateUser = authenticateUser
    }
}