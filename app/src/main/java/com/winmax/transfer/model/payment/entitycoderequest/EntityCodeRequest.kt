package com.winmax.transfer.model.payment.entitycoderequest

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class EntityCodeRequest {

    @SerializedName("CompanyCode")
    @Expose
    var companyCode: String? = null
    @SerializedName("UserLogin")
    @Expose
     var userLogin: String? = null
    @SerializedName("UserPassword")
    @Expose
     var userPassword: String? = null
    @SerializedName("WebServiceURL")
    @Expose
     var webServiceURL: String? = null
    @SerializedName("EntityCode")
    @Expose
     var EntityCode: String? = null
}