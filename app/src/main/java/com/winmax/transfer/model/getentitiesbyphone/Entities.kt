package com.winmax.transfer.model.getentitiesbyphone

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Entities {
    @SerializedName("Entity")
    @Expose
    var entity: Entity? = null
}