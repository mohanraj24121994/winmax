package com.winmax.transfer.model.paymentresult

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class PaymentResultResponse {
    @SerializedName("PayDocumentsResult")
    @Expose
    private var payDocumentsResult: PayDocumentsResult? = null

    fun getPayDocumentsResult(): PayDocumentsResult? {
        return payDocumentsResult
    }

    fun setPayDocumentsResult(payDocumentsResult: PayDocumentsResult?) {
        this.payDocumentsResult = payDocumentsResult
    }
}