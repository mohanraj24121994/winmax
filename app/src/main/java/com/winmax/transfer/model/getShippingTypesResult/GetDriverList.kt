package com.winmax.transfer.model.getShippingTypesResult

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class GetDriverList {
    @SerializedName("GetShippingTypesResult")
    @Expose
    private var getShippingTypesResult: GetShippingTypesResult? = null

    fun getGetShippingTypesResult(): GetShippingTypesResult? {
        return getShippingTypesResult
    }

    fun setGetShippingTypesResult(getShippingTypesResult: GetShippingTypesResult?) {
        this.getShippingTypesResult = getShippingTypesResult
    }

}