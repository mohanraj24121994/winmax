package com.winmax.transfer.model.getArticals.response

import android.os.Parcelable
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Stock(
        @SerializedName("WarehouseCode")
        @Expose
        val warehouseCode: String? = null,
        @SerializedName("CurrentStock")
        @Expose
        val currentStock: String? = null,
        @SerializedName("Location")
        @Expose
        val location: String? = null
) : Parcelable