package com.winmax.transfer.model.getentitiesbyphone

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Address {
    @SerializedName("Designation")
    @Expose
    var designation: String? = null

    @SerializedName("ZipCode")
    @Expose
    var zipCode: String? = null

    @SerializedName("Location")
    @Expose
    var location: String? = null

    @SerializedName("CountryCode")
    @Expose
    var countryCode: String? = null

    @SerializedName("Phone")
    @Expose
    var phone: String? = null

    var remarks: String? = null

    @SerializedName("Address")
    @Expose
    var address: String? = null
}