package com.winmax.transfer.model.tracking

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.winmax.transfer.model.createentity.Credential

class TrackingUpdateRequest {

    @SerializedName("credential")
    @Expose
    var credential: Credential? = null

    @SerializedName("ShippingTypeCode")
    @Expose
    var shippingTypeCode: String? = null

    @SerializedName("EntityCode")
    @Expose
    var entityCode: String? = null

    @SerializedName("DocumentNumber")
    @Expose
    var documentNumber: String? = null

    @SerializedName("DropDate")
    @Expose
    var dropDate: String? = null

    @SerializedName("OrderDetails")
    @Expose
    var orderDetails: TrackingOrderDetails? = null
}