package com.winmax.transfer.model.createentity.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Winmax4Entity {
    @SerializedName("IsCreated")
    @Expose
    var isCreated: String? = null

    @SerializedName("EntityCode")
    @Expose
    var entityCode: String? = null
}