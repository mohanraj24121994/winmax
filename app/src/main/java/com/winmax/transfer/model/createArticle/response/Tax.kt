package com.winmax.transfer.model.createArticle.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Tax {
    @SerializedName("TaxFeeCode")
    @Expose
     val taxFeeCode: String? = null
    @SerializedName("TaxFeeRatePercentage")
    @Expose
     val taxFeeRatePercentage: String? = null
    @SerializedName("TaxFeeRateFixedAmount")
    @Expose
     val taxFeeRateFixedAmount: String? = null
    @SerializedName("SAFTDesignationID")
    @Expose
     val sAFTDesignationID: String? = null
}