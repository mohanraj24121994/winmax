package com.winmax.transfer.model.paymentresult.paymentrequest

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PayDocumentRequest {
    @SerializedName("EntityCode")
    @Expose
    var entityCode: String? = null
    @SerializedName("EntityType")
    @Expose
    var entityType: String? = null
    @SerializedName("PaymentType")
    @Expose
    var paymentType: String? = null
    @SerializedName("Value")
    @Expose
    var value: String? = null
}