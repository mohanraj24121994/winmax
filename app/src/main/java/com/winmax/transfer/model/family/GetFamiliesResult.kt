package com.winmax.transfer.model.family

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetFamiliesResult {

    @SerializedName("Code")
    @Expose
    val code: String? = null
    @SerializedName("Message")
    @Expose
    val message: String? = null
    @SerializedName("Families")
    @Expose
    val families: Families? = null
}