package com.winmax.transfer.model.paymentresult.paymentrequest

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CredentialRequest {

    @SerializedName("credential")
    @Expose
    var credential: Credential? = null
    @SerializedName("PayDocument")
    @Expose
    var PayDocument: PayDocumentRequest? = null
}