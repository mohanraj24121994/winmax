package com.winmax.transfer.model.family

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Families(@SerializedName("Family")
                    @Expose
                    val family: Family? = null) : Parcelable