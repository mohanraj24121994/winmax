package com.winmax.transfer.model.createArticle.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class CreateArticleResponse {


    @SerializedName("Winmax4Document")
    @Expose
     val winmax4Document: Winmax4Document? = null
}