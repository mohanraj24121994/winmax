package com.winmax.transfer.model.getItemCode

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Article {

    @SerializedName("ArticleCode")
    @Expose
    var articleCode: String? = null

    @SerializedName("Designation")
    @Expose
    var designation: String? = null

    @SerializedName("FamilyCode")
    @Expose
    var familyCode: String? = null

    @SerializedName("SubFamilyCode")
    @Expose
    var subFamilyCode: String? = null

    @SerializedName("DiscountLevel")
    @Expose
    var discountLevel: String? = null

    @SerializedName("SellUnitCode")
    @Expose
    var sellUnitCode: String? = null

    @SerializedName("SAFTType")
    @Expose
    var sAFTType: String? = null

    @SerializedName("LastPurchaseDate")
    @Expose
    var lastPurchaseDate: String? = null

    @SerializedName("LastSellDate")
    @Expose
    var lastSellDate: String? = null

    @SerializedName("SubSubFamilyCode")
    @Expose
    var subSubFamilyCode: String? = null
}