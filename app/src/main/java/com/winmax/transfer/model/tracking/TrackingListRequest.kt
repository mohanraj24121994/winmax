package com.winmax.transfer.model.tracking

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.winmax.transfer.model.createentity.Credential

class TrackingListRequest {
    @SerializedName("credential")
    @Expose
    var credential: Credential? = null

    @SerializedName("ShippingTypeCode")
    @Expose
    var shippingTypeCode: String? = null
}