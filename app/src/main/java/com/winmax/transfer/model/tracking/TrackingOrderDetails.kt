package com.winmax.transfer.model.tracking

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TrackingOrderDetails {

    @SerializedName("OrderStage")
    @Expose
    var orderStage: String? = null

    @SerializedName("Address")
    @Expose
    var address: String? = null

    @SerializedName("Latitude")
    @Expose
    var latitude: String? = null

    @SerializedName("Longtitude")
    @Expose
    var longtitude: String? = null
}