package com.winmax.transfer.model.getItemCode

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GetArticlesResponse {
    @SerializedName("GetArticlesResult")
    @Expose
    var getArticlesResult: GetArticlesResult? = null
}