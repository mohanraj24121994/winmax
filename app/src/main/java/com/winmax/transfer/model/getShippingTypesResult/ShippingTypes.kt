package com.winmax.transfer.model.getShippingTypesResult

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class ShippingTypes {
    @SerializedName("ShippingType")
    @Expose
    private var shippingType: List<ShippingType?>? = null

    fun getShippingType(): List<ShippingType?>? {
        return shippingType
    }

    fun setShippingType(shippingType: List<ShippingType?>?) {
        this.shippingType = shippingType
    }
}