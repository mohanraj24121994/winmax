package com.winmax.transfer.model.createArticle.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Detail {

    @SerializedName("ArticleCode")
    @Expose
     val articleCode: String? = null
    @SerializedName("ArticleDesignation")
    @Expose
     val articleDesignation: String? = null
    @SerializedName("ArticleShortDesignation")
    @Expose
     val articleShortDesignation: String? = null
    @SerializedName("ArticleType")
    @Expose
     val articleType: String? = null
    @SerializedName("Batches")
    @Expose
     val batches: String? = null
    @SerializedName("SerialNumbers")
    @Expose
     val serialNumbers: String? = null
    @SerializedName("Composition")
    @Expose
     val composition: String? = null
    @SerializedName("Remarks")
    @Expose
     val remarks: String? = null
    @SerializedName("ExtraRemarks")
    @Expose
     val extraRemarks: String? = null
    @SerializedName("UnitCode")
    @Expose
     val unitCode: String? = null
    @SerializedName("UnitAbsoluteValue")
    @Expose
     val unitAbsoluteValue: String? = null
    @SerializedName("Taxes")
    @Expose
     val taxes: Taxes? = null
    @SerializedName("UnitaryPriceWithoutTaxesBeforeDiscounts")
    @Expose
     val unitaryPriceWithoutTaxesBeforeDiscounts: String? = null
    @SerializedName("UnitaryPriceWithTaxesBeforeDiscounts")
    @Expose
     val unitaryPriceWithTaxesBeforeDiscounts: String? = null
    @SerializedName("DiscountPercentage1")
    @Expose
     val discountPercentage1: String? = null
    @SerializedName("DiscountPercentage2")
    @Expose
     val discountPercentage2: String? = null
    @SerializedName("UnitaryPriceWithoutTaxesAfterDiscounts")
    @Expose
     val unitaryPriceWithoutTaxesAfterDiscounts: String? = null
    @SerializedName("UnitaryPriceWithTaxesAfterDiscounts")
    @Expose
     val unitaryPriceWithTaxesAfterDiscounts: String? = null
    @SerializedName("Quantity")
    @Expose
     val quantity: String? = null
    @SerializedName("QuantityX")
    @Expose
     val quantityX: String? = null
    @SerializedName("QuantityY")
    @Expose
     val quantityY: String? = null
    @SerializedName("QuantityZ")
    @Expose
     val quantityZ: String? = null
    @SerializedName("TotalWithoutTaxes")
    @Expose
     val totalWithoutTaxes: String? = null
    @SerializedName("TotalWithTaxes")
    @Expose
     val totalWithTaxes: String? = null
    @SerializedName("IsTare")
    @Expose
     val isTare: String? = null
}