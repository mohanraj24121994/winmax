package com.winmax.transfer.model.payment.entitycoderesponse

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class EntityCodeDetail {
    @SerializedName("GetEntityResult")
    @Expose
    private var getEntityResult: GetEntityResult? = null

    fun getGetEntityResult(): GetEntityResult? {
        return getEntityResult
    }

    fun setGetEntityResult(getEntityResult: GetEntityResult?) {
        this.getEntityResult = getEntityResult
    }
}