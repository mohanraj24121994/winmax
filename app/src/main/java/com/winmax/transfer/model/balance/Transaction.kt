package com.winmax.transfer.model.balance

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Transaction {
    @SerializedName("Date")
    @Expose
    var date: String? = null

    @SerializedName("Description")
    @Expose
    var description: String? = null

    @SerializedName("Debit")
    @Expose
    var debit: String? = null

    @SerializedName("Credit")
    @Expose
    var credit: String? = null

    @SerializedName("Balance")
    @Expose
    var balance: String? = null

}