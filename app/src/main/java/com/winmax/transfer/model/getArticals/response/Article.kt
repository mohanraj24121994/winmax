package com.winmax.transfer.model.getArticals.response

import android.os.Parcelable
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Article(
        @SerializedName("ArticleCode")
        @Expose
        val articleCode: String? = null ,
        @SerializedName("Designation")
        @Expose
        val designation: String? = null ,
        @SerializedName("FamilyCode")
        @Expose
        val familyCode: String? = null ,
        @SerializedName("SubFamilyCode")
        @Expose
        val SubFamilyCode: String? = null ,
        @SerializedName("DiscountLevel")
        @Expose
        val discountLevel: String? = null ,
        @SerializedName("SellUnitCode")
        @Expose
        val sellUnitCode: String? = null ,
        @SerializedName("SAFTType")
        @Expose
        val sAFTType: String? = null ,
        @SerializedName("Prices")
        @Expose
        val prices: Prices? = null ,
        @SerializedName("Stocks")
        @Expose
        val stocks: Stocks? = null ,
        @SerializedName("VatRate")
        @Expose
        val vatRate: Int = 0
) : Parcelable