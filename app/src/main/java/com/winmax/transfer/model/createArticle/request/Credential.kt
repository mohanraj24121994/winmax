package com.winmax.transfer.model.createArticle.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Credential {

    @SerializedName("CompanyCode")
    @Expose
    private var companyCode: String? = null
    @SerializedName("UserLogin")
    @Expose
    private var userLogin: String? = null
    @SerializedName("UserPassword")
    @Expose
    private var userPassword: String? = null
    @SerializedName("WebServiceURL")
    @Expose
    private var webServiceURL: String? = null

    fun getCompanyCode(): String? {
        return companyCode
    }

    fun setCompanyCode(companyCode: String?) {
        this.companyCode = companyCode
    }

    fun getUserLogin(): String? {
        return userLogin
    }

    fun setUserLogin(userLogin: String?) {
        this.userLogin = userLogin
    }

    fun getUserPassword(): String? {
        return userPassword
    }

    fun setUserPassword(userPassword: String?) {
        this.userPassword = userPassword
    }

    fun getWebServiceURL(): String? {
        return webServiceURL
    }

    fun setWebServiceURL(webServiceURL: String?) {
        this.webServiceURL = webServiceURL
    }




}