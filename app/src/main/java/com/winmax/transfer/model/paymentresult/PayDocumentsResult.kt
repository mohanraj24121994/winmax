package com.winmax.transfer.model.paymentresult

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class PayDocumentsResult {

    @SerializedName("Code")
    @Expose
    private var code: String? = null
    @SerializedName("Message")
    @Expose
    private var message: String? = null
    @SerializedName("Documents")
    @Expose
    private var documents: Documents? = null

    fun getCode(): String? {
        return code
    }

    fun setCode(code: String?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getDocuments(): Documents? {
        return documents
    }

    fun setDocuments(documents: Documents?) {
        this.documents = documents
    }
}