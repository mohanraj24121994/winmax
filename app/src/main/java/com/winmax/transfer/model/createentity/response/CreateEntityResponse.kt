package com.winmax.transfer.model.createentity.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class CreateEntityResponse {
    @SerializedName("winmax4Entity")
    @Expose
    var winmax4Entity: Winmax4Entity? = null
}