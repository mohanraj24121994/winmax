package com.winmax.transfer.model.authenticateUser.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class UserAccount {
    @SerializedName("UserLogin")
    @Expose
    private var userLogin: String? = null
    @SerializedName("UserName")
    @Expose
    private var userName: String? = null
    @SerializedName("Email")
    @Expose
    private var email: String? = null
    @SerializedName("Phone")
    @Expose
    private var phone: String? = null
    @SerializedName("SalesPersonCode")
    @Expose
    private var salesPersonCode: String? = null
    @SerializedName("UserGroupDesignation")
    @Expose
    private var userGroupDesignation: String? = null
    @SerializedName("AccessOptions")
    @Expose
    private var accessOptions: AccessOptions? = null

    fun getUserLogin(): String? {
        return userLogin
    }

    fun setUserLogin(userLogin: String?) {
        this.userLogin = userLogin
    }

    fun getUserName(): String? {
        return userName
    }

    fun setUserName(userName: String?) {
        this.userName = userName
    }

    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String?) {
        this.email = email
    }

    fun getPhone(): String? {
        return phone
    }

    fun setPhone(phone: String?) {
        this.phone = phone
    }

    fun getSalesPersonCode(): String? {
        return salesPersonCode
    }

    fun setSalesPersonCode(salesPersonCode: String?) {
        this.salesPersonCode = salesPersonCode
    }

    fun getUserGroupDesignation(): String? {
        return userGroupDesignation
    }

    fun setUserGroupDesignation(userGroupDesignation: String?) {
        this.userGroupDesignation = userGroupDesignation
    }

    fun getAccessOptions(): AccessOptions? {
        return accessOptions
    }

    fun setAccessOptions(accessOptions: AccessOptions?) {
        this.accessOptions = accessOptions
    }
}