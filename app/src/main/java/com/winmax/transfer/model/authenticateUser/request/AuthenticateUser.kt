package com.winmax.transfer.model.authenticateUser.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class AuthenticateUser {
    @SerializedName("AuthenticationUser")
    @Expose
    private var authenticationUser: String? = null
    @SerializedName("AuthenticationPassword")
    @Expose
    private var authenticationPassword: String? = null
    @SerializedName("OptionsToCheckAccess")
    @Expose
    private var optionsToCheckAccess: String? = null

    fun getAuthenticationUser(): String? {
        return authenticationUser
    }

    fun setAuthenticationUser(authenticationUser: String?) {
        this.authenticationUser = authenticationUser
    }

    fun getAuthenticationPassword(): String? {
        return authenticationPassword
    }

    fun setAuthenticationPassword(authenticationPassword: String?) {
        this.authenticationPassword = authenticationPassword
    }

    fun getOptionsToCheckAccess(): String? {
        return optionsToCheckAccess
    }

    fun setOptionsToCheckAccess(optionsToCheckAccess: String?) {
        this.optionsToCheckAccess = optionsToCheckAccess
    }
}