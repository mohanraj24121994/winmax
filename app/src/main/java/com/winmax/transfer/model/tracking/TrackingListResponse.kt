package com.winmax.transfer.model.tracking

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TrackingListResponse {
    @SerializedName("EntityName")
    @Expose
    var entityName: String? = null

    @SerializedName("TypedArticleCode")
    @Expose
    var typedArticleCode: String? = null

    @SerializedName("SalesPersonId")
    @Expose
    var salesPersonId: Int? = null

    @SerializedName("EntityAddress")
    @Expose
    var entityAddress: String? = null

    @SerializedName("EntityLocation")
    @Expose
    var entityLocation: String? = null

    @SerializedName("EntityZipCode")
    @Expose
    var entityZipCode: String? = null

    @SerializedName("Remarks")
    @Expose
    var remarks: String? = null

    @SerializedName("Code")
    @Expose
    var code: Int? = null

    @SerializedName("DocumentNumber")
    @Expose
    var documentNumber: String? = null

    @SerializedName("ShippingTypeCode")
    @Expose
    var shippingTypeCode: String? = null

    @SerializedName("ShippingTypeID")
    @Expose
    var shippingTypeID: Int? = null

    @SerializedName("DriverName")
    @Expose
    var driverName: String? = null

    @SerializedName("StateOforder")
    @Expose
    var stateOforder: String? = null

    @SerializedName("PhoneNumber")
    @Expose
    var phoneNumber: String? = null

    @SerializedName("ItemDescription")
    @Expose
    var itemDescription: String? = null

    @SerializedName("DocDateTime")
    @Expose
    var docDateTime: String? = null

}