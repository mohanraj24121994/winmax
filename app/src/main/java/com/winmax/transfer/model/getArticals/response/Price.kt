package com.winmax.transfer.model.getArticals.response

import android.os.Parcelable
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Price(

        //without tax
        @SerializedName("CurrencyCode")
        @Expose
        val currencyCode: String? = null,
        @SerializedName("GroupPrice")
        @Expose
        val groupPrice: String? = null,
        @SerializedName("SalesPrice1WithoutTaxesFees")
        @Expose
        val salesPrice1WithoutTaxesFees: String? = "-",
        @SerializedName("SalesPrice2WithoutTaxesFees")
        @Expose
        val salesPrice2WithoutTaxesFees: String? = "-",
        @SerializedName("SalesPrice3WithoutTaxesFees")
        @Expose
        val salesPrice3WithoutTaxesFees: String? = "-",
        @SerializedName("SalesPrice4WithoutTaxesFees")
        @Expose
        val salesPrice4WithoutTaxesFees: String? = "-",
        @SerializedName("SalesPrice5WithoutTaxesFees")
        @Expose
        val salesPrice5WithoutTaxesFees: String? = "-",
        @SerializedName("SalesPrice6WithoutTaxesFees")
        @Expose
        val salesPrice6WithoutTaxesFees: String? = "-",
        @SerializedName("SalesPrice7WithoutTaxesFees")
        @Expose
        val salesPrice7WithoutTaxesFees: String? = "-",
        @SerializedName("SalesPrice8WithoutTaxesFees")
        @Expose
        val salesPrice8WithoutTaxesFees: String? = "-",
        @SerializedName("SalesPrice9WithoutTaxesFees")
        @Expose
        val salesPrice9WithoutTaxesFees: String? = "-",


        //with tax
        @SerializedName("SalesPrice1WithTaxesFees")
        @Expose
        val salesPrice1WithTaxesFees: String? = "-",
        @SerializedName("SalesPrice2WithTaxesFees")
        @Expose
        val salesPrice2WithTaxesFees: String? = "-",
        @SerializedName("SalesPrice3WithTaxesFees")
        @Expose
        val salesPrice3WithTaxesFees: String? = "-",
        @SerializedName("SalesPrice4WithTaxesFees")
        @Expose
        val salesPrice4WithTaxesFees: String? = "-",
        @SerializedName("SalesPrice5WithTaxesFees")
        @Expose
        val salesPrice5WithTaxesFees: String? = "-",
        @SerializedName("SalesPrice6WithTaxesFees")
        @Expose
        val salesPrice6WithTaxesFees: String? = "-",
        @SerializedName("SalesPrice7WithTaxesFees")
        @Expose
        val salesPrice7WithTaxesFees: String? = "-",
        @SerializedName("SalesPrice8WithTaxesFees")
        @Expose
        val salesPrice8WithTaxesFees: String? = "-",
        @SerializedName("SalesPrice9WithTaxesFees")
        @Expose
        val salesPrice9WithTaxesFees: String? = "-",
        @SerializedName("GrossCostPrice")
        @Expose
        val GrossCostPrice: String? = "-",
        @SerializedName("NetCostPrice")
        @Expose
        val NetCostPrice: String? = "-"

) : Parcelable