package com.winmax.transfer.model.family

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class FamilyResponse {

    @SerializedName("GetFamiliesResult")
    @Expose
     val getFamiliesResult: GetFamiliesResult? = null
}