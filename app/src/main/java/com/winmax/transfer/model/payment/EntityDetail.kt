package com.winmax.transfer.model.payment

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class EntityDetail
{
    @SerializedName("GetEntitiesResult")
    @Expose
    private var getEntitiesResult: GetEntitiesResult? = null

    fun getGetEntitiesResult(): GetEntitiesResult? {
        return getEntitiesResult
    }

    fun setGetEntitiesResult(getEntitiesResult: GetEntitiesResult?) {
        this.getEntitiesResult = getEntitiesResult
    }
}