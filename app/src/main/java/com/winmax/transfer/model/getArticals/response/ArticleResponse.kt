package com.winmax.transfer.model.getArticals.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class ArticleResponse {


    @SerializedName("GetArticlesResult")
    @Expose
     val getArticlesResult: GetArticlesResult? = null
}