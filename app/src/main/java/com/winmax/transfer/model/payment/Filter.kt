package com.winmax.transfer.model.payment

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Filter {
    @SerializedName("PageNumber")
    @Expose
    private var pageNumber: String? = null
    @SerializedName("TotalPages")
    @Expose
    private var totalPages: String? = null

    fun getPageNumber(): String? {
        return pageNumber
    }

    fun setPageNumber(pageNumber: String?) {
        this.pageNumber = pageNumber
    }

    fun getTotalPages(): String? {
        return totalPages
    }

    fun setTotalPages(totalPages: String?) {
        this.totalPages = totalPages
    }
}