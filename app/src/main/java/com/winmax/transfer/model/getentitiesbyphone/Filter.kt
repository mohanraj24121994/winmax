package com.winmax.transfer.model.getentitiesbyphone

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Filter (
    @SerializedName("PageNumber")
    @Expose
    var pageNumber: String? = null,

    @SerializedName("TotalPages")
    @Expose
    var totalPages: String? = null
)