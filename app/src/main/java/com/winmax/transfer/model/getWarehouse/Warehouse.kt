package com.winmax.transfer.model.getWarehouse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Warehouse {

    @SerializedName("Code")
    @Expose
    private var code: String? = null
    @SerializedName("Designation")
    @Expose
    private var designation: String? = null

    fun getCode(): String? {
        return code
    }

    fun setCode(code: String?) {
        this.code = code
    }

    fun getDesignation(): String? {
        return designation
    }

    fun setDesignation(designation: String?) {
        this.designation = designation
    }
}