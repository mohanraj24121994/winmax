package com.winmax.transfer.model.getentitiesbyphone.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetEntitiesByPhoneRequest {
    @SerializedName("CompanyCode")
    @Expose
    var companyCode: String? = null
    @SerializedName("UserLogin")
    @Expose
    var userLogin: String? = null
    @SerializedName("UserPassword")
    @Expose
    var userPassword: String? = null
    @SerializedName("WebServiceURL")
    @Expose
    var webServiceURL: String? = null
    @SerializedName("PhoneNumber")
    @Expose
    var PhoneNumber: String? = null
}