package com.winmax.transfer.model.getShippingTypesResult

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class GetShippingTypesResult {
    @SerializedName("Code")
    @Expose
    private var code: String? = null

    @SerializedName("Message")
    @Expose
    private var message: String? = null

    @SerializedName("Filter")
    @Expose
    private var filter: Filter? = null

    @SerializedName("ShippingTypes")
    @Expose
    private var shippingTypes: ShippingTypes? = null

    fun getCode(): String? {
        return code
    }

    fun setCode(code: String?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getFilter(): Filter? {
        return filter
    }

    fun setFilter(filter: Filter?) {
        this.filter = filter
    }

    fun getShippingTypes(): ShippingTypes? {
        return shippingTypes
    }

    fun setShippingTypes(shippingTypes: ShippingTypes?) {
        this.shippingTypes = shippingTypes
    }
}