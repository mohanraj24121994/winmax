package com.winmax.transfer.model.createArticle.response

import android.telecom.Call.Details

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Winmax4Document {
    @SerializedName("IsCreated")
    @Expose
     val isCreated: String? = null
    @SerializedName("Header")
    @Expose
     val header: Header? = null
//    @SerializedName("Details")
//    @Expose
//     val details: com.winmax.transfer.model.createArticle.response.Details? = null
}