package com.winmax.transfer.model.getItemCode

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GetArticlesResult {
    @SerializedName("Code")
    @Expose
    var code: String? = null

    @SerializedName("Message")
    @Expose
    var message: String? = null

    @SerializedName("Filter")
    @Expose
    var filter: Filter? = null

    @SerializedName("Articles")
    @Expose
    var articles: Articles? = null

}