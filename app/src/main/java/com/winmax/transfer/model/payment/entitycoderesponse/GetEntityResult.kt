package com.winmax.transfer.model.payment.entitycoderesponse

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class GetEntityResult {
    @SerializedName("Code")
    @Expose
    private var code: String? = null
    @SerializedName("Message")
    @Expose
    private var message: String? = null
    @SerializedName("Entity")
    @Expose
    private var entity: Entity? = null

    fun getCode(): String? {
        return code
    }

    fun setCode(code: String?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getEntity(): Entity? {
        return entity
    }

    fun setEntity(entity: Entity?) {
        this.entity = entity
    }

}