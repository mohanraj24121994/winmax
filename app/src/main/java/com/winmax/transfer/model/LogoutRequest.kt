package com.winmax.transfer.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LogoutRequest {
    @SerializedName("Database")
    @Expose
    var database: String? = null
    @SerializedName("Driver")
    @Expose
    var driver: String? = null
}