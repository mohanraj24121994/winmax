package com.winmax.transfer.model.getShippingTypesResult

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class ShippingType {
    @SerializedName("Code")
    @Expose
    private var code: String? = null

    @SerializedName("Designation")
    @Expose
    private var designation: String? = null

    var isCheck: Boolean = false

    fun getCode(): String? {
        return code
    }

    fun setCode(code: String?) {
        this.code = code
    }

    fun getDesignation(): String? {
        return designation
    }

    fun setDesignation(designation: String?) {
        this.designation = designation
    }

}