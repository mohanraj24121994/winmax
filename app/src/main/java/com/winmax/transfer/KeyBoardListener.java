package com.winmax.transfer;

public interface KeyBoardListener {

    void clickDelete();

    void clickDone();

    void clickScan();

    void clickKeyDone();
}


