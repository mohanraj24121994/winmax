package com.winmax.transfer.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.winmax.transfer.InvoiceOrderActivity
import com.winmax.transfer.R
import com.winmax.transfer.model.itemCode.ItemCodeList
import kotlinx.android.synthetic.main.item_code_list.view.*

class ItemCodeListAdapter(private var mContext: Activity? , private var itemList: ArrayList<ItemCodeList>) : RecyclerView.Adapter<ItemCodeListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_code_list , parent , false)
        return ViewHolder(v , mContext)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        holder.bindItems(itemList , position)
    }

    inner class ViewHolder(itemView: View , private var mContext: Activity?) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(itemListData: ArrayList<ItemCodeList> , position: Int) {
            itemView.tvItemCode.text = itemListData.get(position).articleCode + "  " + itemListData.get(position).designation

            itemView.tvItemCode.setOnClickListener {
                val intent = Intent(mContext , InvoiceOrderActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.putExtra("articleCode" , itemListData.get(position).articleCode)
                mContext?.startActivity(intent)
                mContext?.finish()
            }
        }
    }
}