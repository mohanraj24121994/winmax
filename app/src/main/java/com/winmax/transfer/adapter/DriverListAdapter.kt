package com.winmax.transfer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.appcompat.widget.AppCompatCheckBox
import com.winmax.transfer.R
import com.winmax.transfer.model.getShippingTypesResult.ShippingType
import com.winmax.transfer.utils.IsDriverChecked
import java.util.*


class DriverListAdapter(private var driverName: ArrayList<ShippingType> ,
                        var isDriverChecked: IsDriverChecked) : BaseAdapter() {

    override fun getView(position: Int , view: View? , parent: ViewGroup?): View {
        val v = LayoutInflater.from(parent !!.context).inflate(R.layout.spinner_driver_item , parent , false)
        val chkDriverName = v.findViewById<AppCompatCheckBox>(R.id.tvDriverName)
        chkDriverName.text = driverName[position].getDesignation()

        chkDriverName.isChecked = driverName[position].isCheck
        chkDriverName.setOnCheckedChangeListener { p0 , isCheck ->
            driverName.forEach {
                if (it.isCheck) {
                    it.isCheck = false
                }
            }
            notifyDataSetChanged()
            driverName[position].isCheck = isCheck
            isDriverChecked.isDriverChecked(position , isCheck , driverName = driverName[position].getDesignation())
        }
        return v
    }

    override fun getItem(p0: Int): Any {
        return p0
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return driverName.size
    }
}

