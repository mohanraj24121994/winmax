package com.winmax.transfer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.winmax.transfer.R
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.model.getentitiesbyphone.Address
import kotlinx.android.synthetic.main.add_address_item.view.*

class AddressListAdapter(private var mContext: Context? , private var itemList: List<Address> , val listener: (List<Address> , Int , View) -> Unit) : RecyclerView.Adapter<AddressListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.add_address_item , parent , false)
        return ViewHolder(v , mContext)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        holder.bindItems(itemList , listener , position)
    }

    inner class ViewHolder(itemView: View , private var mContext: Context?) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(itemListData: List<Address> , listener: (List<Address> , Int , View) -> Unit , position: Int) {

            itemView.tv_current.text = position.plus(1).toString()
            itemView.tv_total.text = itemListData.size.toString()
            itemView.etAddress.setText(itemListData[position].address)
            itemView.etPostalCode.setText(itemListData[position].zipCode)
            itemView.etLocality.setText(itemListData[position].location)

            if (position == 0) {

                itemView.tvRemarks.visibility = View.VISIBLE
                itemView.etRemarks.visibility = View.VISIBLE

                itemView.etDesignation.visibility = View.GONE
                itemView.tvetDesignation.visibility = View.GONE

                itemView.etRemarks.setText(itemListData[position].remarks)
            } else {
                itemView.tvRemarks.visibility = View.GONE
                itemView.etRemarks.visibility = View.GONE

                itemView.etDesignation.visibility = View.VISIBLE
                itemView.tvetDesignation.visibility = View.VISIBLE

                itemView.etDesignation.setText(itemListData[position].designation)
            }

           /* if (itemListData.size >= 5) {
                itemView.tvAdd.visibility = View.GONE
            }*/

            itemView.tvAdd.setOnClickListener {
                listener(itemListData , position , it)
            }

            itemView.tvEdit.setOnClickListener {
                listener(itemListData , position , it)
            }
        }
    }
}