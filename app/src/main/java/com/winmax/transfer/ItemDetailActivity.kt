package com.winmax.transfer

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.model.getArticals.response.ArticleResponse
import com.winmax.transfer.model.getArticals.response.GetArticlesResult
import com.winmax.transfer.model.getArticals.response.Prices
import com.winmax.transfer.model.getArticals.response.Stocks
import com.winmax.transfer.model.webserviceConnection.WebTestRequest
import com.winmax.transfer.webservices.ApiClient
import kotlinx.android.synthetic.main.activity_item_detail.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ItemDetailActivity : AppCompatActivity() {

    var itemCode:String?=""
    var mContext:Context?=null
    var articalRes: GetArticlesResult? = null
    var mPricesList : Prices?=null
    var mStocksList: Stocks?=null
    var detailAdapter:ItemDetailAdapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)

        inItViews()

    }

    private fun inItViews() {
        mContext= this
        itemCode = intent.getStringExtra("select_item_code")

        itemCode?.let { callApi(it) }
    }

    private fun callApi(itemCode: String) {

        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = WebTestRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)

        val call = ApiClient.getService().getArticle(itemCode, request)

        call.enqueue(object : Callback<ArticleResponse> {
            override fun onResponse(call: Call<ArticleResponse>, response: Response<ArticleResponse>) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getArticlesResult?.code.equals("0")) {
                        Constants.shortToast(mContext!!, getString(R.string.success))
                        articalRes = response.body()?.getArticlesResult
                        setData(articalRes)
                    } else if (response.body()?.getArticlesResult?.code.equals("5")) {
                        Constants.shortToast(mContext!!, "Invalid Item Code!!")
                    }
                } else {
                    Constants.shortToast(mContext!!, getString(R.string.failure))
                }
            }

            override fun onFailure(call: Call<ArticleResponse>, t: Throwable) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext!!, getString(R.string.something_went_wrong))
            }
        })
    }

    private fun setData(articalRes: GetArticlesResult?) {
        item_code.text = articalRes?.articles?.article?.articleCode
        item_name.text = articalRes?.articles?.article?.designation
        //it_net.text = articalRes?.articles?.article?.prices?.price?.salesPrice1WithTaxesFees
    }
}
