package com.winmax.transfer

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.google.gson.internal.LinkedTreeMap
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.downloader.FileDownloader
import com.winmax.transfer.model.balance.GetBalanceAmount
import com.winmax.transfer.model.getentitiesbyphone.GetEntitiesByPhone
import com.winmax.transfer.model.getentitiesbyphone.request.GetEntitiesByPhoneRequest
import com.winmax.transfer.model.getentitydescription.GetEntityDescription
import com.winmax.transfer.model.getentitydescription.PhoneNumberRequest
import com.winmax.transfer.model.payment.Entity
import com.winmax.transfer.model.payment.EntityDetail
import com.winmax.transfer.model.payment.entitycoderequest.EntityCodeRequest
import com.winmax.transfer.model.payment.entitycoderesponse.EntityCodeDetail
import com.winmax.transfer.model.payment.entityrequest.EntityRequest
import com.winmax.transfer.model.paymentresult.PaymentResultResponse
import com.winmax.transfer.model.paymentresult.paymentrequest.Credential
import com.winmax.transfer.model.paymentresult.paymentrequest.CredentialRequest
import com.winmax.transfer.model.paymentresult.paymentrequest.PayDocumentRequest
import com.winmax.transfer.utils.CirclesLoadingView
import com.winmax.transfer.webservices.ApiClient
import kotlinx.android.synthetic.main.activity_payment.*
import org.apache.commons.io.FileUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PaymentActivity : AppCompatActivity() , View.OnClickListener {

    private var mContext: Context? = null
    var entityData = ArrayList<Entity>()
    var entityspData: ArrayList<String> = ArrayList()

    private var mPhotoFile: File? = null
    private var mPhotoFile1: File? = null
    private var mPhotoFile2: File? = null
    private var mPhotoFile3: File? = null
    private var mPhotoFile4: File? = null
    private val GALLERY = 1
    private val CAMERA = 2

    private var mChequeFile: ArrayList<File>? = null

    private var uri: Uri? = null

    var entityCode: String? = null
    var entityType: String? = null
    var filename: String? = null
    var isLimitExceed: Boolean = false
    private var isAPICAll: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        mContext = this

        onClickListener()

        if (Constants.isNetworkAvailable(mContext !!)) {
            //getEntityDetail()
            /* et_entity.setOnEditorActionListener { v , actionId , event ->
                 if (actionId == EditorInfo.IME_ACTION_DONE) {
                     if (et_entity.text?.length == 0) {
                         Constants.shortToast(mContext !! , getString(R.string.choose_entity))
                     } else {
                         getEntityByCode(et_entity.text.toString())
                     }
                 }
                 return@setOnEditorActionListener false
             }*/

            sp_entity.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {

                }

                override fun beforeTextChanged(s: CharSequence? , start: Int , count: Int , after: Int) {
                }

                override fun onTextChanged(s: CharSequence? , start: Int , before: Int , count: Int) {
                    if (s?.length ?: 0 <= 5) {
                        // tv_entity.visibility = View.GONE
                        tv_balance.visibility = View.GONE
                        btnPay.isEnabled = false
                        btnPay.alpha = 0.5f
                    }
                }
            })

            getEntityDescriptionList()
        } else {
            Constants.shortToast(mContext !! , getString(R.string.check_internet))
        }

        setVersion()

        cashOrChequeVisible()

        checkPermission(true)

        val isCheckPhotoReturn = PrefManager(applicationContext).getBooleanValue("isCheckPhotoReturn")
        if (isCheckPhotoReturn) {
            val code = PrefManager(mContext).getStringValue("entityCode")
            val name = PrefManager(mContext).getStringValue("entityName")
            val amount = PrefManager(mContext).getStringValue("Amount")
            val balance = PrefManager(mContext).getStringValue("balance")
            entityType = PrefManager(mContext).getStringValue("entityType")

            if (! code.isNullOrEmpty()) {
                et_entity.setText(code)
            }
            if (! name.isNullOrEmpty()) {
                // tv_entity.visibility = View.VISIBLE
                sp_entity.setText(name)
            }
            if (! amount.isNullOrEmpty()) {
                amount_paid_et.setText(amount)
            }

            if (! balance.isNullOrEmpty()) {
                tv_balance.visibility = View.VISIBLE
                tv_balance.text = balance
            }

            if (! code.isNullOrEmpty() && ! name.isNullOrEmpty()) {
                btnPay.isEnabled = true
                btnPay.alpha = 1.0f
            }

            rbtnCheque.isChecked = true
            ll_cheque.visibility = View.VISIBLE

            PrefManager(applicationContext).setBooleanValue("isCheckPhotoReturn" , false)
        }

        //mChequeFile = ArrayList()
    }

    private fun getEntityDescriptionList() {
        circle_load.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = EntityRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)

        val call = ApiClient.getService().getEntityDescription(request)
        call.enqueue(object : Callback<ArrayList<GetEntityDescription>> {
            override fun onFailure(call: Call<ArrayList<GetEntityDescription>> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , getString(R.string.failure))
            }

            override fun onResponse(call: Call<ArrayList<GetEntityDescription>> , response: Response<ArrayList<GetEntityDescription>>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        Singleton.instance.entityDescription.clear()
                        Singleton.instance.entityDescription.addAll(response.body() !!)

                        if (Singleton.instance.entityDescription.size > 0) {
                            loadEntityDescription(Singleton.instance.entityDescription)
                        }
                    } else {
                        Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                    }

                } else {
                    Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                }
            }
        })
    }

    private fun loadEntityDescription(entityDescription: ArrayList<GetEntityDescription>) {
        val spData: ArrayList<String> = ArrayList()
        for (i in 0 until entityDescription.size) {
            spData.add(entityDescription[i].entityName?.replace("(" , "( ") ?: "")
        }

        entityspData.clear()
        entityspData.addAll(spData)

        val arrayAdapter = ArrayAdapter(this , R.layout.spinner_item , entityspData)
        sp_entity.threshold = 1
        sp_entity.setAdapter(arrayAdapter)

        sp_entity.onItemClickListener = AdapterView.OnItemClickListener { _ , _ , position , _ ->
            val name = arrayAdapter.getItem(position)
            Singleton.instance.entityDescription.forEach {
                if (it.entityName?.replace("(" , "( ").equals(name)) {
                    val getEntityCode = it.code
                    PrefManager(mContext).setValue("entityCode" , getEntityCode.toString())
                    getEntityByCode(getEntityCode.toString())
                }
            }
        }
    }

    private fun getPhoneNumber(entityCode: Int) {
        circle_load.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = PhoneNumberRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)
        request.EntityCode = entityCode

        val call = ApiClient.getService().getPhoneNumber(request)
        call.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , getString(R.string.failure))
            }

            override fun onResponse(call: Call<String> , response: Response<String>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        //view_edit_text_phone_num.setText(response.body().toString())
                        getEntityByPhoneNumber(response.body().toString())
                    } else {
                        Constants.shortToast(mContext !! , response.message())
                    }
                }
            }
        })
    }

    private fun getEntityByPhoneNumber(phoneNumber: String) {
        circle_load.visibility = View.VISIBLE

        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = GetEntitiesByPhoneRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)
        request.PhoneNumber = phoneNumber

        val call = ApiClient.getService().getEntityByPhoneNumber(request)
        call.enqueue(object : Callback<GetEntitiesByPhone> {

            override fun onResponse(call: Call<GetEntitiesByPhone> , response: Response<GetEntitiesByPhone>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    isAPICAll = true
                    if (response.body() !!.getEntitiesResult !!.code.equals("0") ||
                            response.body() !!.getEntitiesResult !!.code == null) {
                        if (response.body() !!.getEntitiesResult != null) {
                            if (response.body() !!.getEntitiesResult !!.entities != null) {
                                // et_entity.setText(response.body()?.getEntitiesResult?.entities?.entity?.code)
                                getEntityByCode(response.body()?.getEntitiesResult?.entities?.entity?.code
                                        ?: "")
                            }
                        }
                    } else {
                        Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                    }
                } else {
                    Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<GetEntitiesByPhone> , t: Throwable) {
                isAPICAll = true
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            }
        })
    }

    private fun cashOrChequeVisible() {
        rg_pay.setOnCheckedChangeListener { group , checkedId ->
            val rbChecked = rg_pay.findViewById<View>(checkedId)
            if (rbChecked.id == R.id.rbt_cash) {
                ll_cheque.visibility = View.GONE
            } else {
                ll_cheque.visibility = View.VISIBLE
            }
        }
    }

    private fun getEntityDetail() {
        circle_load.visibility = View.VISIBLE

        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = EntityRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)

        val call = ApiClient.getService().getEntityDetail(request)
        call.enqueue(object : Callback<EntityDetail> {
            override fun onResponse(call: Call<EntityDetail> , response: Response<EntityDetail>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getGetEntitiesResult()?.getCode().equals("0")) {
                        for (i in 0 until response.body() !!.getGetEntitiesResult()?.getEntities()?.getEntity()?.size !!) {
                            val entity = Entity()
                            entity.setCode(response.body() !!.getGetEntitiesResult()?.getEntities()?.getEntity()?.get(i)?.getCode())
                            entity.setEntityType(response.body() !!.getGetEntitiesResult()?.getEntities()?.getEntity()?.get(i)?.getEntityType())
                            entity.setName(response.body() !!.getGetEntitiesResult()?.getEntities()?.getEntity()?.get(i)?.getName())
                            entityData.add(entity)
                        }

                        if (Singleton.instance.entityData.size > 0) {
                            Singleton.instance.entityData.clear()
                        }
                        Singleton.instance.entityData = entityData
                        loadtoSpinner(Singleton.instance.entityData)
                    } else {
                        Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                    }
                } else {
                    Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<EntityDetail> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , getString(R.string.failure))
            }
        })
    }

    private fun getEntityByCode(entityCod: String) {
        circle_load.visibility = View.VISIBLE

        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = EntityCodeRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)
        request.EntityCode = entityCod

        val call = ApiClient.getService().getEntityCodeDetail(request)
        call.enqueue(object : Callback<EntityCodeDetail> {
            override fun onFailure(call: Call<EntityCodeDetail> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , getString(R.string.failure))
            }

            override fun onResponse(call: Call<EntityCodeDetail> , response: Response<EntityCodeDetail>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getGetEntityResult()?.getCode().equals("0")) {
                        // tv_entity.visibility = View.VISIBLE
                        sp_entity.setText(response.body()?.getGetEntityResult()?.getEntity()?.getName() !!)
                        sp_entity.setSelection(response.body()?.getGetEntityResult()?.getEntity()?.getName() !!.length)

                        entityCode = response.body()?.getGetEntityResult()?.getEntity()?.getCode() !!
                        entityType = response.body()?.getGetEntityResult()?.getEntity()?.getEntityType() !!

                        if (entityCode != null && entityType != null) {
                            btnPay.isEnabled = true
                            btnPay.alpha = 1.0f
                        }

                        getBalanceAmountAPI(entityCod)

                    } else {
                        Constants.shortToast(mContext !! , response.body()?.getGetEntityResult()?.getMessage() !!)
                    }
                } else {
                    Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                }
            }
        })

    }

    private fun getBalanceAmountAPI(entityCod: String) {
        circle_load.visibility = View.VISIBLE

        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = EntityCodeRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)
        request.EntityCode = entityCod

        val call = ApiClient.getService().getBalanceAmount(request)
        call.enqueue(object : Callback<GetBalanceAmount> {
            override fun onFailure(call: Call<GetBalanceAmount> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            }

            override fun onResponse(call: Call<GetBalanceAmount> , response: Response<GetBalanceAmount>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getEntityCurrentAccount?.code.equals("0")) {
                        tv_balance.visibility = View.VISIBLE
                        response.body()?.getEntityCurrentAccount?.entityCurrentAccount?.transactions?.transaction.let {
                            if (it is List<*>) {
                                it.forEach {
                                    val bal = it as LinkedTreeMap<* , *>
                                    tv_balance.text = "".plus("Balance: €").plus(bal["Balance"])
                                }
                            } else {
                                val bal = it as LinkedTreeMap<* , *>
                                tv_balance.text = "".plus("Balance: €").plus(bal["Balance"])
                            }
                        }
                    } else {
                        Constants.shortToast(mContext !! , response.body()?.getEntityCurrentAccount?.message
                                ?: "")
                    }
                } else {
                    Constants.shortToast(mContext !! , response.body()?.getEntityCurrentAccount?.message
                            ?: "")
                }
            }
        })
    }

    private fun callServicePay() {
        circle_load.visibility = View.VISIBLE

        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = CredentialRequest()
        request.credential = Credential()
        request.PayDocument = PayDocumentRequest()

        request.credential?.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.credential?.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.credential?.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.credential?.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)

        request.PayDocument?.entityType = entityType
        request.PayDocument?.entityCode = PrefManager(mContext).getStringValue("entityCode")
        request.PayDocument?.paymentType = "Unique"
        request.PayDocument?.value = amount_paid_et.text.toString()

        val call = ApiClient.getService().paymentDetail(request)
        call.enqueue(object : Callback<PaymentResultResponse> {

            override fun onResponse(call: Call<PaymentResultResponse> , response: Response<PaymentResultResponse>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getPayDocumentsResult()?.getCode().equals("0")) {

                        val c = Calendar.getInstance()
                        val dateformat = SimpleDateFormat(":dd:MM:yyyy:hh:mm:ss")
                        val datetime = dateformat.format(c.time)
                        filename = "W4 Rec " + sp_entity.text.toString() + datetime

                        val pdfURL = PrefManager(mContext).getStringValue(Constants.pdfURL)

                        if (mPhotoFile1 != null && mPhotoFile2 == null && mPhotoFile3 == null && mPhotoFile4 == null) {
                            DownloadFile(this@PaymentActivity , mPhotoFile1 , null , null , null , null , circle_load).execute(pdfURL + response.body()?.getPayDocumentsResult()?.getDocuments()?.getDocument()?.getArchivedDocumentHTTPPath()?.trim() , filename + ".pdf")
                        } else if (mPhotoFile1 != null && mPhotoFile2 != null && mPhotoFile3 == null && mPhotoFile4 == null) {
                            DownloadFile(this@PaymentActivity , mPhotoFile1 , mPhotoFile2 !! , null , null , null , circle_load).execute(pdfURL + response.body()?.getPayDocumentsResult()?.getDocuments()?.getDocument()?.getArchivedDocumentHTTPPath()?.trim() , filename + ".pdf")
                        } else if (mPhotoFile1 != null && mPhotoFile2 != null && mPhotoFile3 != null && mPhotoFile4 == null) {
                            DownloadFile(this@PaymentActivity , mPhotoFile1 !! , mPhotoFile2 , mPhotoFile3 , null , null , circle_load).execute(pdfURL + response.body()?.getPayDocumentsResult()?.getDocuments()?.getDocument()?.getArchivedDocumentHTTPPath()?.trim() , filename + ".pdf")
                        } else if (mPhotoFile1 != null && mPhotoFile2 != null && mPhotoFile3 != null && mPhotoFile4 != null) {
                            DownloadFile(this@PaymentActivity , mPhotoFile1 !! , mPhotoFile2 , mPhotoFile3 , mPhotoFile4 , null , circle_load).execute(pdfURL + response.body()?.getPayDocumentsResult()?.getDocuments()?.getDocument()?.getArchivedDocumentHTTPPath()?.trim() , filename + ".pdf")
                        } else if (mPhotoFile1 != null && mPhotoFile2 != null && mPhotoFile3 != null && mPhotoFile4 != null && mPhotoFile != null) {
                            DownloadFile(this@PaymentActivity , mPhotoFile1 !! , mPhotoFile2 , mPhotoFile3 , mPhotoFile4 , mPhotoFile , circle_load).execute(pdfURL + response.body()?.getPayDocumentsResult()?.getDocuments()?.getDocument()?.getArchivedDocumentHTTPPath()?.trim() , filename + ".pdf")
                        } else {
                            DownloadFile(this@PaymentActivity , null , null , null , null , null , circle_load).execute(pdfURL + response.body()?.getPayDocumentsResult()?.getDocuments()?.getDocument()?.getArchivedDocumentHTTPPath() , "$filename.pdf")
                        }
                    } else {
                        Constants.shortToast(mContext !! , response.body()?.getPayDocumentsResult()?.getMessage() !!)
                    }
                } else {
                    Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<PaymentResultResponse> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , getString(R.string.failure))
            }
        })
    }


    private fun loadtoSpinner(entityData: ArrayList<Entity>) {

        val spData: ArrayList<String> = ArrayList()
        for (i in 0 until entityData.size) {
            spData.add(entityData[i].getName() !!)
        }

        entityspData.clear()
        entityspData.addAll(spData)

        entityspData.add(0 , "Choose Entity")

        val arrayAdapter = ArrayAdapter(this , R.layout.spinner_item , entityspData)
        sp_selec.threshold = 1
        sp_selec.setAdapter(arrayAdapter)

        sp_selec.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>? , view: View? , position: Int , id: Long) {
                entityCode = entityData[position].getCode()
                entityType = entityData[position].getEntityType()
            }

        }
    }

    private fun setVersion() {
        tvVersion.text = PrefManager(applicationContext).getStringValue(Constants.versionConst)
    }

    private fun onClickListener() {

        btnPay.isEnabled = false
        btnPay.alpha = 0.5f

        ivBack.setOnClickListener(this)
        ivLogout.setOnClickListener(this)
        btnPay.setOnClickListener(this)
        ll_cheque.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivBack -> {
                onBackPressed()
            }
            R.id.ivLogout -> {
                PrefManager(mContext).setBooleanValue("isLoggedIn" , false)
                PrefManager(mContext).isFirstTimeLaunch = false
                val to = Intent(this , MainActivity::class.java)
                startActivity(to)
                overridePendingTransition(R.anim.enter , R.anim.exit)
                finish()
            }
            R.id.btnPay -> {
                if (checkWriteExternalPermission() && checkCameraPermission()) {
                    onClickButtonPay()
                } else {
                    checkPermission(true)
                }
            }
            R.id.ll_cheque -> {
                //PrefManager(mContext).setValue("entityCode" , et_entity.text.toString())
                PrefManager(mContext).setValue("entityName" , tv_entity.text.toString())
                PrefManager(mContext).setValue("entityType" , entityType)
                PrefManager(mContext).setValue("Amount" , amount_paid_et.text.toString())
                PrefManager(mContext).setValue("balance" , tv_balance.text.toString())

                if (! isLimitExceed) {
                    showDialog()
                } else {
                    Toast.makeText(applicationContext , "Cannot add more than 5" , Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun checkWriteExternalPermission(): Boolean {
        val permission = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val res: Int = checkCallingOrSelfPermission(permission)
        return res == PackageManager.PERMISSION_GRANTED
    }

    private fun checkCameraPermission(): Boolean {
        val permission = Manifest.permission.CAMERA
        val res: Int = checkCallingOrSelfPermission(permission)
        return res == PackageManager.PERMISSION_GRANTED
    }

    private fun clearValue() {
        et_entity.setText("")
        tv_entity.visibility = View.GONE
        ll_cheque.visibility = View.GONE
        amount_paid_et.setText("")

        btnPay.isEnabled = false
        btnPay.alpha = 0.5f
        rbt_cash.isChecked = false
        rbtnCheque.isChecked = false
    }

    private fun onClickButtonPay() {
        when {
            /*et_entity.text?.length == 0 -> {
                Constants.shortToast(mContext !! , getString(R.string.choose_entity))
            }*/
            amount_paid_et.text?.length == 0 -> {
                Constants.shortToast(mContext !! , getString(R.string.enter_amount))
            }
            rg_pay.checkedRadioButtonId == - 1 -> {
                Constants.shortToast(mContext !! , getString(R.string.cash_or_cheque))
            }
            /* rg_pay.checkedRadioButtonId == R.id.rbtnCheque -> {
                *//* if (mPhotoFile == null) {
                    Constants.shortToast(mContext !! , getString(R.string.select_image))
                } else {
                    callServicePay()
                }*//*
            }*/
            else -> {
                callServicePay()
            }
        }
    }

    private fun showDialog() {
        val items = arrayOf<CharSequence>(
                resources.getString(R.string.selectAction) ,
                resources.getString(R.string.gallery) ,
                resources.getString(R.string.camera)
        )
        val builder = AlertDialog.Builder(this)
        builder.setItems(items) { dialog , item ->
            when {
                items[item] == resources.getString(R.string.camera) -> {
                    requestStoragePermission(true)
                }
                items[item] == resources.getString(R.string.gallery) -> {
                    requestStoragePermission(false)
                }
                items[item] == resources.getString(R.string.selectAction) -> {
                    dialog.dismiss()
                }
            }
        }
        builder.show()
    }

    private fun checkPermission(isCamera: Boolean) {
        Dexter.withActivity(this).withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE ,
                Manifest.permission.WRITE_EXTERNAL_STORAGE ,
                Manifest.permission.CAMERA
        )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {

                        if (report !!.isAnyPermissionPermanentlyDenied) {
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>? ,
                                                                    token: PermissionToken?) {
                        token?.continuePermissionRequest()
                    }
                }).withErrorListener {
                    Toast.makeText(this , "Error occurred! " , Toast.LENGTH_SHORT).show()
                }.onSameThread().check()
    }

    private fun requestStoragePermission(isCamera: Boolean) {
        Dexter.withActivity(this).withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE ,
                Manifest.permission.WRITE_EXTERNAL_STORAGE ,
                Manifest.permission.CAMERA
        )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report !!.areAllPermissionsGranted()) {
                            if (isCamera) {
                                takePhotoFromCamera()
                            } else {
                                dispatchGalleryIntent()
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied) {
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>? ,
                                                                    token: PermissionToken?) {
                        token?.continuePermissionRequest()
                    }
                }).withErrorListener {
                    Toast.makeText(this , "Error occurred! " , Toast.LENGTH_SHORT).show()
                }.onSameThread().check()
    }

    fun takePhotoFromCamera() {

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            val file = createImageFile()
            val mUri = FileProvider.getUriForFile(
                    this ,
                    "$packageName.fileprovider" , file !!
            )
            mPhotoFile = file
            intent.putExtra(MediaStore.EXTRA_OUTPUT , mUri)
            this.startActivityForResult(intent , CAMERA)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun dispatchGalleryIntent() {
        val pickPhoto = Intent(Intent.ACTION_PICK , MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivityForResult(pickPhoto , GALLERY)
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs camera & storage permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS") { dialog , which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialog , which -> dialog.cancel() }
        builder.show()
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package" , packageName , null)
        intent.data = uri
        startActivityForResult(intent , 101)
    }

    private fun createImageFile(): File? {
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss" , Locale.getDefault()).format(Date())
        val mFileName = "" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(mFileName , ".png" , storageDir)

    }

    override fun onRequestPermissionsResult(requestCode: Int , @NonNull permissions: Array<String> , @NonNull grantResults: IntArray) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this)
            } else {
                Toast.makeText(
                        this , "Cancelling, required permissions are not granted" ,
                        Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun performCrop(selectedImageUri: Uri) {
        CropImage.activity(selectedImageUri)
                .setGuidelines(CropImageView.Guidelines.ON_TOUCH)
                .start(this@PaymentActivity)
    }

    override fun onActivityResult(requestCode: Int , resultCode: Int , data: Intent?) {
        super.onActivityResult(requestCode , resultCode , data)
        var selectedImageUri: Uri?
        if (resultCode != Activity.RESULT_CANCELED) {
            if (requestCode == CAMERA) {
                if (resultCode == Activity.RESULT_OK) {

                    val isCamera: Boolean
                    isCamera = if (data == null) {
                        true
                    } else {
                        val action = data.action
                        if (action == null) {
                            false
                        } else {
                            action == MediaStore.ACTION_IMAGE_CAPTURE
                        }
                    }

                    selectedImageUri = if (isCamera) {
                        Uri.fromFile(mPhotoFile)
                    } else {
                        data !!.data
                    }

                    if (selectedImageUri != null) {
                        performCrop(selectedImageUri)
                    } else {
                        selectedImageUri = Uri.fromFile(mPhotoFile)
                        performCrop(selectedImageUri)
                    }
                }

            } else if (requestCode == GALLERY) {

                if (resultCode == Activity.RESULT_OK) {
                    var selectedImage = data?.data

                    if (selectedImage != null) {
                        performCrop(selectedImage)
                    } else {
                        selectedImage = Uri.fromFile(mPhotoFile)
                        performCrop(selectedImage)
                    }
                }
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode != Activity.RESULT_CANCELED) {
                val result = CropImage.getActivityResult(data)
                uri = result.uri

                if (resultCode == Activity.RESULT_OK) {
                    val cropImageDirectory: File? = if (requestCode == GALLERY) {
                        File(getRealPathFromUri(result.uri))
                    } else {
                        File(result.uri.path)
                    }

                    try {
                        cropImageDirectory !!.createNewFile()
                    } catch (e: IOException) {
                        Log.d("printStackTrace" , e.message , e)
                    }

                    val root = File((Environment.getExternalStorageDirectory()).toString() + File.separator + Constants.DIRECTORY_NAME +
                            File.separator)
                    try {
                        FileUtils.copyFileToDirectory(cropImageDirectory , root)
                    } catch (e: IOException) {
                        Log.d("printStackTrace" , e.message , e)
                    }

                    if (File(root.toString() + File.separator + cropImageDirectory?.name).isFile) {
                        mPhotoFile = File(root.toString() + File.separator + cropImageDirectory?.name)
                        val bitmap = BitmapFactory.decodeFile(mPhotoFile !!.absolutePath)

                        if (ivCheque.drawable == null) {
                            ivCheque.setImageBitmap(bitmap)
                            mPhotoFile1 = mPhotoFile
                        } else if (ivCheque1.drawable == null) {
                            ivCheque1.visibility = View.VISIBLE
                            ivCheque1.setImageBitmap(bitmap)
                            mPhotoFile2 = mPhotoFile
                        } else if (ivCheque2.drawable == null) {
                            ivCheque2.visibility = View.VISIBLE
                            ivCheque2.setImageBitmap(bitmap)
                            mPhotoFile3 = mPhotoFile
                        } else if (ivCheque3.drawable == null) {
                            ivCheque3.visibility = View.VISIBLE
                            ivCheque3.setImageBitmap(bitmap)
                            mPhotoFile4 = mPhotoFile
                        } else if (ivCheque4.drawable == null) {
                            ivCheque4.visibility = View.VISIBLE
                            isLimitExceed = true
                            ivCheque4.setImageBitmap(bitmap)
                        }

                        //mChequeFile !!.add(mPhotoFile !!)
                        tv_addMore.text = "Add more.."
                    }

                }
            } else {
                PrefManager(mContext).setBooleanValue("isCheckPhotoReturn" , true)

                val to = Intent(this , PaymentActivity::class.java)
                startActivity(to)
                finish()
            }
        }
    }

    private fun getRealPathFromUri(contentUri: Uri?): String? {
        var cursor: Cursor? = null
        try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = contentResolver.query(contentUri !! , proj , null , null , null)
            assert(cursor != null)
            val column = cursor !!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(column)
        } finally {
            cursor?.close()

        }
    }

    private class DownloadFile(applicationContext: Activity , file1: File? , file2: File? , file3: File? , file4: File? , file: File? , view: CirclesLoadingView) : AsyncTask<String? , String? , String?>() {

        var folder: File? = null
        var mFile: File? = null
        var mFile1: File? = null
        var mFile2: File? = null
        var mFile3: File? = null
        var mFile4: File? = null

        //val fileCheque: ArrayList<File> = fileCheque

        // private val mContext: Context
        private val activity: Activity = applicationContext
        private var filename: String? = null
        private var pg: CirclesLoadingView? = null

        init {
            mFile = file
            mFile1 = file1
            mFile2 = file2
            mFile3 = file3
            mFile4 = file4
            pg = view
        }

        override fun onPostExecute(file_url: String?) {
            activity.runOnUiThread {
                pg?.visibility = View.GONE
            }
            val intent = Intent(activity , TransactionReceiptActivity::class.java)
            if (mFile1 != null && mFile2 == null && mFile3 == null && mFile4 == null && mFile == null) {
                intent.putExtra("KEY1" , Uri.fromFile(mFile1).toString())
            } else if (mFile1 != null && mFile2 != null && mFile3 == null && mFile4 == null && mFile == null) {
                intent.putExtra("KEY1" , Uri.fromFile(mFile1).toString())
                intent.putExtra("KEY2" , Uri.fromFile(mFile2).toString())
            } else if (mFile1 != null && mFile2 != null && mFile3 != null && mFile4 == null && mFile == null) {
                intent.putExtra("KEY1" , Uri.fromFile(mFile1).toString())
                intent.putExtra("KEY2" , Uri.fromFile(mFile2).toString())
                intent.putExtra("KEY3" , Uri.fromFile(mFile3).toString())
            } else if (mFile1 != null && mFile2 != null && mFile3 != null && mFile4 != null && mFile == null) {
                intent.putExtra("KEY1" , Uri.fromFile(mFile1).toString())
                intent.putExtra("KEY2" , Uri.fromFile(mFile2).toString())
                intent.putExtra("KEY3" , Uri.fromFile(mFile3).toString())
                intent.putExtra("KEY3" , Uri.fromFile(mFile4).toString())
            } else if (mFile1 != null && mFile2 != null && mFile3 != null && mFile4 != null && mFile != null) {
                intent.putExtra("KEY" , Uri.fromFile(mFile).toString())
                intent.putExtra("KEY1" , Uri.fromFile(mFile1).toString())
                intent.putExtra("KEY2" , Uri.fromFile(mFile2).toString())
                intent.putExtra("KEY3" , Uri.fromFile(mFile3).toString())
                intent.putExtra("KEY3" , Uri.fromFile(mFile4).toString())
            }

            intent.putExtra("filename" , filename)
            //   intent.putExtra("cheque" , fileCheque)
            activity.startActivity(intent)
            activity.finish()
        }

        override fun doInBackground(vararg params: String?): String? {
            activity.runOnUiThread {
                pg?.visibility = View.VISIBLE
            }
            val fileUrl = params[0]
            filename = params[1]
            //val extStorageDirectory = Environment.getExternalStorageDirectory().toString()
            folder = File(activity.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) , "Winmax")
            folder?.mkdir()
            val pdfFile = File(folder , filename ?: "")
            try {
                pdfFile.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            FileDownloader.downloadFile(fileUrl , pdfFile)
            return null
        }
    }

    private
    var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this , "Please click BACK again to exit" , Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false } , 2000)
    }

}

