package com.winmax.transfer

import android.app.Application

class WinmaxApplication : Application() {

    companion object {
        private var sInstance: WinmaxApplication? = null
        fun getInstance(): WinmaxApplication? {
            return sInstance
        }
    }

    override fun onCreate() {
        super.onCreate()
    }
}