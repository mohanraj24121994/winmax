package com.winmax.transfer.fcm

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.winmax.transfer.TrackingActivity
import com.winmax.transfer.constant.Constants
import org.json.JSONException
import org.json.JSONObject


class MyFirebaseMessagingService : FirebaseMessagingService() {

    private var notificationUtils: NotificationUtils? = null

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage.notification != null) {
            handleNotification(remoteMessage.notification !!.body)
        }
        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            try {
                val json = JSONObject(remoteMessage.data.toString())
                handleDataMessage(json)
            } catch (e: Exception) {
                Log.e("" , "Exception: " + e.message)
            }
        }
    }

    override fun onNewToken(refreshedToken: String) {
        super.onNewToken(refreshedToken)
        storeRegIdInPref(refreshedToken)

        val registrationComplete = Intent(Constants.REGISTRATION_COMPLETE)
        registrationComplete.putExtra("token" , refreshedToken)
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete)

        Log.e("onNewToken","onNewToken"+ refreshedToken)
    }

    private fun storeRegIdInPref(token: String?) {
        val pref = applicationContext.getSharedPreferences(Constants.SHARED_PREF_FIREBASE , 0)
        val editor = pref.edit()
        editor.putString("firebaseRegId" , token)
        editor.apply()
    }

    private fun handleNotification(message: String?) {
        if (! NotificationUtils.isAppIsInBackground(applicationContext)) {
            val pushNotification = Intent(Constants.PUSH_NOTIFICATION)
            pushNotification.putExtra("message" , message)
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)

            // play notification sound
            val notificationUtils = NotificationUtils(applicationContext)
            notificationUtils.playNotificationSound()
            /* Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            Intent resultIntent = new Intent(getApplicationContext(), NavigationActivity.class);
            resultIntent.putExtra("push_notifi_message", message);
            // check for image attachment
            showNotificationMessage(getApplicationContext(), "AleefCoin Notification", message, String.valueOf(timestamp), resultIntent);
    */
        }
    }

    private fun handleDataMessage(json: JSONObject) {
        try {
            val data = json.getJSONObject("data")
            val title = data.getString("title")
            val message = data.getString("message")
            val imageUrl = data.getString("image")
            val timestamp = data.getString("timestamp")

            if (! NotificationUtils.isAppIsInBackground(applicationContext)) {
                // app is in foreground, broadcast the push message
                val pushNotification = Intent(Constants.PUSH_NOTIFICATION)
                pushNotification.putExtra("message" , message)
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)

                // play notification sound
                val notificationUtils = NotificationUtils(applicationContext)
                notificationUtils.playNotificationSound()
            } else {
                // app is in background, show the notification in notification tray
                val resultIntent = Intent(applicationContext , TrackingActivity::class.java)
                resultIntent.putExtra("push_notifi_message" , message)

                // check for image attachment
                if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(applicationContext , title , message , timestamp , resultIntent)
                } else {
                    // image is present, show notificatiUser123
                    // on with image
                    showNotificationMessageWithBigImage(applicationContext , title , message , timestamp , resultIntent , imageUrl)
                }
            }
        } catch (e: JSONException) {
            Log.e("" , "Json Exception: " + e.message)
        } catch (e: Exception) {
            Log.e("" , "Exception: " + e.message)
        }
    }

    /**
     * Showing notification with text only
     */
    private fun showNotificationMessage(context: Context , title: String , message: String , timeStamp: String , intent: Intent) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils?.showNotificationMessage(title , message , timeStamp , intent)
    }

    /**
     * Showing notification with text and image
     */
    private fun showNotificationMessageWithBigImage(context: Context , title: String , message: String , timeStamp: String , intent: Intent , imageUrl: String) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils !!.showNotificationMessage(title , message , timeStamp , intent , imageUrl)
    }
}
