package com.winmax.transfer

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.model.ItemListModel
import kotlinx.android.synthetic.main.transfer_list_items.view.*


class ItemListAdapter(private var mContext: Context? , private var itemList: ArrayList<ItemListModel> , val listener: (ArrayList<ItemListModel> , Int , View) -> Unit) : RecyclerView.Adapter<ItemListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.transfer_list_items , parent , false)

        return ViewHolder(v , mContext)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        holder.bindItems(itemList , listener , position)
    }


    inner class ViewHolder(itemView: View , private var mContext: Context?) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(itemListData: ArrayList<ItemListModel> , listener: (ArrayList<ItemListModel> , Int , View) -> Unit , position: Int) {


            itemView.item_code.text = itemListData.get(position).itemCode
            itemView.item_product_name.text = itemListData.get(position).productName
            itemView.item_qty.setText(itemListData.get(position).quantity.toString())
            itemView.item_qty.setSelection(itemView.item_qty.text.toString().length)
            itemView.item_qty.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(charSequence: CharSequence , i: Int , i1: Int , i2: Int) {}
                override fun onTextChanged(charSequence: CharSequence , i: Int , i1: Int , i2: Int) {
                    //itemList.get(adapterPosition).setEditTextValue(editText.getText().toString())
                }

                override fun afterTextChanged(editable: Editable) {
                    if (editable.length > 0) {
                        Singleton.instance.articleListSingletonInventory[position].quantity = itemView.item_qty.text.toString().trim()
                    } else {
                        Toast.makeText(mContext , "Please enter valid quantity!" , Toast.LENGTH_LONG).show()
                        Singleton.instance.articleListSingletonInventory[position].quantity = itemListData.get(position).quantity.toString()
                    }
                }
            })

            itemView.itemContainer.setOnClickListener {
                listener(itemListData , position , it)

            }


        }
    }


}



