package com.winmax.transfer

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.winmax.transfer.model.getArticals.response.Article
import kotlinx.android.synthetic.main.detail_list_items.view.*
import org.json.JSONObject


class ItemDetailAdapter(private var mContext: Context?, private var itemList: ArrayList<Article>, val listener: (ArrayList<Article>, Int, View) -> Unit) : RecyclerView.Adapter<ItemDetailAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.detail_list_items, parent, false)

        return ViewHolder(v, mContext)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itemList, listener, position)
    }


    inner class ViewHolder(itemView: View, private var mContext: Context?) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(itemListData: ArrayList<Article>, listener: (ArrayList<Article>, Int, View) -> Unit, position: Int) {


            itemView.item_product_name.text = itemListData[position].prices?.price?.salesPrice1WithTaxesFees


            var listA = itemListData[position].prices?.price


            val gson = Gson()
            val json = gson.toJson(itemListData[position].prices?.price)
            val rootJsonObject = JSONObject("price")

            ///val map = posts.getMap() as Map<String, JSONObject>

            //val list = ArrayList(map.keys)
            //val developerArray = rootJsonObject.getString("developers")

            //val mJsonArray = JSONArray(developerArray)
        }
    }
}


