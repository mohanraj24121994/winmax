package com.winmax.transfer.locationupdate

import android.Manifest
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleService
import com.google.android.gms.location.*
import com.winmax.transfer.R
import com.winmax.transfer.SelectWareHouseScreen
import java.lang.reflect.InvocationTargetException


class BGLocationService : LifecycleService() {
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var client: FusedLocationProviderClient? = null

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) createNotificationChannel(
                "my_service" ,
                "Location Tracking"
        ) else
            startForeground(
                    1 ,
                    Notification()
            )
        requestLocationUpdates(true)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String , channelName: String) {
        val resultIntent = Intent(this , SelectWareHouseScreen::class.java)
        // Create the TaskStackBuilder and add the intent, which inflates the back stack
        val stackBuilder = TaskStackBuilder.create(this)
        stackBuilder.addNextIntentWithParentStack(resultIntent)
        val resultPendingIntent =
                stackBuilder.getPendingIntent(0 , PendingIntent.FLAG_UPDATE_CURRENT)
        val chan =
                NotificationChannel(channelId , channelName , NotificationManager.IMPORTANCE_DEFAULT)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val manager =
                (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
        manager.createNotificationChannel(chan)
        val notificationBuilder =
                NotificationCompat.Builder(this , channelId)
        val notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.app_icon_round)
                .setContentTitle("Location Tracking Started")
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentIntent(resultPendingIntent) //intent
                .build()
        val notificationManager = NotificationManagerCompat.from(this)
        notificationManager.notify(1 , notificationBuilder.build())
        startForeground(1 , notification)
    }

    override fun onStartCommand(intent: Intent? , flags: Int , startId: Int): Int {
        super.onStartCommand(intent , flags , startId)
        val action = intent?.action
        if (action != null) {
            if (action == "StopService") {
                stopFGService()
                requestLocationUpdates(false)
            }
        }
        return START_STICKY
    }

    private fun stopFGService() {
        stopForeground(true)
        stopSelf()
    }

    private fun requestLocationUpdates(enable: Boolean) {
        val request = LocationRequest()
        request.interval = 15000
        request.fastestInterval = 5000
        request.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        client = LocationServices.getFusedLocationProviderClient(this)

        val permission = ContextCompat.checkSelfPermission(this ,
                Manifest.permission.ACCESS_FINE_LOCATION)
        if (permission == PackageManager.PERMISSION_GRANTED) { // Request location updates and when an update is
            if (enable) {
                client?.requestLocationUpdates(request , mLocationCallback , null)
            } else {
                client?.removeLocationUpdates(mLocationCallback)
            }
        }
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val location: Location = locationResult.lastLocation
            if (location != null) {
                latitude = location.latitude
                longitude = location.longitude
                getLocation(latitude , longitude)
                Log.e("longitude" , "longitude==>" + longitude + latitude)
            }
        }
    }

    private fun getLocation(lat: Double , lng: Double) {
        val getViewModel = LocationUpdateViewModel(application)
        try {
            getViewModel.callLocationUpdateAPI(lat = lat.toString() , long = lng.toString())
        } catch (e: InvocationTargetException) {
            Log.e("" , "InvocationTargetException" + e.targetException)
        }
    }
}