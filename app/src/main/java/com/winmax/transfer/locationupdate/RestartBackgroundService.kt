package com.winmax.transfer.locationupdate

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build

class RestartBackgroundService : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context?.startForegroundService(Intent(context, BGLocationService::class.java))
        } else {
            context?.startService(Intent(context, BGLocationService::class.java))
        }
    }
}