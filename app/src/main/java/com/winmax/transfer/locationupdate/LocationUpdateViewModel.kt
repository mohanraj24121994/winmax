package com.winmax.transfer.locationupdate

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.webservices.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocationUpdateViewModel(application: Application) : AndroidViewModel(application) {

    /* init {
         if (PrefManager(getApplication()).getStringValue("designation") == "DRIVERS") {
             callLocationUpdateAPI()
         }
     }*/

    fun callLocationUpdateAPI(lat: String , long: String) {
        try {
            val request = LocationUpdateRequest()
            request.driverName = PrefManager(getApplication()).getStringValue("getDriverCode")
            request.resturantName = PrefManager(getApplication()).getStringValue(Constants.companyCodeConst)
            request.latitude = lat
            request.longitude = long

            val apiCall = ApiClient.getService().locationUpdate(request)
            apiCall.enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String> , response: Response<String>) {
                    Log.e("LocationUpdate" , "LocationUpdate" + "success")
                }

                override fun onFailure(call: Call<String> , t: Throwable) {
                    Log.e("LocationUpdate" , "LocationUpdate" + "failure")
                }
            })

        } catch (e: Exception) {
            Log.e("logTag" , e.toString())
        }
    }
}