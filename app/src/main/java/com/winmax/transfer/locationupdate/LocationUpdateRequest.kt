package com.winmax.transfer.locationupdate

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LocationUpdateRequest(
        @SerializedName("DriverName")
        @Expose
        var driverName: String? = null ,
        @SerializedName("ResturantName")
        @Expose
        var resturantName: String? = null ,
        @SerializedName("Latitude")
        @Expose
        var latitude: String? = null ,

        @SerializedName("Longitude")
        @Expose
        var longitude: String? = null
)