package com.winmax.transfer.search

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.zxing.integration.android.IntentIntegrator
import com.winmax.transfer.KeyBoardListener
import com.winmax.transfer.MainActivity
import com.winmax.transfer.R
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.databinding.ActivityItemSearchBinding
import com.winmax.transfer.model.getArticals.response.Articles
import com.winmax.transfer.search.itemdetail.ItemDetailActivity
import com.winmax.transfer.utils.AnyOrientationCaptureActivity
import com.winmax.transfer.utils.LoadingIndicatorListener
import com.winmax.transfer.utils.toast


class ItemSearchActivity : AppCompatActivity(), KeyBoardListener, LoadingIndicatorListener {

    private lateinit var viewModel: ItemSearchViewModel
    private lateinit var binding: ActivityItemSearchBinding
    private var logTag = "LOG_ItemSearchActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            binding = DataBindingUtil.setContentView(this, R.layout.activity_item_search)
            viewModel = ViewModelProviders.of(this).get(ItemSearchViewModel::class.java)
            binding.lifecycleOwner = this
            binding.viewModel = viewModel
            viewModel.loadingIndicatorListener = this
            initViewListeners()
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

    }

    //setup: view listeners
    private fun initViewListeners() {
        try {
            keepFocusToItemCode()
            //set listener: for focus change of itemCode
            /*  binding.viewEditTextItemCode.setOnFocusChangeListener { view, isFocused ->

                  if (isFocused) {
                      keepFocusToItemCode()
                  }
              }*/

            //set listener: for back action
            viewModel.actionPressedIconBack().observe(this, Observer { isBackButtonPressed ->

                if (isBackButtonPressed) {
                    finishActivity()
                }

            })

            //set listener: for logout action
            viewModel.actionPressedLogout().observe(this, Observer { isLogoutPressed ->

                if (isLogoutPressed) {
                    PrefManager(this).isFirstTimeLaunch = false
                    PrefManager(this).setBooleanValue("isLoggedIn", false)
                    startActivityForResult(Intent(this, MainActivity::class.java), Constants.REQUEST_CODE_EXIT);
                    overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right)
                    finish()
                }
            })

            //set listener: for button search
            viewModel.actionPressedSearch().observe(this, Observer { isSearchPressed ->
                if (isSearchPressed) {
                    if (null != viewModel.getDataArticles().value) {
                        //start ItemSearchDetailActivity
                        val intent = Intent(this, ItemDetailActivity::class.java)
                        val articles: Articles = viewModel.getDataArticles().value!!
                        intent.putExtra(Constants.EXTRA_KEY_DATA_ARTICLES, articles)
                        startActivityForResult(intent, Constants.REQUEST_CODE_EXIT)
                    } else {
                        toast("Something went wrong")
                    }
                }
            })

        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    private fun keepFocusToItemCode() {
        binding.viewEditTextItemCode.showSoftInputOnFocus = false; //no response on keyboard touch
        binding.viewEditTextItemCode.requestFocus()
        val inputConnection = binding.viewEditTextItemCode.onCreateInputConnection(EditorInfo())
        binding.viewKeyboardCustom.setInputConnection(inputConnection)
        hideKeyboard()
    }

    private fun hideKeyboard() {
        val imm: InputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = this.currentFocus
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    //KeyBoardListener: clickRemove
    override fun clickDelete() {
        try {
            val length: Int = binding.viewEditTextItemCode.text!!.toString().trim().length
            if (length > 0) {
                binding.viewEditTextItemCode.text!!.delete(length - 1, length)
                viewModel.clearDescription()
            }
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    //KeyBoardListener: clickScan
    override fun clickScan() {
        val integrator = IntentIntegrator(this)
        integrator.setRequestCode(Constants.REQUEST_CODE_SCANNER)
        integrator.setOrientationLocked(false)
        integrator.captureActivity = AnyOrientationCaptureActivity::class.java
        integrator.initiateScan()
    }

    override fun clickKeyDone() {
        try {
            viewModel.onClickButtonKeyboardDone()
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    //KeyBoardListener: clickDone
    override fun clickDone() {
        try {
            viewModel.onClickButtonKeyboardDone()
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    //API: started
    override fun onStarted() {
        try {
            binding.circleView.visibility = View.VISIBLE
            viewModel.isTouchEnabled.set(false)
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    //API: success
    override fun onSuccess() {
        try {
            binding.circleView.visibility = View.GONE
            viewModel.isTouchEnabled.set(true)
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    //API: failure
    override fun onFailure(message: String?) {
        try {
            if (!message.isNullOrEmpty()) {
                toast(message)
            }
            binding.circleView.visibility = View.GONE
            viewModel.isTouchEnabled.set(true)
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    //finishActivity
    private fun finishActivity() {
        finish()
        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right)
    }

    override fun onBackPressed() {
        finishActivity()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == Constants.REQUEST_CODE_EXIT) {
                onActivityResultFromItemDetailScreen(resultCode)
            } else if (requestCode == Constants.REQUEST_CODE_SCANNER) {
                onActivityResultFromScanner(resultCode, data)
            }
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    //result: from item detail screen
    private fun onActivityResultFromItemDetailScreen(resultCode: Int) {
        try {
            toast("Exit activity")
            if (resultCode == Constants.REQUEST_CODE_EXIT) {
                finishActivity()
            }
        } catch (e: java.lang.Exception) {
            Log.e(logTag, e.toString())
        }
    }

    //result: from scanner
    private fun onActivityResultFromScanner(resultCode: Int, data: Intent?) {
        try {
            val result = IntentIntegrator.parseActivityResult(resultCode, data)
            if (result != null) {
                if (result.contents == null) {
                    toast("Cancelled: Scanning")
                } else {

                    toast("Result : ${result.contents}")
                    //set result in edit text
                    binding.viewEditTextItemCode.text = Editable.Factory.getInstance().newEditable(result.contents)
                    binding.viewEditTextItemCode.setSelection(result.contents.length)
                    //clear description
                    viewModel.clearDescription()
                }
            }
        } catch (e: java.lang.Exception) {
            Log.e(logTag, e.toString())
        }
    }

}