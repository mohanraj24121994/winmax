package com.winmax.transfer.search

import android.app.Application
import android.util.Log
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.Constants.RESPONSE_CODE_0
import com.winmax.transfer.constant.Constants.RESPONSE_CODE_5
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.model.getArticals.response.ArticleResponse
import com.winmax.transfer.model.getArticals.response.Articles
import com.winmax.transfer.model.webserviceConnection.WebTestRequest
import com.winmax.transfer.webservices.ApiClient
import com.winmax.transfer.utils.LoadingIndicatorListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ItemSearchViewModel(application: Application) : AndroidViewModel(application) {

    private var logTag = "LOG_ItemSearchViewModel"
    var itemCode: String? = ""//9786185266103
    private val isBackPressed = MutableLiveData<Boolean>()
    private val isLogoutPressed = MutableLiveData<Boolean>()
    private val isSearchPressed = MutableLiveData<Boolean>()
    val isSearchVisible = ObservableBoolean(false)
    val isTouchEnabled = ObservableBoolean(true)
    val itemDescription = MutableLiveData<String>()
    var dataArticles = MutableLiveData<Articles?> ()

    var loadingIndicatorListener: LoadingIndicatorListener? = null


    fun clearDescription() {
        itemDescription.value = ""
        isSearchVisible.set(false)
    }

    fun getDataArticles(): LiveData<Articles?>{
        return dataArticles
    }

    //action on : pressed back
    fun actionPressedIconBack(): LiveData<Boolean> {
        return isBackPressed
    }

    //action on : pressed logout
    fun actionPressedLogout(): LiveData<Boolean> {
        return isLogoutPressed
    }

    //action on : pressed search
    fun actionPressedSearch(): LiveData<Boolean> {
        return isSearchPressed
    }

    //onclick : icon back
    fun onClickIconButtonBack(view: View) {
        isBackPressed.value = true
    }

    //onclick : icon logout
    fun onClickIconLogout(view: View){
        isLogoutPressed.value = true
    }

    //onclick: button done
    fun onClickButtonKeyboardDone() {
        if (validateItemCode()) {
            loadingIndicatorListener?.onStarted()//show
            callApiGetArticle()
        }
    }

    //onclick: button search
    fun onClickButtonSearch(view: View) {
        isSearchPressed.value = true
    }

    //helper: validate view item code
    private fun validateItemCode(): Boolean {
        if (itemCode.isNullOrEmpty()) {
            loadingIndicatorListener?.onFailure("Item code is empty, please enter.")
            isSearchVisible.set(false)
            return false
        } else {
            return true
        }
    }

    //call api: get article
    private fun callApiGetArticle() {
        try {
            val request = buildRequestForGetArticle()
            val apiCall = ApiClient.getService().getArticle(itemCode!!, request)
            apiCall.enqueue(object : Callback<ArticleResponse> {

                //api: success
                override fun onResponse(call: Call<ArticleResponse>, response: Response<ArticleResponse>) {
                    loadingIndicatorListener?.onSuccess()
                    onNewResponseGetArticle(response)
                }

                //api: failure
                override fun onFailure(call: Call<ArticleResponse>, t: Throwable) {
                    isSearchVisible.set(false)
                    loadingIndicatorListener?.onFailure("Something went wrong")
                }
            })
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    //api request: get article
    private fun buildRequestForGetArticle(): WebTestRequest { //9786185266103
        val request = WebTestRequest()
        try {
            request.companyCode = PrefManager(getApplication()).getStringValue(Constants.companyCodeConst)
            request.userLogin = PrefManager(getApplication()).getStringValue(Constants.userNameConst)
            request.userPassword = PrefManager(getApplication()).getStringValue(Constants.passwordConst)
            request.webServiceURL = PrefManager(getApplication()).getStringValue(Constants.webUrlConst)
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
        return request
    }

    //api response: get article
    private fun onNewResponseGetArticle(response: Response<ArticleResponse>) {

        try {
            if (response.isSuccessful) {
                if (response.body()?.getArticlesResult?.code.equals(RESPONSE_CODE_0)) {
                    itemDescription.value = response.body()?.getArticlesResult?.articles?.article?.designation.toString()
                    dataArticles.value = response.body()?.getArticlesResult?.articles
                    isSearchVisible.set(true)
                } else if (response.body()?.getArticlesResult?.code.equals(RESPONSE_CODE_5)) {
                    loadingIndicatorListener?.onFailure("Invalid Item Code!!")
                    dataArticles.value = null
                    isSearchVisible.set(false)
                }
            } else {
                isSearchVisible.set(false)
                dataArticles.value = null
                loadingIndicatorListener?.onFailure("Something went wrong")
            }
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }


}