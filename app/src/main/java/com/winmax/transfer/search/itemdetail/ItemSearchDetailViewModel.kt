package com.winmax.transfer.search.itemdetail

import android.app.Application
import android.util.Log
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.model.family.FamilyResponse
import com.winmax.transfer.model.getArticals.response.Articles
import com.winmax.transfer.model.getArticals.response.Price
import com.winmax.transfer.model.getArticals.response.Prices
import com.winmax.transfer.model.getArticals.response.Stock
import com.winmax.transfer.model.webserviceConnection.WebTestRequest
import com.winmax.transfer.utils.LoadingIndicatorListener
import com.winmax.transfer.webservices.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ItemSearchDetailViewModel(application: Application , private val articles: Articles) : AndroidViewModel(application) {

    private var logTag = "LOG_ItemSearchDetailViewModel"

    private val isBackPressed = MutableLiveData<Boolean>()
    private val isLogoutPressed = MutableLiveData<Boolean>()
    private val isButtonNewSearchPressed = MutableLiveData<Boolean>()
    private val isButtonExitPressed = MutableLiveData<Boolean>()

    //viewVariables
    val itemDescription = MutableLiveData<String>()
    var itemCode = MutableLiveData<String>()
    val viewTextFamilyDesignation = MutableLiveData<String>()
    val viewTextPrice1WithOutTax = MutableLiveData<String>()
    val viewTextPrice2WithOutTax = MutableLiveData<String>()
    val viewTextPrice3WithOutTax = MutableLiveData<String>()
    val viewTextPrice4WithOutTax = MutableLiveData<String>()
    val viewTextPrice5WithOutTax = MutableLiveData<String>()
    val viewTextPrice6WithOutTax = MutableLiveData<String>()
    val viewTextPrice7WithOutTax = MutableLiveData<String>()
    val viewTextPrice8WithOutTax = MutableLiveData<String>()
    val viewTextPrice9WithOutTax = MutableLiveData<String>()
    val viewTextPrice1WithTax = MutableLiveData<String>()
    val viewTextPrice2WithTax = MutableLiveData<String>()
    val viewTextPrice3WithTax = MutableLiveData<String>()
    val viewTextPrice4WithTax = MutableLiveData<String>()
    val viewTextPrice5WithTax = MutableLiveData<String>()
    val viewTextPrice6WithTax = MutableLiveData<String>()
    val viewTextPrice7WithTax = MutableLiveData<String>()
    val viewTextPrice8WithTax = MutableLiveData<String>()
    val viewTextPrice9WithTax = MutableLiveData<String>()
    val viewTextStock1 = MutableLiveData<String>()
    val viewTextStock2 = MutableLiveData<String>()
    val viewTextStock3 = MutableLiveData<String>()
    val viewTextStock4 = MutableLiveData<String>()
    val viewTextStock5 = MutableLiveData<String>()
    val viewTextStock6 = MutableLiveData<String>()
    val viewTextStock7 = MutableLiveData<String>()
    val viewTextStock8 = MutableLiveData<String>()
    val viewTextStock9 = MutableLiveData<String>()
    val viewTextStockTotal = MutableLiveData<String>()
    val viewTextNetCostPrice = MutableLiveData<String>()
    val viewTextGrossCostPrice = MutableLiveData<String>()
    val viewTextSubFamilyCode = MutableLiveData<String>()

    var loadingIndicatorListener: LoadingIndicatorListener? = null

    init {
        Log.e(logTag , articles.toString())
        try {
            callApiGetFamilyDetails(articles.article?.familyCode !!)
            fillViewValues()
        } catch (e: java.lang.Exception) {

        }
    }

    //call api: get article
    private fun callApiGetFamilyDetails(familyCode: String) {
        try {
            val request = buildRequestForGetFamilyDetail()
            val apiCall = ApiClient.getService().GetFamilyDesignation(familyCode , request)
            apiCall.enqueue(object : Callback<FamilyResponse> {

                //api: success
                override fun onResponse(call: Call<FamilyResponse> , response: Response<FamilyResponse>) {
                    loadingIndicatorListener?.onSuccess()
                    onNewResponseGetFamily(response)
                }

                //api: failure
                override fun onFailure(call: Call<FamilyResponse> , t: Throwable) {
                    loadingIndicatorListener?.onFailure("Something went wrong")
                }
            })
        } catch (e: Exception) {
            Log.e(logTag , e.toString())
        }
    }

    //api request: get article
    private fun buildRequestForGetFamilyDetail(): WebTestRequest { //9786185266103
        val request = WebTestRequest()
        try {
            request.companyCode = PrefManager(getApplication()).getStringValue(Constants.companyCodeConst)
            request.userLogin = PrefManager(getApplication()).getStringValue(Constants.userNameConst)
            request.userPassword = PrefManager(getApplication()).getStringValue(Constants.passwordConst)
            request.webServiceURL = PrefManager(getApplication()).getStringValue(Constants.webUrlConst)
        } catch (e: Exception) {
            Log.e(logTag , e.toString())
        }
        return request
    }

    //api response: get family detail
    private fun onNewResponseGetFamily(response: Response<FamilyResponse>) {

        try {
            if (response.isSuccessful) {
                if (response.body()?.getFamiliesResult?.code.equals(Constants.RESPONSE_CODE_0)) {
                    viewTextFamilyDesignation.value = response.body()?.getFamiliesResult?.families?.family?.designation
                } else {
                    loadingIndicatorListener?.onFailure("Invalid family Code!!")
                    viewTextFamilyDesignation.value = null
                }
            } else {
                viewTextFamilyDesignation.value = null
                loadingIndicatorListener?.onFailure("Something went wrong")
            }
        } catch (e: Exception) {
            Log.e(logTag , e.toString())
        }
    }

    private fun fillViewValues() {
        itemCode.value = articles.article?.articleCode
        itemDescription.value = articles.article?.designation
        //viewTextSubFamilyCode.value = articles.article?.SubFamilyCode
        viewTextSubFamilyCode.value = "ANTIVIRUS"


        //fill stock value
        articles.article?.stocks?.stock.let {
            it?.forEach {
                fillStock(it)
            }
        }

        articles.article?.prices?.price.let {
            //with out tax price values
            viewTextPrice1WithOutTax.value = it?.salesPrice1WithoutTaxesFees
            viewTextPrice2WithOutTax.value = it?.salesPrice2WithoutTaxesFees
            viewTextPrice3WithOutTax.value = it?.salesPrice3WithoutTaxesFees
            viewTextPrice4WithOutTax.value = it?.salesPrice4WithoutTaxesFees
            viewTextPrice5WithOutTax.value = it?.salesPrice5WithoutTaxesFees
            viewTextPrice6WithOutTax.value = it?.salesPrice6WithoutTaxesFees
            viewTextPrice7WithOutTax.value = it?.salesPrice7WithoutTaxesFees
            viewTextPrice8WithOutTax.value = it?.salesPrice8WithoutTaxesFees
            viewTextPrice9WithOutTax.value = it?.salesPrice9WithoutTaxesFees
            //with tax price values
            viewTextPrice1WithTax.value = it?.salesPrice1WithTaxesFees
            viewTextPrice2WithTax.value = it?.salesPrice2WithTaxesFees
            viewTextPrice3WithTax.value = it?.salesPrice3WithTaxesFees
            viewTextPrice4WithTax.value = it?.salesPrice4WithTaxesFees
            viewTextPrice5WithTax.value = it?.salesPrice5WithTaxesFees
            viewTextPrice6WithTax.value = it?.salesPrice6WithTaxesFees
            viewTextPrice7WithTax.value = it?.salesPrice7WithTaxesFees
            viewTextPrice8WithTax.value = it?.salesPrice8WithTaxesFees
            viewTextPrice9WithTax.value = it?.salesPrice9WithTaxesFees
            viewTextNetCostPrice.value = it?.NetCostPrice
            viewTextGrossCostPrice.value = it?.GrossCostPrice

            viewTextStock1.value = (it?.salesPrice1WithoutTaxesFees !!.toFloat() / it.NetCostPrice !!.toFloat() * 100).toString() + "%"
            viewTextStock2.value = (it.salesPrice2WithoutTaxesFees !!.toFloat() / it.NetCostPrice.toFloat() * 100).toString() + "%"
            viewTextStock3.value = (it.salesPrice3WithoutTaxesFees !!.toFloat() / it.NetCostPrice.toFloat() * 100).toString() + "%"
            viewTextStock4.value = (it.salesPrice4WithoutTaxesFees !!.toFloat() / it.NetCostPrice.toFloat() * 100).toString() + "%"
            viewTextStock5.value = (it.salesPrice5WithoutTaxesFees !!.toFloat() / it.NetCostPrice.toFloat() * 100).toString() + "%"
            viewTextStock6.value = (it.salesPrice6WithoutTaxesFees !!.toFloat() / it.NetCostPrice.toFloat() * 100).toString() + "%"
            viewTextStock7.value = (it.salesPrice7WithoutTaxesFees !!.toFloat() / it.NetCostPrice.toFloat() * 100).toString() + "%"
            viewTextStock8.value = (it.salesPrice8WithoutTaxesFees !!.toFloat() / it.NetCostPrice.toFloat() * 100).toString() + "%"
            viewTextStock9.value = (it.salesPrice9WithoutTaxesFees !!.toFloat() / it.NetCostPrice.toFloat() * 100).toString() + "%"
        }

    }

    private fun fillStock(stock: Stock) {

        when (stock.warehouseCode) {
            "0" -> viewTextStockTotal.value = stock.currentStock
            /*"1" -> viewTextStock1.value = stock.currentStock
            "2" -> viewTextStock2.value = stock.currentStock
            "3" -> viewTextStock3.value = stock.currentStock
            "4" -> viewTextStock4.value = stock.currentStock
            "5" -> viewTextStock5.value = stock.currentStock
            "6" -> viewTextStock6.value = stock.currentStock
            "7" -> viewTextStock7.value = stock.currentStock
            "8" -> viewTextStock8.value = stock.currentStock
            "10" -> viewTextStock9.value = stock.currentStock*/
        }

    }

    //action on : pressed back
    fun actionPressedIconBack(): LiveData<Boolean> {
        return isBackPressed
    }

    //action on : pressed logout
    fun actionPressedLogout(): LiveData<Boolean> {
        return isLogoutPressed
    }

    //action on : pressed newSearch
    fun actionPressedButtonNewSearch(): LiveData<Boolean> {
        return isButtonNewSearchPressed
    }

    //action on : pressed exit
    fun actionPressedButtonExit(): LiveData<Boolean> {
        return isButtonExitPressed
    }

    //onclick : icon back
    fun onClickIconButtonBack(view: View) {
        isBackPressed.value = true
    }

    //onclick : icon logout
    fun onClickIconLogout(view: View) {
        isLogoutPressed.value = true
    }

    //onclick : button newSearch
    fun onClickButtonNewSearch(view: View) {
        isButtonNewSearchPressed.value = true
    }

    //onclick : button exit
    fun onClickButtonExit(view: View) {
        isButtonExitPressed.value = true
    }


}