package com.winmax.transfer.search.itemdetail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.winmax.transfer.MainActivity
import com.winmax.transfer.R
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.databinding.ActivityItemSearchDetailBinding
import com.winmax.transfer.model.getArticals.response.Articles
import com.winmax.transfer.utils.LoadingIndicatorListener
import com.winmax.transfer.utils.modelprovider.ItemSearchDetailViewModelFactory
import com.winmax.transfer.utils.toast
import java.lang.Exception
import java.net.ProtocolFamily

class ItemDetailActivity : AppCompatActivity(), LoadingIndicatorListener {

    private lateinit var viewModel: ItemSearchDetailViewModel
    private lateinit var binding: ActivityItemSearchDetailBinding
    private var logTag = "LOG_ItemSearchDetailActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_item_search_detail)
        val articles: Articles = intent.getParcelableExtra(Constants.EXTRA_KEY_DATA_ARTICLES) as Articles
        val viewModelFactory = ItemSearchDetailViewModelFactory(application, articles)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ItemSearchDetailViewModel::class.java)
        viewModel.loadingIndicatorListener = this
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        initViewListeners()
    }

    private fun initViewListeners() {

        try {
            //set listener: for back action
            viewModel.actionPressedIconBack().observe(this, Observer { isBackButtonPressed ->

                if (isBackButtonPressed) {
                    finishActivity()
                }

            })

            //set listener: for logout action
            viewModel.actionPressedLogout().observe(this, Observer { it ->
                if (it) {
                    PrefManager(this).isFirstTimeLaunch = false
                    PrefManager(this).setBooleanValue("isLoggedIn", false)
                    startActivity(Intent(this, MainActivity::class.java));
                    overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right)
                    finish()
                }
            })

            //set listener: for newSearch
            viewModel.actionPressedButtonNewSearch().observe(this, Observer { it ->
                if (it) {
                    finishActivity()
                }

            })

            //set listener: for exit
            viewModel.actionPressedButtonExit().observe(this, Observer { it ->
                if (it) {
                    setResult(Constants.REQUEST_CODE_EXIT)
                    finish()
                }

            })
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    //API: started
    override fun onStarted() {
        try {
            binding.circleView.visibility = View.VISIBLE
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    //API: success
    override fun onSuccess() {
        try {
            binding.circleView.visibility = View.GONE
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    //API: failure
    override fun onFailure(message: String?) {
        try {
            if (!message.isNullOrEmpty()) {
                toast(message)
            }
            binding.circleView.visibility = View.GONE
        } catch (e: Exception) {
            Log.e(logTag, e.toString())
        }
    }

    override fun onBackPressed() {
        finishActivity()
    }

    //finishActivity
    private fun finishActivity() {
        finish()
        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right)
    }
}