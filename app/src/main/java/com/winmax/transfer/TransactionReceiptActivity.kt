package com.winmax.transfer

import android.content.Context
import android.content.Intent
import android.graphics.*
import android.net.Uri
import android.os.*
import android.os.StrictMode.VmPolicy
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.utils.PDFTools
import com.winmax.transfer.utils.imageShare.ConvertBitmapToFile
import com.winmax.transfer.utils.imageShare.IConvertBitmapToFile
import kotlinx.android.synthetic.main.activity_transaction_receipt.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream


class TransactionReceiptActivity : AppCompatActivity() , View.OnClickListener , OnPageErrorListener {

    var mContext: Context? = null
    private var uri: Uri? = null
    private var uri1: Uri? = null
    private var uri2: Uri? = null
    private var uri3: Uri? = null
    private var uri4: Uri? = null
    private var pdfuri: Uri? = null

    private var filename: String? = null
    private var isTracking = false
    private var isInvoice = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_receipt)

        mContext = this

        onClickListener()

        setVersion()

        getImagePdf()

        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
    }

    private fun getImagePdf() {
        val extras = intent.extras

        filename = extras?.getString("filename")

        isTracking = extras?.getBoolean("isTracking") ?: false
        isInvoice = extras?.getBoolean("isInvoice") ?: false


/*
        val documents = intent.getSerializableExtra("cheque") as ArrayList<File>

        if (documents.size > 0) {
            documents.forEach {
               uri =  Uri.parse(it.toString())
            }
        }
*/

        if (extras != null && extras.containsKey("KEY1")) {
            uri1 = Uri.parse(extras.getString("KEY1"))
        }

        if (extras != null && extras.containsKey("KEY2")) {
            uri2 = Uri.parse(extras.getString("KEY2"))
        }

        if (extras != null && extras.containsKey("KEY3")) {
            uri3 = Uri.parse(extras.getString("KEY3"))
        }

        if (extras != null && extras.containsKey("KEY4")) {
            uri4 = Uri.parse(extras.getString("KEY4"))
        }

        if (extras != null && extras.containsKey("KEY")) {
            uri = Uri.parse(extras.getString("KEY"))
        }
        loadPDFView()

        if (isTracking) {
            signaturePad.visibility = View.VISIBLE
            tvSign.visibility = View.VISIBLE
        }
    }

    private fun startActivityPayment() {
        startActivity(Intent(this , PaymentActivity::class.java))
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isTracking || isInvoice) {
            val to = Intent(this , SelectWareHouseScreen::class.java)
            to.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(to)
        } else {
            startActivityPayment()
        }
    }

    private fun loadPDFView() {
        // val extStorageDirectory = Environment.getExternalStorageDirectory().toString()
        //val folder = File(extStorageDirectory , "Winmax")
        val folder = File(this.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) , "Winmax")
        folder.mkdir()

        val file = File(folder , filename ?: "")
        pdfuri = Uri.fromFile(file)
        val pdfView = findViewById<PDFView>(R.id.pdfView)
        pdfView.fromFile(file)
                .password(null)
                .defaultPage(0)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .onPageError { page , _ ->
                    Toast.makeText(this , "Error at page: $page" , Toast.LENGTH_LONG
                    ).show()
                }
                .load()

        /* val web = findViewById<WebView>(R.id.webview)
         web.webViewClient = WebViewClient()
         web.settings.setSupportZoom(true)
         web.settings.javaScriptEnabled = true

         val url = "https://docs.google.com/gview?embedded=true&url=" + URLEncoder.encode(pdfuri!!.path , "ISO-8859-1")
         web.loadUrl(url)*/

    }

    private fun setVersion() {
        tvVersion.text = PrefManager(applicationContext).getStringValue(Constants.versionConst)
    }

    private fun onClickListener() {
        ivBack.setOnClickListener(this)
        ivLogout.setOnClickListener(this)
        btnShare.setOnClickListener(this)
        ivClose.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivBack -> {
                if (isTracking || isInvoice) {
                    val to = Intent(this , SelectWareHouseScreen::class.java)
                    to.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(to)
                } else {
                    startActivityPayment()
                }
            }
            R.id.ivLogout -> {
                PrefManager(mContext).setBooleanValue("isLoggedIn" , false)
                PrefManager(mContext).isFirstTimeLaunch = false
                val to = Intent(this , MainActivity::class.java)
                to.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(to)
                overridePendingTransition(R.anim.enter , R.anim.exit)
                finish()
            }
            R.id.btnShare -> {
                if (! isTracking) {
                    shareMail()
                } else {
                    if (signaturePad.isEmpty) {
                        Toast.makeText(this , "Please enter signature!" , Toast.LENGTH_SHORT).show()
                    } else {
                        signatureAttach()
                    }
                }
            }
            R.id.ivClose -> {
                signaturePad.clear()
            }
        }
    }

    private fun signatureAttach() {
        val pdf = PDFTools(applicationContext)
        val signatureBitmap = signaturePad.transparentSignatureBitmap
        val outputSignature = saveSignature(signatureBitmap)

        val folder = File(this.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) , "Winmax")
        folder.mkdir()

        val file = File(folder , filename ?: "")

        val tmpLocalFile = File(file.toString())
        pdf.open(tmpLocalFile)
        pdf.insertImage(outputSignature?.path , 350 , 50 , 200 , 50 , 1)

        val files = ArrayList<Uri>()
        val intent = Intent(Intent.ACTION_SEND_MULTIPLE)
        intent.type = "message/rfc822"
        files.add(Uri.fromFile(tmpLocalFile))
        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM , files)
        startActivity(Intent.createChooser(intent , "Share Deal"))
    }

    private fun saveSignature(signature: Bitmap): File? {
        try {
            val output = File.createTempFile("signer" , ".png" , applicationContext.cacheDir)
            saveBitmapToPNG(signature , output)
            return output
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    @Throws(IOException::class)
    private fun saveBitmapToPNG(bitmap: Bitmap , photo: File) {
        val newBitmap = Bitmap.createBitmap(bitmap.width , bitmap.height , Bitmap.Config.ARGB_8888)
        val canvas = Canvas(newBitmap)
        canvas.drawColor(Color.TRANSPARENT)
        canvas.drawBitmap(bitmap , 0f , 0f , null)
        val stream: OutputStream = FileOutputStream(photo)
        newBitmap.compress(Bitmap.CompressFormat.PNG , 100 , stream)
        stream.close()
    }

    override fun onPageError(page: Int , t: Throwable?) {
        Log.e("TAG" , "Cannot load page $page")
    }

    private fun shareMail() {
        val intent = Intent(Intent.ACTION_SEND_MULTIPLE)
        // intent.type = "*/*"
        intent.type = "message/rfc822"
        val files = ArrayList<Uri>()

        if (uri1 != null) {
            files.add(uri1 !!)
        }
        if (uri2 != null) {
            files.add(uri2 !!)
        }
        if (uri3 != null) {
            files.add(uri3 !!)
        }
        if (uri4 != null) {
            files.add(uri4 !!)
        }
        if (uri != null) {
            files.add(uri !!)
        }

        files.add(pdfuri !!)

        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM , files)
        startActivity(Intent.createChooser(intent , "Share Deal"))
    }

/*    private fun saveSignature() {
        signaturePad.isDrawingCacheEnabled = true
        val bm1 = Bitmap.createBitmap(signaturePad.drawingCache)

        val bitmap = Bitmap.createBitmap(pdfView.width , pdfView.height , Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        pdfView.draw(canvas)

        try {
            val result = mergeBitmap(bitmap , bm1)
            result?.let { bitmapToFile(it) }
        } catch (e: Exception) {
            Log.e("Exception" , "Exception==>" + e.message)
        }

        bitmap.recycle()
        bm1.recycle()
    }

    private fun bitmapToFile(bitmap: Bitmap) {
        //create a file to write bitmap data
        val convert = ConvertBitmapToFile()
        convert.setConvertBitmapToFileListener(object : IConvertBitmapToFile {
            override fun onSuccess(aFilePath: File) {
                Log.e("aFilePath" , "aFilePath" + aFilePath)
                pdfuri = Uri.fromFile(aFilePath)
                shareMail()
            }

            override fun onFailure() {
                Toast.makeText(this@TransactionReceiptActivity , "failed" , Toast.LENGTH_SHORT).show()
            }
        } , bitmap , this , Constants.signature)
    }

    private fun createSingleImageFromMultipleImages(firstImage: Bitmap , secondImage: Bitmap): Bitmap? {
        val result = Bitmap.createBitmap(firstImage.width + secondImage.width , firstImage.height , firstImage.config)
        val canvas = Canvas(result)
        canvas.drawBitmap(firstImage , 0f , 0f , null)
        canvas.drawBitmap(secondImage , firstImage.width.toFloat() , 0f , null)
        return result
    }*/

    private fun mergeBitmap(bitmap1: Bitmap , bitmap2: Bitmap): Bitmap? {
        val mergedBitmap: Bitmap?
        val h: Int = bitmap1.height + bitmap2.height
        val w = if (bitmap1.width > bitmap2.width) {
            bitmap1.width
        } else {
            bitmap2.width
        }
        mergedBitmap = Bitmap.createBitmap(w , h , Bitmap.Config.ARGB_8888)
        val canvas = Canvas(mergedBitmap)
        canvas.drawBitmap(bitmap1 , 0f , 0f , null)
        canvas.drawBitmap(bitmap2 , 0f , bitmap1.height.toFloat() , null)
        return mergedBitmap
    }
}
