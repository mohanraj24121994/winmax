package com.winmax.transfer

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.*
import com.winmax.transfer.adapter.DriverListAdapter
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.downloader.FileDownloader
import com.winmax.transfer.model.MapModel
import com.winmax.transfer.model.getShippingTypesResult.GetDriverList
import com.winmax.transfer.model.getShippingTypesResult.ShippingType
import com.winmax.transfer.model.tracking.*
import com.winmax.transfer.model.webserviceConnection.WebTestRequest
import com.winmax.transfer.utils.CirclesLoadingView
import com.winmax.transfer.utils.IsDriverChecked
import com.winmax.transfer.utils.imageShare.CommonUtils
import com.winmax.transfer.webservices.ApiClient
import kotlinx.android.synthetic.main.activity_tracking.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.lang.reflect.InvocationTargetException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class TrackingActivity : AppCompatActivity() , View.OnClickListener {

    private var mContext: Context? = null
    private var listAdapter: TrackingListAdapter? = null
    private var entityCode = ""
    private var documentNumber = ""
    private var shippingTypeCode = ""
    private var stateOforder = ""
    private var latitude = ""
    private var longitude = ""
    private var addressToApi = ""

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val MY_PERMISSIONS_REQUEST_READ_FINE_LOCATION = 100

    var driverListResponse = ArrayList<ShippingType>()
    var driverList: ArrayList<String> = ArrayList()

    private var ascending = false
    var offer = MapModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tracking)

        setVersion()

        mContext = this

        requestPermission()

        checkLocation()

        callApi()

        setOnClickListener()

        setCurrentDate()
    }

    private fun setCurrentDate() {
        val sdf = SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
        val currentDate = sdf.format(Date())
        tvDate.text = currentDate
        tvChooseDriver.setOnClickListener {
            spDriverName.performClick()
        }
    }

    override fun onClick(v: View?) {
        when (v !!.id) {
            R.id.ivBack -> {
                finish()
            }
        }
    }

    private fun setOnClickListener() {
        ivBack.setOnClickListener(this)
    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) ActivityCompat.requestPermissions(this@TrackingActivity ,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION ,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) , MY_PERMISSIONS_REQUEST_READ_FINE_LOCATION
        ) else ActivityCompat.requestPermissions(this@TrackingActivity , arrayOf(Manifest.permission.ACCESS_FINE_LOCATION , Manifest.permission.WRITE_EXTERNAL_STORAGE) ,
                MY_PERMISSIONS_REQUEST_READ_FINE_LOCATION
        )
    }

    private fun setVersion() {
        tvVersion.text = PrefManager(applicationContext).getStringValue(Constants.versionConst)
    }

    private fun callApi() {
        circle_load.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = TrackingListRequest()
        request.shippingTypeCode = "0"

        val createEntityCredential = com.winmax.transfer.model.createentity.Credential()
        createEntityCredential.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        createEntityCredential.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        createEntityCredential.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        createEntityCredential.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)

        request.credential = createEntityCredential
        Singleton.instance.trackingListSingleton.clear()
        val call = ApiClient.getService().getTrackingOrderList(request)
        call.enqueue(object : Callback<ArrayList<TrackingListResponse>> {

            override fun onResponse(call: Call<ArrayList<TrackingListResponse>> , response: Response<ArrayList<TrackingListResponse>>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body() != null) {

                        if (PrefManager(mContext).getStringValue("designation") == "DRIVERS") {
                            val driverName = PrefManager(mContext).getStringValue("getDriverCode")
                            val trackingList = response.body()?.filter { it.shippingTypeCode?.contains(driverName , true) !! }
                            Singleton.instance.trackingListSingleton.addAll(trackingList !!)
                        } else {
                            Singleton.instance.trackingListSingleton.addAll(response.body() !!)
                        }

                        Singleton.instance.trackingListSingleton.sortWith(Comparator { p0 , p1 ->
                            return@Comparator p0.docDateTime?.compareTo(p1.docDateTime ?: "") ?: 0
                        })

                        if (Singleton.instance.trackingListSingleton.size > 0) {
                            rv_tracking_list.visibility = View.VISIBLE
                            tvNoItems.visibility = View.INVISIBLE

                            ivFilter.setOnClickListener {
                                ascending = if (ascending) {
                                    Singleton.instance.trackingListSingleton.sortWith(Comparator { p0 , p1 ->
                                        return@Comparator p0.docDateTime?.compareTo(p1.docDateTime
                                                ?: "") ?: 0
                                    })
                                    recyclerListItems(Singleton.instance.trackingListSingleton)
                                    false
                                } else {
                                    Singleton.instance.trackingListSingleton.reverse()
                                    recyclerListItems(Singleton.instance.trackingListSingleton)
                                    true
                                }
                            }

                            recyclerListItems(Singleton.instance.trackingListSingleton)
                        } else {
                            rv_tracking_list.visibility = View.INVISIBLE
                            tvNoItems.visibility = View.VISIBLE
                            ivFilter.visibility = View.GONE
                        }

                        if (PrefManager(mContext).getStringValue("designation") != "DRIVERS") {
                            callDriverListAPI()
                        } else {
                            ivFilter.visibility = View.GONE
                            spDriverName.visibility = View.GONE
                            //tvChooseDriver.visibility = View.GONE
                            //tvDriverName.visibility = View.VISIBLE
                            val driverName = PrefManager(mContext).getStringValue("username")
                            tvChooseDriver.text = driverName
                        }

                    } else {
                        Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                    }
                }
            }

            override fun onFailure(call: Call<ArrayList<TrackingListResponse>> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , "Failure")
            }
        })
    }

    fun recyclerListItems(trackingListSingleton: ArrayList<TrackingListResponse>) {

        val orderList = trackingListSingleton

        rv_tracking_list?.layoutManager = LinearLayoutManager(mContext)

        listAdapter = TrackingListAdapter(mContext , orderList) { itemList , i , view ->
            when (view.id) {
                R.id.cvTracking -> {
                    entityCode = itemList[i].code.toString()
                    documentNumber = itemList[i].documentNumber.toString()
                    shippingTypeCode = itemList[i].shippingTypeCode.toString()
                    stateOforder = itemList[i].stateOforder.toString()
                    val dialogClickListener = DialogInterface.OnClickListener { dialog , which ->
                        when (which) {
                            DialogInterface.BUTTON_POSITIVE -> {
                                if (checkPermission()) {
                                   callUpdateApi(latitude , longitude , addressToApi)
                                   /* Singleton.instance.invoiceListSingleton.clear()
                                    val intent = Intent(this , TransactionReceiptActivity::class.java)
                                    intent.putExtra("filename" , "W4 Update 76:25:05:2021:10:24:39.pdf")
                                    intent.putExtra("isInvoice" , true)
                                    startActivity(intent)
                                    overridePendingTransition(R.anim.enter , R.anim.exit)
                                    finish()*/
                                } else {
                                    requestPermission()
                                }
                            }
                            DialogInterface.BUTTON_NEGATIVE -> {
                                offer.address = itemList[i].entityAddress.plus(",")
                                        .plus(itemList[i].entityLocation).plus(",")
                                        .plus(itemList[i].entityZipCode)

                                if (Singleton.instance.addressvalue.size == 0) {
                                    Singleton.instance.addressvalue.add(offer)
                                } else {
                                    Singleton.instance.addressvalue.clear()
                                }
                                Singleton.instance.addressvalue.add(offer)
                                val intent = Intent(this@TrackingActivity , TrackinMapsActivity::class.java)
                                startActivity(intent)
                            }
                        }
                    }
                    val builder = AlertDialog.Builder(this@TrackingActivity)
                    builder.setMessage("Are you sure to update?").setPositiveButton(itemList[i].stateOforder.toString() , dialogClickListener)
                            .setNegativeButton("View Map" , dialogClickListener).show()
                }
            }
        }
        rv_tracking_list?.adapter = listAdapter
        listAdapter?.notifyDataSetChanged()
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this , Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun addAllProducts(): ArrayList<TrackingListResponse> {
        return Singleton.instance.trackingListSingleton
    }

    private fun callUpdateApi(lat: String , long: String , address: String) {
        circle_load.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = TrackingUpdateRequest()
        val addressRequest = TrackingOrderDetails()

        request.shippingTypeCode = shippingTypeCode
        request.entityCode = entityCode
        request.documentNumber = documentNumber

        val createEntityCredential = com.winmax.transfer.model.createentity.Credential()
        createEntityCredential.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        createEntityCredential.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        createEntityCredential.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        createEntityCredential.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)
        request.credential = createEntityCredential

        addressRequest.address = address
        addressRequest.latitude = lat
        addressRequest.longtitude = long
        addressRequest.orderStage = stateOforder
        request.orderDetails = addressRequest
        request.dropDate = CommonUtils.getDateTime()

        val call = ApiClient.getService().updateOrderState(request)
        call.enqueue(object : Callback<TrackingUpdateResponse> {

            override fun onResponse(call: Call<TrackingUpdateResponse> , response: Response<TrackingUpdateResponse>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        //callApi()
                        val c = Calendar.getInstance()
                        val dateformat = SimpleDateFormat(":dd:MM:yyyy:hh:mm:ss")
                        val datetime = dateformat.format(c.time)

                        val filename = "W4 Update " + response.body()?.entityCode + datetime

                        DownloadFile(this@TrackingActivity , circle_load).execute(response.body()?.pdf , "$filename.pdf")
                    } else {
                        Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                    }
                }
            }

            override fun onFailure(call: Call<TrackingUpdateResponse> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , "Failure")
            }
        })
    }

    private fun checkLocation() {
        val manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (! manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showAlertLocation()
        } else {
            requestLocationUpdates(true)
        }
    }

    private fun showAlertLocation() {
        val dialog = AlertDialog.Builder(this)
        dialog.setMessage("Your location settings is set to Off, Please enable location to use this application")
        dialog.setPositiveButton("Settings") { _ , _ ->
            val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(myIntent)
        }
        dialog.setNegativeButton("Cancel") { _ , _ ->

        }
        dialog.setCancelable(false)
        dialog.show()
    }

    private fun requestLocationUpdates(enable: Boolean) {
        val request = LocationRequest()
        request.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        val permission = ContextCompat.checkSelfPermission(this , Manifest.permission.ACCESS_FINE_LOCATION)
        if (permission == PackageManager.PERMISSION_GRANTED) {
            if (enable) {
                fusedLocationClient.requestLocationUpdates(request , mLocationCallback , null)
            } else {
                fusedLocationClient.removeLocationUpdates(mLocationCallback)
            }
        }
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        @SuppressLint("LogNotTimber" , "LongLogTag")
        override fun onLocationResult(locationResult: LocationResult) {
            val location: Location = locationResult.lastLocation
            latitude = location.latitude.toString()
            longitude = location.longitude.toString()
            try {
                val addresses: List<Address>
                val geocoder = Geocoder(mContext , Locale.getDefault())
                addresses = geocoder.getFromLocation(location.latitude , location.longitude , 1)
                val address = addresses[0].getAddressLine(0)
                addressToApi = address
            } catch (e: InvocationTargetException) {
                Log.e("InvocationTargetException" , "InvocationTargetException" + e.targetException)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<String> , grantResults: IntArray) {
        if (grantResults.isNotEmpty()) {
            if (requestCode == MY_PERMISSIONS_REQUEST_READ_FINE_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("LocationServiceRequest" , "location update")
            }
        }
    }

    private fun callDriverListAPI() {

        circle_load.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = WebTestRequest()
        request.companyCode = PrefManager(this).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(this).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(this).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(this).getStringValue(Constants.webUrlConst)

        val call = ApiClient.getService().getDriverList(request)
        call.enqueue(object : Callback<GetDriverList> {
            override fun onResponse(call: Call<GetDriverList> , response: Response<GetDriverList>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getGetShippingTypesResult()?.getCode().equals("0")) {

                        driverListResponse = ArrayList()
                        driverListResponse.clear()

                        if (response.body()?.getGetShippingTypesResult()?.getShippingTypes()?.getShippingType() != null) {
                            for (i in 0 until response.body()?.getGetShippingTypesResult()?.getShippingTypes()?.getShippingType()?.size !!) {

                                val driver = ShippingType()
                                driver.setCode(response.body() !!.getGetShippingTypesResult()?.getShippingTypes()?.getShippingType()?.get(i)?.getCode())
                                driver.setDesignation(response.body() !!.getGetShippingTypesResult()?.getShippingTypes()?.getShippingType()?.get(i)?.getDesignation())

                                driverListResponse.add(driver)
                            }
                            if (driverListResponse.size > 0) {
                                loadSpinner(driverListResponse)
                            }
                        }
                    }
                }
            }

            override fun onFailure(call: Call<GetDriverList> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(applicationContext !! , getString(R.string.something_went_wrong_try))
            }
        })
    }

    private fun loadSpinner(getShippingType: ArrayList<ShippingType>) {

        val spData: ArrayList<String> = ArrayList()
        for (i in 0 until getShippingType.size) {
            spData.add(getShippingType[i].getDesignation() !!)
        }

        driverList.clear()
        driverList.addAll(spData)

        val dataAdapter = DriverListAdapter(getShippingType , object : IsDriverChecked {
            override fun isDriverChecked(position: Int , isCheck: Boolean , driverName: String?) {
                selectedDriver(driverName , isCheck)
            }
        })

        spDriverName.adapter = dataAdapter
    }

    private fun selectedDriver(text: String? , isCheck: Boolean) {
        if (isCheck) {
            val orderList: ArrayList<TrackingListResponse> = ArrayList()
            orderList.clear()
            for (d in Singleton.instance.trackingListSingleton) {
                if (d.driverName !!.contains(text !!)) {
                    orderList.add(d)
                }
            }
            if (orderList.size > 0) {
                rv_tracking_list.visibility = View.VISIBLE
                tvNoItems.visibility = View.INVISIBLE
                recyclerListItems(orderList)
            } else {
                rv_tracking_list.visibility = View.INVISIBLE
                tvNoItems.visibility = View.VISIBLE
            }
        } else {
            rv_tracking_list.visibility = View.VISIBLE
            tvNoItems.visibility = View.INVISIBLE
            recyclerListItems(Singleton.instance.trackingListSingleton)
        }
    }

    private class DownloadFile(val activity: Activity , view: CirclesLoadingView) : AsyncTask<String? , String? , String?>() {

        private var filename: String? = null
        val pg = view
        var folder: File? = null

        override fun onPostExecute(file_url: String?) {
            activity.runOnUiThread {
                pg.visibility = View.GONE
            }
            val intent = Intent(activity , TransactionReceiptActivity::class.java)
            intent.putExtra("filename" , filename)
            intent.putExtra("isTracking" , true)
            activity.startActivity(intent)
            activity.overridePendingTransition(R.anim.enter , R.anim.exit)
            activity.finish()
        }

        override fun doInBackground(vararg params: String?): String? {
            activity.runOnUiThread {
                pg.visibility = View.VISIBLE
            }
            val fileUrl = params[0]
            filename = params[1]
            folder = File(activity.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) , "Winmax")
            folder?.mkdir()
            val pdfFile = File(folder , filename ?: "")
            try {
                pdfFile.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            FileDownloader.downloadFile(fileUrl , pdfFile)
            return null
        }
    }
}