package com.winmax.transfer

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import com.google.zxing.integration.android.IntentIntegrator
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.model.InvoiceListModel
import com.winmax.transfer.model.authenticateUser.request.AuthenticateUser
import com.winmax.transfer.model.authenticateUser.request.Request
import com.winmax.transfer.model.authenticateUser.response.AuthResponse
import com.winmax.transfer.model.createArticle.request.ArticleDetail
import com.winmax.transfer.model.createArticle.request.CreateArticleRequest
import com.winmax.transfer.model.createArticle.request.Credential
import com.winmax.transfer.model.createArticle.request.DocumentDetail
import com.winmax.transfer.model.createArticle.response.CreateArticleResponse
import com.winmax.transfer.model.getArticals.response.ArticleResponse
import com.winmax.transfer.model.getArticals.response.GetArticlesResult
import com.winmax.transfer.model.getItemCode.Article
import com.winmax.transfer.model.getShippingTypesResult.GetDriverList
import com.winmax.transfer.model.getShippingTypesResult.ShippingType
import com.winmax.transfer.model.itemCode.ItemCodeList
import com.winmax.transfer.model.webserviceConnection.WebTestRequest
import com.winmax.transfer.utils.AnyOrientationCaptureActivity
import com.winmax.transfer.utils.toast
import com.winmax.transfer.webservices.ApiClient
import kotlinx.android.synthetic.main.activity_add_product_detail_screen.btnShowItems
import kotlinx.android.synthetic.main.activity_add_product_detail_screen.btnUndo
import kotlinx.android.synthetic.main.activity_add_product_detail_screen.ivBack
import kotlinx.android.synthetic.main.activity_invoice_order.*
import kotlinx.android.synthetic.main.activity_invoice_order.circle_view
import kotlinx.android.synthetic.main.activity_invoice_order.etQty
import kotlinx.android.synthetic.main.activity_invoice_order.textViewToolBarTitle
import kotlinx.android.synthetic.main.activity_invoice_order.tvVersion
import kotlinx.android.synthetic.main.activity_item_code_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class InvoiceOrderActivity : AppCompatActivity() , KeyBoardListener , View.OnClickListener {

    private var keyboard: MyKeyboard? = null
    private var whichFocus = "itemCode"
    private var apiCalled = false
    private var isClickKeyBoard: Boolean = false
    private var sendingCode: String? = null
    private var receivingCode: String? = null
    private var articalRes: GetArticlesResult? = null
    private var isClickOrder: Boolean = false

    var driverList: ArrayList<String> = ArrayList()
    var driverListResponse = ArrayList<ShippingType>()

    var itemCodeList = ArrayList<Article>()
    var itemCode: ArrayList<String> = ArrayList()

    var orderStates = arrayOf("Exchange" , "Drop" , "PickUp")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invoice_order)

        setVersion()

        keyboard = findViewById(R.id.keyboardview)
        hideKeyboard()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        onClickListener()
        initializeKeyBoard()

        isClickOrder = PrefManager(applicationContext).getBooleanValue("orderClick")

        setButtonOrderInvoice()

        //callDriverAPI()

        if (PrefManager(this).getStringValue("designation") != "DRIVERS") {
            callDriverListAPI()
        } else {
            spDriver.visibility = View.INVISIBLE
            tvDriverName.visibility = View.VISIBLE

            val driverName = PrefManager(this).getStringValue("username")
            tvDriverName.setText(driverName)
        }

        getCurrentDate()

        loadSpinnerOrderState()

        val articleCode = intent.getStringExtra("articleCode")
        if (articleCode != null) {
            etItemCode.setText(articleCode)
            etItemCode.setSelection(articleCode.length)
            callApi(etItemCode.text.toString())
        }
    }

    private fun callArticleAPI() {

        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = WebTestRequest()
        request.companyCode = PrefManager(this).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(this).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(this).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(this).getStringValue(Constants.webUrlConst)

        val call = ApiClient.getService().getItemCode("" , request)
        call.enqueue(object : Callback<ArrayList<ItemCodeList>> {
            override fun onFailure(call: Call<ArrayList<ItemCodeList>> , t: Throwable) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(applicationContext !! , getString(R.string.something_went_wrong_try))
            }

            override fun onResponse(call: Call<ArrayList<ItemCodeList>> , response: Response<ArrayList<ItemCodeList>>) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        itemCodeList = ArrayList()
                        itemCodeList.clear()

                        response.body()?.forEach {
                            val itemCode = Article()
                            itemCode.articleCode = it.articleCode
                            itemCode.designation = it.designation
                            itemCodeList.add(itemCode)
                        }

                        if (itemCodeList.size > 0) {
                            loadItemDesignationSpinner(itemCodeList)
                        }
                    }
                }
            }
        })
    }

    private fun loadItemDesignationSpinner(itemCodeList: ArrayList<Article>) {

        val spData: ArrayList<String> = ArrayList()
        for (i in 0 until itemCodeList.size) {
            spData.add(itemCodeList[i].designation !!)
        }

        itemCode.clear()
        itemCode.addAll(spData)

        val arrayAdapter = ArrayAdapter(this , R.layout.spinner_item , itemCode)
        atItemDescription.threshold = 1
        atItemDescription.setAdapter(arrayAdapter)

        atItemDescription.onItemClickListener = AdapterView.OnItemClickListener { parent , _ , position , _ ->
            val designation = arrayAdapter.getItem(position)
            Log.e("getItemCode" , "getItemCode" + designation)

            itemCodeList.forEach {
                if (it.designation?.equals(designation) !!) {
                    val getDriverCode = it.articleCode

                    etItemCode.setText(getDriverCode)
                    etItemCode.setSelection(getDriverCode?.length !!)

                    callApi(getDriverCode)
                }
            }
        }
    }

    private fun callDriverAPI() {

        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = Request()
        val credential = com.winmax.transfer.model.authenticateUser.request.Credential()
        val authUser = AuthenticateUser()

        credential.setCompanyCode(PrefManager(this).getStringValue(Constants.companyCodeConst))
        credential.setUserLogin(PrefManager(this).getStringValue(Constants.userNameConst))
        credential.setUserPassword(PrefManager(this).getStringValue(Constants.passwordConst))
        credential.WebServiceURL = PrefManager(this).getStringValue(Constants.webUrlConst)

        authUser.setAuthenticationUser(PrefManager(this).getStringValue(Constants.userNameConst))
        authUser.setAuthenticationPassword(PrefManager(this).getStringValue(Constants.passwordConst))
        authUser.setOptionsToCheckAccess("1")

        request.setAuthenticateUser(authUser)
        request.setCredential(credential)

        val call = ApiClient.getService().authenticateUser(request)
        call.enqueue(object : Callback<AuthResponse> {
            override fun onResponse(call: Call<AuthResponse> , response: Response<AuthResponse>) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getAuthenticateUserResult()?.getCode().equals("0")) {
                        //tvDriver.setText(response.body()?.getAuthenticateUserResult()?.getUserAccount()?.getUserName())
                    }
                }
            }

            override fun onFailure(call: Call<AuthResponse> , t: Throwable) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(applicationContext !! , getString(R.string.something_went_wrong_try))
            }
        })
    }

    private fun callDriverListAPI() {

        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = WebTestRequest()
        request.companyCode = PrefManager(this).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(this).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(this).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(this).getStringValue(Constants.webUrlConst)

        val call = ApiClient.getService().getDriverList(request)
        call.enqueue(object : Callback<GetDriverList> {
            override fun onResponse(call: Call<GetDriverList> , response: Response<GetDriverList>) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getGetShippingTypesResult()?.getCode().equals("0")) {

                        callArticleAPI()

                        driverListResponse = ArrayList()
                        driverListResponse.clear()

                        if (response.body() !!.getGetShippingTypesResult()?.getShippingTypes()?.getShippingType() != null) {
                            for (i in 0 until response.body() !!.getGetShippingTypesResult()?.getShippingTypes()?.getShippingType()?.size !!) {

                                val driver = ShippingType()
                                driver.setCode(response.body() !!.getGetShippingTypesResult()?.getShippingTypes()?.getShippingType()?.get(i)?.getCode())
                                driver.setDesignation(response.body() !!.getGetShippingTypesResult()?.getShippingTypes()?.getShippingType()?.get(i)?.getDesignation())

                                driverListResponse.add(driver)
                            }
                            if (driverListResponse.size > 0) {
                                loadSpinner(driverListResponse)
                            }
                        }
                    }
                }
            }

            override fun onFailure(call: Call<GetDriverList> , t: Throwable) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(applicationContext !! , getString(R.string.something_went_wrong_try))
            }
        })
    }

    private fun loadSpinner(getShippingType: ArrayList<ShippingType>) {

        val spData: ArrayList<String> = ArrayList()
        for (i in 0 until getShippingType.size) {
            spData.add(getShippingType[i].getDesignation() !!)
        }

        driverList.clear()
        driverList.addAll(spData)

        val dataAdapter = ArrayAdapter<String>(this@InvoiceOrderActivity , android.R.layout.simple_spinner_item , driverList)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spDriver.adapter = dataAdapter

        PrefManager(this@InvoiceOrderActivity).setValue("getDriverCode" , getShippingType[0].getCode())

        spDriver.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                Log.e("onNothingSelected" , "onNothingSelected")

            }

            override fun onItemSelected(p0: AdapterView<*>? , p1: View? , position: Int , p3: Long) {
                val getDriverCode = getShippingType[position].getCode()
                PrefManager(this@InvoiceOrderActivity).setValue("getDriverCode" , getDriverCode)
            }
        }
    }

    private fun loadSpinnerOrderState() {

        val dataAdapter = ArrayAdapter<String>(this@InvoiceOrderActivity , android.R.layout.simple_spinner_item , orderStates)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        orderState_sp.adapter = dataAdapter

        orderState_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(adapter: AdapterView<*>?) {
                val orderState = adapter?.selectedItem.toString()
                Log.e("onNothingSelected" , "onNothingSelected" + orderState)
                PrefManager(this@InvoiceOrderActivity).setValue("OrderState" , orderState)
            }

            override fun onItemSelected(adapter: AdapterView<*>? , p1: View? , position: Int , p3: Long) {
                val orderState = adapter?.selectedItem.toString()
                Log.e("itemSelected" , "itemSelected$orderState")
                PrefManager(this@InvoiceOrderActivity).setValue("OrderState" , orderState)
            }
        }
    }

    private fun setButtonOrderInvoice() {
        if (isClickOrder) {
            btnOrderInvoice.text = getString(R.string.add_product)
            textViewToolBarTitle.text = getString(R.string.order)
            btnUndoShow.visibility = View.VISIBLE
            etDate.isEnabled = true
            etDate.setTextColor(getColor(R.color.hint_text_color))
        } else {
            btnOrderInvoice.text = getString(R.string.add_product)
            textViewToolBarTitle.text = getString(R.string.invoice)
            btnUndoShow.visibility = View.VISIBLE
            etDate.isEnabled = true
        }
    }

    private fun onClickListener() {
        btnShowItems.setOnClickListener(this)
        ivBack.setOnClickListener(this)
        btnOrderInvoice.setOnClickListener(this)
        btnUndo.setOnClickListener(this)
        etDate.setOnClickListener(this)

        if (! apiCalled) {
            btnOrderInvoice.alpha = 0.5f
            btnOrderInvoice.isEnabled = false
        } else {
            btnOrderInvoice.alpha = 1f
            btnOrderInvoice.isEnabled = true
        }

        etItemCode.setOnLongClickListener {
            val intent = Intent(this , ItemCodeListActivity::class.java)
            startActivity(intent)
            return@setOnLongClickListener false
        }
    }

    override fun onClick(v: View?) {
        when (v !!.id) {
            R.id.etDate -> {
                setCurrentDate()
            }
            R.id.btnOrderInvoice -> {
                if (isClickOrder) {
                    if (validateDetails())
                    //createOrder()
                        onClickButtonAddProducts()
                } else {
                    if (validateDetails())
                        onClickButtonAddProducts()
                }
            }
            R.id.btnShowItems -> {
                onClickButtonShowItems()
            }
            R.id.btnUndo -> {
                onClickButtonUndo()
            }
            R.id.ivBack -> {
                Singleton.instance.articleListRequestSingleton.clear()
                Singleton.instance.invoiceListSingleton.clear()
                startActivity(Intent(this , InvoiceActivity::class.java))
                overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right)
                finish()
            }
        }
    }

    private fun onClickButtonShowItems() {
        if (Singleton.instance.invoiceListSingleton.size > 0) {
            redirectToShowItem()
        } else {
            Constants.shortToast(this , "Please add product")
        }
    }

    private fun redirectToShowItem() {
        startActivity(Intent(this , InvoiceListActivity::class.java))
        overridePendingTransition(R.anim.enter , R.anim.exit)
        finish()
    }

    private fun initializeKeyBoard() {

        etItemCode.post {
            etItemCode.requestFocus()
        }

        etItemCode.setOnFocusChangeListener { _ , b ->
            if (b) {
                setFocusToItemCode()
            }
        }

        atItemDescription.setOnFocusChangeListener { _ , b ->
            if (b) {
                setFocusToItemDesignation()
            }
        }

        etItemPrice.setOnFocusChangeListener { _ , b ->
            if (b) {
                setFocusToItemPrice()
            }
        }

        etPrice.setOnFocusChangeListener { _ , hasFocus ->
            if (hasFocus) {
                //etPrice.inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL
                etPrice.setTextIsSelectable(true)
                etPrice.showSoftInputOnFocus = false //no response on keyboard touch
                etPrice.requestFocus()

                val ic: InputConnection = etPrice.onCreateInputConnection(EditorInfo()) !!
                keyboard?.setInputConnection(ic)

                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(etPrice.windowToken , 0)
                whichFocus = "itemPrice"
                hideKeyboard()
            }
        }

        etQty.setOnFocusChangeListener { _ , b ->
            if (b) {
                //  etQty.inputType = InputType.TYPE_CLASS_NUMBER
                etQty.setTextIsSelectable(true)
                etQty.showSoftInputOnFocus = false //no response on keyboard touch
                etQty.requestFocus()

                val ic: InputConnection = etQty.onCreateInputConnection(EditorInfo()) !!
                keyboard?.setInputConnection(ic)

                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(etQty.windowToken , 0)
                whichFocus = "itemQty"
                hideKeyboard()
            }
        }

        etItemCode.setOnEditorActionListener { _ , actionId , _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                etItemCode.inputType = InputType.TYPE_CLASS_NUMBER
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(etItemCode.windowToken , 0)
                true
            } else if (actionId == EditorInfo.IME_NULL) {
                etItemCode.inputType = InputType.TYPE_CLASS_NUMBER
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(etItemCode.windowToken , 0)
                true
            } else false
        }

    }

    private fun setFocusToItemPrice() {
        // etItemPrice.setRawInputType(InputType.TYPE_CLASS_NUMBER)
        etItemPrice.setTextIsSelectable(true)
        etItemPrice.showSoftInputOnFocus = false //no response on keyboard touch
        etItemPrice.requestFocus()

        val ic: InputConnection = etItemPrice.onCreateInputConnection(EditorInfo()) !!
        keyboard?.setInputConnection(ic)

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(etItemPrice.windowToken , 0)
        whichFocus = "itemPriceWithTax"
        hideKeyboard()
    }

    private fun setFocusToItemCode() {
        etItemCode.setRawInputType(InputType.TYPE_CLASS_NUMBER)
        etItemCode.setTextIsSelectable(true)
        etItemCode.showSoftInputOnFocus = false //no response on keyboard touch
        etItemCode.requestFocus()

        val ic: InputConnection = etItemCode.onCreateInputConnection(EditorInfo()) !!
        keyboard?.setInputConnection(ic)

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(etItemCode.windowToken , 0)
        whichFocus = "itemCode"
        hideKeyboard()
    }

    private fun setFocusToItemDesignation() {
        atItemDescription.setRawInputType(InputType.TYPE_CLASS_NUMBER)
        atItemDescription.setTextIsSelectable(true)
        atItemDescription.showSoftInputOnFocus = false //no response on keyboard touch
        atItemDescription.requestFocus()

        val ic: InputConnection = atItemDescription.onCreateInputConnection(EditorInfo()) !!
        keyboard?.setInputConnection(ic)

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(atItemDescription.windowToken , 0)
        whichFocus = "itemDesignation"
        hideKeyboard()
    }


    private fun setVersion() {
        tvVersion.text = PrefManager(applicationContext).getStringValue(Constants.versionConst)
    }

    private fun hideKeyboard() {
        val imm: InputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = this.currentFocus
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken , 0)
    }

    override fun clickKeyDone() {
        if (whichFocus == "itemCode") {
            sendingCode = PrefManager(applicationContext).getStringValue("sendingCode")
            receivingCode = PrefManager(applicationContext).getStringValue("receivingCode")
            if (etItemCode.text.toString().trim().isNotEmpty()) {
                callApi(etItemCode.text.toString().trim())
            } else {
                Constants.shortToast(applicationContext !! , "Please enter Item Code")
            }

        } else {
            if (apiCalled) {
                if (isClickOrder) {
                    if (validateDetails())
                    //  createOrder()
                        onClickButtonAddProducts()
                } else {
                    if (validateDetails())
                        onClickButtonAddProducts()
                }
            }
        }
    }

    override fun clickDone() {
        if (whichFocus == "itemCode") {
            sendingCode = PrefManager(applicationContext).getStringValue("sendingCode")
            receivingCode = PrefManager(applicationContext).getStringValue("receivingCode")
            if (etItemCode.text.toString().trim().isNotEmpty()) {
                callApi(etItemCode.text.toString().trim())
            } else {
                Constants.shortToast(applicationContext !! , "Please enter Item Code")
            }

        } else {
            if (apiCalled) {
                if (isClickOrder) {
                    if (validateDetails())
                    // createOrder()
                        onClickButtonAddProducts()
                } else {
                    if (validateDetails())
                        onClickButtonAddProducts()
                }
            }
        }
    }

    private fun onClickButtonAddProducts() {
        if (apiCalled) {
            if (etQty.text.toString().trim() != "0") {
                prepareItemListData()
            } else {
                Constants.shortToast(this , "Item Quantity cannot be 0!!")
            }
        }
    }

    override fun clickDelete() {
        if (whichFocus == "itemCode") {
            val length: Int = etItemCode.text !!.toString().trim().length
            if (length > 0) {
                etItemCode.text !!.delete(length - 1 , length)
            }
            //atItemDescription.setText("")
            //etItemDescription.setText("")
        } else if (whichFocus == "itemPrice") {
            val length: Int = etPrice.text !!.toString().trim().length
            if (length > 0) {
                etPrice.text !!.delete(length - 1 , length)
            }
        } else if (whichFocus == "itemDesignation") {
            val length: Int = atItemDescription.text !!.toString().length
            if (length > 0) {
                atItemDescription.text !!.delete(length - 1 , length)
            }
        } else if (whichFocus == "itemPriceWithTax") {
            val length: Int = etItemPrice.text !!.toString().length
            if (length > 0) {
                etItemPrice.text !!.delete(length - 1 , length)
            }
        } else {
            val length: Int = etQty.text !!.toString().trim().length
            if (length > 0) {
                etQty.text !!.delete(length - 1 , length)
            }
        }
    }

    override fun clickScan() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (whichFocus == "itemCode") {
            etItemCode.inputType = InputType.TYPE_CLASS_TEXT
            imm.showSoftInput(etItemCode , InputMethodManager.SHOW_FORCED)

            val integrator = IntentIntegrator(this)
            integrator.setRequestCode(Constants.REQUEST_CODE_SCANNER)
            integrator.setOrientationLocked(false)
            integrator.captureActivity = AnyOrientationCaptureActivity::class.java
            integrator.initiateScan()
        } else {
            toast("Feature enable for only item code")
        }
    }

    private fun onClickButtonUndo() {
        if (Singleton.instance.invoiceListSingleton.size > 0) {
            Singleton.instance.invoiceListSingleton.removeAt(Singleton.instance.invoiceListSingleton.size - 1)
            Constants.shortToast(this , "Removed last added item")
        } else {
            Constants.shortToast(this , "No product added!!")
        }
    }

    private fun callApi(itemCode: String) {
        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = WebTestRequest()
        request.companyCode = PrefManager(applicationContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(applicationContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(applicationContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(applicationContext).getStringValue(Constants.webUrlConst)

        val call = ApiClient.getService().getArticle(itemCode , request)

        call.enqueue(object : Callback<ArticleResponse> {
            override fun onResponse(call: Call<ArticleResponse> , response: Response<ArticleResponse>) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getArticlesResult?.code.equals("0")) {
                        Constants.shortToast(applicationContext !! , getString(R.string.success))
                        // etItemDescription.setText(response.body()?.getArticlesResult?.articles?.article?.designation.toString())
                        atItemDescription.setText(response.body()?.getArticlesResult?.articles?.article?.designation.toString())
                        atItemDescription.setSelection(response.body()?.getArticlesResult?.articles?.article?.designation.toString().length)
                        etItemPrice.setText(response.body()?.getArticlesResult?.articles?.article?.prices?.price?.salesPrice1WithTaxesFees.toString())
                        etPrice.setText(response.body()?.getArticlesResult?.articles?.article?.prices?.price?.salesPrice1WithoutTaxesFees.toString())

                        PrefManager(this@InvoiceOrderActivity).setValue("vatRate" , response.body()?.getArticlesResult?.articles?.article?.vatRate.toString())

                        articalRes = response.body()?.getArticlesResult
                        apiCalled = true

                        btnOrderInvoice.isEnabled = true
                        btnOrderInvoice.alpha = 1f

                        isClickKeyBoard = false
                    } else if (response.body()?.getArticlesResult?.code.equals("5")) {
                        Constants.shortToast(applicationContext !! , "Invalid Item Code!!")
                        atItemDescription.setText("")
                        //etItemDescription.setText("")
                    }
                } else {
                    Constants.shortToast(applicationContext !! , getString(R.string.failure))
                }
            }

            override fun onFailure(call: Call<ArticleResponse> , t: Throwable) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(applicationContext !! , getString(R.string.something_went_wrong))
            }
        })
    }

    private fun generateCreateArticleOrderRequest(): CreateArticleRequest {

        val createArticleRequest = CreateArticleRequest()

        val credential = com.winmax.transfer.model.createArticle.request.Credential()
        credential.setCompanyCode(PrefManager(applicationContext).getStringValue(Constants.companyCodeConst))
        credential.setUserLogin(PrefManager(applicationContext).getStringValue(Constants.userNameConst))
        credential.setUserPassword(PrefManager(applicationContext).getStringValue(Constants.passwordConst))
        credential.setWebServiceURL(PrefManager(applicationContext).getStringValue(Constants.webUrlConst))

        val documentDetail = DocumentDetail()
        documentDetail.setSourceWarehouseCode("1")
        documentDetail.setTargetWarehouseCode("1")
        documentDetail.setDocumentTypeCode("CO")
        documentDetail.setEntityCode(PrefManager(this).getStringValue("entityCode"))
        documentDetail.IsPOS = "0"
        documentDetail.IsExternal = "1"

        Singleton.instance.articleListRequestSingleton.clear()
        Singleton.instance.articleListRequestSingleton.add(generateArtDetails())

        createArticleRequest.setCredential(credential)
        createArticleRequest.setDocumentDetail(documentDetail)
        createArticleRequest.setArticleDetail(Singleton.instance.articleListRequestSingleton)

        createArticleRequest.stateOfOrder = PrefManager(this).getStringValue("OrderState")
        createArticleRequest.shippingTypeCode = PrefManager(this).getStringValue("getDriverCode")

        return createArticleRequest
    }


    private fun prepareItemListData() {

        circle_view.visibility = View.VISIBLE

        Handler().postDelayed(Runnable {
            circle_view.visibility = View.GONE
            Constants.longToast(this , "Product added.")
        } , 2000)

        sendingCode = PrefManager(this).getStringValue("sendingCode")
        receivingCode = PrefManager(this).getStringValue("receivingCode")

        Singleton.instance.invoiceListSingleton.add(InvoiceListModel(
                articalRes?.articles?.article?.articleCode !! , articalRes?.articles?.article?.designation !! , etQty.text.toString().trim() ,
                etPrice.text.toString().trim() , (etQty.text.toString().toFloat() * etPrice.text.toString().toDouble()).toString() , PrefManager(this).getStringValue("vatRate").toDouble()))

        Singleton.instance.createDocModel.add(generateCreateArticleRequest())
        Singleton.instance.articleListRequestSingleton.add(generateArtDetails())

        resetView()
    }

    private fun generateCreateArticleRequest(): CreateArticleRequest {
        val createArticleRequest = CreateArticleRequest()

        val credential = Credential()
        credential.setCompanyCode(PrefManager(this).getStringValue(Constants.companyCodeConst))
        credential.setUserLogin(PrefManager(this).getStringValue(Constants.userNameConst))
        credential.setUserPassword(PrefManager(this).getStringValue(Constants.passwordConst))
        credential.setWebServiceURL(PrefManager(this).getStringValue(Constants.webUrlConst))

        val documentDetail = DocumentDetail()
        documentDetail.setSourceWarehouseCode(sendingCode)
        documentDetail.setTargetWarehouseCode(receivingCode)
        documentDetail.setDocumentTypeCode("TO")
        documentDetail.setEntityCode(receivingCode)

        createArticleRequest.setCredential(credential)
        createArticleRequest.setDocumentDetail(documentDetail)

        return createArticleRequest
    }

    private fun generateArtDetails(): ArticleDetail {
        val articleDetail = ArticleDetail()
        articleDetail.articleCode = articalRes?.articles?.article?.articleCode
        articleDetail.articleDesignation = articalRes?.articles?.article?.designation
        articleDetail.quantity = etQty.text.toString().trim()
        return articleDetail
    }

    private fun resetView() {
        etItemCode.setText("")
        //etItemDescription.setText("")
        atItemDescription.setText("")
        //  etQty.setText("")
        etItemPrice.setText("")
        etPrice.setText("")
        etItemCode.requestFocus()
    }

    override fun onActivityResult(requestCode: Int , resultCode: Int , data: Intent?) {
        super.onActivityResult(requestCode , resultCode , data)
        try {
            if (requestCode == Constants.REQUEST_CODE_SCANNER) {
                onActivityResultFromScanner(resultCode , data)
            }
        } catch (e: Exception) {
            Log.e("logTag" , e.toString())
        }
    }

    //result: from scanner
    private fun onActivityResultFromScanner(resultCode: Int , data: Intent?) {
        try {
            val result = IntentIntegrator.parseActivityResult(resultCode , data)
            if (result != null) {
                if (result.contents == null) {
                    toast("Cancelled: Scanning")
                } else {
                    toast("Result : ${result.contents}")
                    //set result in edit text
                    isClickKeyBoard = true
                    etItemCode.setText(result.contents)
                    etItemCode.setSelection(result.contents.length)

                    //clear description
                    // etItemDescription.setText("")
                    atItemDescription.setText("")
                }
            }
        } catch (e: java.lang.Exception) {
            Log.e("logTag" , e.toString())
        }
    }

    private fun validateDetails(): Boolean {
        val quantity = etQty.text.toString().trim()
        val price = etPrice.text.toString().trim()
        when {
            quantity.isEmpty() -> {
                Toast.makeText(this , "Please enter quantity!" , Toast.LENGTH_LONG).show()
            }
            price.isEmpty() -> {
                Toast.makeText(this , "Please enter price!" , Toast.LENGTH_LONG).show()
            }
            else -> {
                return true
            }
        }
        return false
    }

    private fun createOrder() {
        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val call = ApiClient.getService().createDocApi(generateCreateArticleOrderRequest())
        call.enqueue(object : Callback<CreateArticleResponse> {
            override fun onResponse(call: Call<CreateArticleResponse> , response: Response<CreateArticleResponse>) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.winmax4Document?.isCreated.equals("true")) {
                        showSuccessDialog(response.body()?.winmax4Document?.header?.documentNumber.toString())
                    } else {
                        Constants.shortToast(applicationContext !! , getString(R.string.something_went_wrong))
                    }
                }
            }

            override fun onFailure(call: Call<CreateArticleResponse> , t: Throwable) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(applicationContext !! , getString(R.string.failure))
            }
        })
    }

    @SuppressLint("SetTextI18n")
    private fun setCurrentDate() {
        val cldr: Calendar = Calendar.getInstance()
        val day: Int = cldr.get(Calendar.DAY_OF_MONTH)
        val month: Int = cldr.get(Calendar.MONTH)
        val year: Int = cldr.get(Calendar.YEAR)

        val datepicker = DatePickerDialog(this@InvoiceOrderActivity ,
                OnDateSetListener { _ , year , monthOfYear , dayOfMonth ->
                    etDate.text = dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year
                } , year , month , day)
        datepicker.show()
    }

    private fun getCurrentDate() {
        val c = Calendar.getInstance().time
        val df = SimpleDateFormat("dd/MM/yyyy" , Locale.getDefault())
        val formattedDate = df.format(c)
        etDate.text = formattedDate
    }

    private fun showSuccessDialog(docNumber: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.doc_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val btnDone = dialog.findViewById(R.id.btnDone) as AppCompatButton
        val docNumberTv = dialog.findViewById(R.id.tvDoc_number) as AppCompatTextView
        docNumberTv.text = docNumber
        btnDone.setOnClickListener {
            resetView()
            dialog.dismiss()
            val to = Intent(this , SelectWareHouseScreen::class.java)
            to.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(to)
            overridePendingTransition(R.anim.enter , R.anim.exit)
            finish()
        }
        dialog.show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Singleton.instance.articleListRequestSingleton.clear()
        Singleton.instance.invoiceListSingleton.clear()
        startActivity(Intent(this , InvoiceActivity::class.java))
        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right)
        finish()
    }
}