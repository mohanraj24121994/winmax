package com.winmax.transfer;

import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;


public class MyKeyboard extends LinearLayout implements View.OnClickListener {

    KeyBoardListener keyBoardListener;

    //Scanner Input handle
    public static boolean isUserInput = false;
    public static StringBuilder keyBoardInput = new StringBuilder();

    // constructors
    public MyKeyboard(Context context) {
        this(context, null, 0);
    }

    public MyKeyboard(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyKeyboard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private TextView keyA;
    private TextView keyB;
    private TextView keyC;
    private TextView keyD;
    private TextView keyE;
    private TextView keyF;
    private TextView keyG;
    private TextView keyH;
    private TextView keyI;
    private TextView keyJ;
    private TextView keyK;
    private TextView keyL;
    private TextView keyM;
    private TextView keyN;
    private TextView keyO;
    private TextView keyP;
    private TextView keyQ;
    private TextView keyR;
    private TextView keyS;
    private TextView keyT;
    private TextView keyU;
    private TextView keyV;
    private TextView keyW;
    private TextView keyX;
    private TextView keyY;
    private TextView keyZ;

    private ImageView key_caps_switch;
    private ImageView caps_off_greek;
    private ImageView key_del;
    private ImageView keyDone;
    private ImageView keyDelete;
    private ImageView keyDelGreek;

    private TextView key_scan;
    private TextView key_done;
    private TextView tv_change_num_letter;
    private TextView tv_greek;
    private LinearLayout view_keyboard_numeric;
    private LinearLayout ll_special;
    private ConstraintLayout view_keyboard_alphabet;
    private ConstraintLayout view_greek_keyboard_small;


    // This will map the button resource id to the String value that we want to
    // input when that button is clicked.
    SparseArray<String> keyNumericValues = new SparseArray<>();
    SparseArray<String> keySymbols = new SparseArray<>();

    // Our communication link to the EditText
    InputConnection inputConnection;


    private TextView keysemi;
    private TextView keySigma;
    private TextView keyEpsilon;
    private TextView keyRho;
    private TextView keyTau;
    private TextView keyUpsilon;
    private TextView keyTheta;
    private TextView keyIota;
    private TextView keyOmicron;
    private TextView keyPi;
    private TextView keyAlpha;
    private TextView keyDelta;
    private TextView keyPhi;
    private TextView keyGamma;
    private TextView keyXi;
    private TextView keySigma1;
    private TextView keyEta;
    private TextView keyLambda;
    private TextView keyKappa;
    private TextView keyZeta;
    private TextView keyChi;
    private TextView keyPsi;
    private TextView keyOmega;
    private TextView keyBeta;
    private TextView keyNu;
    private TextView keyMu;

    private void init(Context context, AttributeSet attrs) {
        keyBoardListener = (KeyBoardListener) this.getContext();
        // initialize buttons
        LayoutInflater.from(context).inflate(R.layout.keyboard1, this, true);
        // keyboard keys (buttons)
        TextView key1 = findViewById(R.id.key1);
        TextView key2 = findViewById(R.id.key2);
        TextView key3 = findViewById(R.id.key3);
        TextView key4 = findViewById(R.id.key4);
        TextView key5 = findViewById(R.id.key5);
        TextView key6 = findViewById(R.id.key6);
        TextView key7 = findViewById(R.id.key7);
        TextView key8 = findViewById(R.id.key8);
        TextView key9 = findViewById(R.id.key9);
        TextView key0 = findViewById(R.id.key0);

        TextView key_plus = findViewById(R.id.key_plus);
        TextView key_under = findViewById(R.id.key_under);
        TextView key_minus = findViewById(R.id.key_minus);
        TextView key_star = findViewById(R.id.key_star);
        TextView key_un = findViewById(R.id.key_un);
        TextView key_percentage = findViewById(R.id.key_percentage);
        TextView key_dollar = findViewById(R.id.key_dollar);
        TextView key_hash = findViewById(R.id.key_hash);
        TextView key_at = findViewById(R.id.key_at);
        TextView key_esc = findViewById(R.id.key_esc);
        TextView key_slash = findViewById(R.id.key_slash);

        TextView key_star1 = findViewById(R.id.star);
        TextView key_hash1 = findViewById(R.id.hash);
        TextView key_dot = findViewById(R.id.key_dot);
        TextView tv_dot = findViewById(R.id.tv_dot);

        keyA = findViewById(R.id.keyA);
        keyB = findViewById(R.id.keyB);
        keyC = findViewById(R.id.keyC);
        keyD = findViewById(R.id.keyD);
        keyE = findViewById(R.id.keyE);
        keyF = findViewById(R.id.keyF);
        keyG = findViewById(R.id.keyG);
        keyH = findViewById(R.id.keyH);
        keyI = findViewById(R.id.keyI);
        keyJ = findViewById(R.id.keyJ);
        keyK = findViewById(R.id.keyK);
        keyL = findViewById(R.id.keyL);
        keyM = findViewById(R.id.keyM);
        keyN = findViewById(R.id.keyN);
        keyO = findViewById(R.id.keyO);
        keyP = findViewById(R.id.keyP);
        keyQ = findViewById(R.id.keyQ);
        keyR = findViewById(R.id.keyR);
        keyS = findViewById(R.id.keyS);
        keyT = findViewById(R.id.keyT);
        keyU = findViewById(R.id.keyU);
        keyV = findViewById(R.id.keyV);
        keyW = findViewById(R.id.keyW);
        keyX = findViewById(R.id.keyX);
        keyY = findViewById(R.id.keyY);
        keyZ = findViewById(R.id.keyZ);

        key_caps_switch = findViewById(R.id.key_caps_switch);
        caps_off_greek = findViewById(R.id.key_caps_small_greek);
        keyDelGreek = findViewById(R.id.keyDelGreekSmall);

        key_del = findViewById(R.id.keyDel);
        keyDone = findViewById(R.id.tv_done);
        keyDelete = findViewById(R.id.keyDelete);
        key_scan = findViewById(R.id.key_scan);
        key_done = findViewById(R.id.key_done);
        tv_change_num_letter = findViewById(R.id.tv_change);
        tv_greek = findViewById(R.id.tv_greek);
        view_keyboard_numeric = findViewById(R.id.view_keyboard_numeric);
        ll_special = findViewById(R.id.ll_special);
        view_keyboard_alphabet = findViewById(R.id.view_keyboard_alphabet);
        view_greek_keyboard_small = findViewById(R.id.view_greek_keyboard_small);


        /*--Greek keyBoard initialization--*/

        keysemi = findViewById(R.id.keysemi);
        keySigma = findViewById(R.id.keySmallς);
        keyEpsilon = findViewById(R.id.keyε);
        keyRho = findViewById(R.id.keyρ);
        keyTau = findViewById(R.id.keyτ);
        keyUpsilon = findViewById(R.id.keyυ);
        keyTheta = findViewById(R.id.keyθ);
        keyIota = findViewById(R.id.keyι);
        keyOmicron = findViewById(R.id.keyο);
        keyPi = findViewById(R.id.keyπ);
        keyAlpha = findViewById(R.id.keyα);
        keySigma1 = findViewById(R.id.keyσ);
        keyDelta = findViewById(R.id.keyδ);
        keyPhi = findViewById(R.id.keyφ);
        keyGamma = findViewById(R.id.keyγ);
        keyEta = findViewById(R.id.keyη);
        keyXi = findViewById(R.id.keyξ);
        keyKappa = findViewById(R.id.keyκ);
        keyLambda = findViewById(R.id.keyλ);
        keyZeta = findViewById(R.id.keyζ);
        keyChi = findViewById(R.id.keyχ);
        keyPsi = findViewById(R.id.keyψ);
        keyOmega = findViewById(R.id.keyω);
        keyBeta = findViewById(R.id.keyβ);
        keyNu = findViewById(R.id.keyν);
        keyMu = findViewById(R.id.keyμ);

        // set button click listeners
        key_plus.setOnClickListener(this);
        key_under.setOnClickListener(this);
        key_minus.setOnClickListener(this);
        key_star.setOnClickListener(this);
        key_un.setOnClickListener(this);
        key_percentage.setOnClickListener(this);
        key_dollar.setOnClickListener(this);
        key_hash.setOnClickListener(this);
        key_at.setOnClickListener(this);
        key_esc.setOnClickListener(this);
        key_slash.setOnClickListener(this);

        key_star1.setOnClickListener(this);
        key_hash1.setOnClickListener(this);
        tv_dot.setOnClickListener(this);
        key_dot.setOnClickListener(this);

        key0.setOnClickListener(this);
        key1.setOnClickListener(this);
        key2.setOnClickListener(this);
        key3.setOnClickListener(this);
        key4.setOnClickListener(this);
        key5.setOnClickListener(this);
        key6.setOnClickListener(this);
        key7.setOnClickListener(this);
        key8.setOnClickListener(this);
        key9.setOnClickListener(this);

        keyA.setOnClickListener(this);
        keyB.setOnClickListener(this);
        keyC.setOnClickListener(this);
        keyD.setOnClickListener(this);
        keyE.setOnClickListener(this);
        keyF.setOnClickListener(this);
        keyG.setOnClickListener(this);
        keyH.setOnClickListener(this);
        keyI.setOnClickListener(this);
        keyJ.setOnClickListener(this);
        keyK.setOnClickListener(this);
        keyL.setOnClickListener(this);
        keyM.setOnClickListener(this);
        keyN.setOnClickListener(this);
        keyO.setOnClickListener(this);
        keyP.setOnClickListener(this);
        keyQ.setOnClickListener(this);
        keyR.setOnClickListener(this);
        keyS.setOnClickListener(this);
        keyT.setOnClickListener(this);
        keyU.setOnClickListener(this);
        keyV.setOnClickListener(this);
        keyW.setOnClickListener(this);
        keyX.setOnClickListener(this);
        keyY.setOnClickListener(this);
        keyZ.setOnClickListener(this);


        keysemi.setOnClickListener(this);
        keySigma.setOnClickListener(this);
        keyEpsilon.setOnClickListener(this);
        keyRho.setOnClickListener(this);
        keyTau.setOnClickListener(this);
        keyUpsilon.setOnClickListener(this);
        keyTheta.setOnClickListener(this);
        keyIota.setOnClickListener(this);
        keyOmicron.setOnClickListener(this);
        keyPi.setOnClickListener(this);
        keyAlpha.setOnClickListener(this);
        keySigma1.setOnClickListener(this);
        keyDelta.setOnClickListener(this);
        keyPhi.setOnClickListener(this);
        keyGamma.setOnClickListener(this);
        keyEta.setOnClickListener(this);
        keyXi.setOnClickListener(this);
        keyKappa.setOnClickListener(this);
        keyLambda.setOnClickListener(this);
        keyZeta.setOnClickListener(this);
        keyChi.setOnClickListener(this);
        keyPsi.setOnClickListener(this);
        keyOmega.setOnClickListener(this);
        keyBeta.setOnClickListener(this);
        keyNu.setOnClickListener(this);
        keyMu.setOnClickListener(this);

        key_caps_switch.setOnClickListener(this);
        caps_off_greek.setOnClickListener(this);
        keyDelGreek.setOnClickListener(this);

        key_del.setOnClickListener(this);
        keyDone.setOnClickListener(this);
        keyDelete.setOnClickListener(this);
        key_scan.setOnClickListener(this);
        key_done.setOnClickListener(this);
        tv_change_num_letter.setOnClickListener(this);
        tv_greek.setOnClickListener(this);
        view_keyboard_numeric.setOnClickListener(this);

        // map buttons IDs to input strings
        keyNumericValues.put(R.id.key1, "1");
        keyNumericValues.put(R.id.key2, "2");
        keyNumericValues.put(R.id.key3, "3");
        keyNumericValues.put(R.id.key4, "4");
        keyNumericValues.put(R.id.key5, "5");
        keyNumericValues.put(R.id.key6, "6");
        keyNumericValues.put(R.id.key7, "7");
        keyNumericValues.put(R.id.key8, "8");
        keyNumericValues.put(R.id.key9, "9");
        keyNumericValues.put(R.id.key0, "0");
    }

    @Override
    public void onClick(View v) {
        if (inputConnection == null) return;
        switch (v.getId()) {
            case R.id.keyA:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.a_small : R.string.a_caps);
                break;
            case R.id.keyB:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.b_small : R.string.b_caps);
                break;
            case R.id.keyC:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.c_small : R.string.c_caps);
                break;
            case R.id.keyD:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.d_small : R.string.d_caps);
                break;
            case R.id.keyE:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.e_small : R.string.e_caps);
                break;
            case R.id.keyF:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.f_small : R.string.f_caps);
                break;
            case R.id.keyG:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.g_small : R.string.g_caps);
                break;
            case R.id.keyH:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.h_small : R.string.h_caps);
                break;
            case R.id.keyI:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.i_small : R.string.i_caps);
                break;
            case R.id.keyJ:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.j_small : R.string.j_caps);
                break;
            case R.id.keyK:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.k_small : R.string.k_caps);
                break;
            case R.id.keyL:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.l_small : R.string.l_caps);
                break;
            case R.id.keyM:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.m_small : R.string.m_caps);
                break;
            case R.id.keyN:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.n_small : R.string.n_caps);
                break;
            case R.id.keyO:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.o_small : R.string.o_caps);
                break;
            case R.id.keyP:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.p_small : R.string.p_caps);
                break;
            case R.id.keyQ:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.q_small : R.string.q_caps);
                break;
            case R.id.keyR:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.r_small : R.string.r_caps);
                break;
            case R.id.keyS:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.s_small : R.string.s_caps);
                break;
            case R.id.keyT:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.t_small : R.string.t_caps);
                break;
            case R.id.keyU:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.u_small : R.string.u_caps);
                break;
            case R.id.keyV:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.v_small : R.string.v_caps);
                break;
            case R.id.keyW:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.w_small : R.string.w_caps);
                break;
            case R.id.keyX:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.x_small : R.string.x_caps);
                break;
            case R.id.keyY:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.y_small : R.string.y_caps);
                break;
            case R.id.keyZ:
                appendValueToEditText(key_caps_switch.getTag().toString().equals("caps_off") ? R.string.z_small : R.string.z_caps);
                break;
            case R.id.key_caps_switch:
                setCapsOnOff((ImageView) v);
                break;

            case R.id.key_done:
                keyBoardListener.clickDone();
                break;
            case R.id.tv_done:
                keyBoardListener.clickKeyDone();
                break;
            case R.id.keyDel:
                keyBoardListener.clickDelete();
                break;
            case R.id.key_scan:
                keyBoardListener.clickScan();
                break;
            /*---Greek keyBoard values---*/

            case R.id.keysemi:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? ";" : ":");
                break;
            case R.id.keySmallς:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "ς" : "ς");
                break;
            case R.id.keyε:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "ε" : "Ε");
                break;
            case R.id.keyρ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "ρ" : "Ρ");
                break;
            case R.id.keyτ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "τ" : "Τ");
                break;
            case R.id.keyυ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "υ" : "Υ");
                break;
            case R.id.keyθ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "θ" : "Θ");
                break;
            case R.id.keyι:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "ι" : "Ι");
                break;
            case R.id.keyο:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "ο" : "Ο");
                break;
            case R.id.keyπ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "π" : "Π");
                break;
            case R.id.keyα:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "α" : "Α");
                break;
            case R.id.keyσ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "σ" : "Σ");
                break;
            case R.id.keyδ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "δ" : "Δ");
                break;
            case R.id.keyφ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "φ" : "Φ");
                break;
            case R.id.keyγ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "γ" : "Γ");
                break;
            case R.id.keyη:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "η" : "Η");
                break;
            case R.id.keyξ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "ξ" : "Ξ");
                break;
            case R.id.keyκ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "κ" : "Κ");
                break;
            case R.id.keyλ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "λ" : "Λ");
                break;
            case R.id.keyζ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "ζ" : "Ζ");
                break;
            case R.id.keyχ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "χ" : "Χ");
                break;
            case R.id.keyψ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "ψ" : "Ψ");
                break;
            case R.id.keyω:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "ω" : "Ω");
                break;
            case R.id.keyβ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "β" : "Β");
                break;
            case R.id.keyν:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "ν" : "Ν");
                break;
            case R.id.keyμ:
                appendValueToEditText(caps_off_greek.getTag().toString().equals("caps_off_greek") ? "μ" : "Μ");
                break;
            case R.id.key_caps_small_greek:
                setCapsOnOffGreeks((ImageView) v);
                break;
            case R.id.keyDelGreekSmall:
                keyBoardListener.clickDelete();
                break;
            /*------------------*/

            case R.id.key_plus:
                appendValueToEditText("+");
                break;
            case R.id.key_under:
                appendValueToEditText("_");
                break;
            case R.id.key_minus:
                appendValueToEditText("-");
                break;
            case R.id.key_star:
                appendValueToEditText("*");
                break;
            case R.id.key_un:
                appendValueToEditText("&");
                break;
            case R.id.key_percentage:
                appendValueToEditText("%");
                break;
            case R.id.key_dollar:
                appendValueToEditText("$");
                break;
            case R.id.key_hash:
                appendValueToEditText("#");
                break;
            case R.id.key_at:
                appendValueToEditText("@");
                break;
            case R.id.key_esc:
                appendValueToEditText("!");
                break;
            case R.id.key_slash:
                appendValueToEditText("/");
                break;
            case R.id.key_dot:
                appendValueToEditText(".");
                break;

            case R.id.key1:
                appendValueToEditText("1");
                break;
            case R.id.key2:
                appendValueToEditText("2");
                break;
            case R.id.key3:
                appendValueToEditText("3");
                break;
            case R.id.key4:
                appendValueToEditText("4");
                break;
            case R.id.key5:
                appendValueToEditText("5");
                break;
            case R.id.key6:
                appendValueToEditText("6");
                break;
            case R.id.key7:
                appendValueToEditText("7");
                break;
            case R.id.key8:
                appendValueToEditText("8");
                break;
            case R.id.key9:
                appendValueToEditText("9");
                break;
            case R.id.key0:
                appendValueToEditText("0");
                break;
            case R.id.star:
                appendValueToEditText("*");
                break;
            case R.id.hash:
                appendValueToEditText("#");
                break;
            case R.id.tv_dot:
                appendValueToEditText(".");
                break;
            case R.id.keyDelete:
                keyBoardListener.clickDelete();
                break;
            case R.id.tv_greek:
                ll_special.setVisibility(View.GONE);
                view_keyboard_alphabet.setVisibility(View.GONE);
                view_keyboard_numeric.setVisibility(View.GONE);
                view_greek_keyboard_small.setVisibility(View.VISIBLE);
                tv_change_num_letter.setText("123");
                break;
            case R.id.tv_change:
                if (view_keyboard_numeric.getVisibility() == View.GONE) {
                    view_keyboard_numeric.setVisibility(View.VISIBLE);
                    ll_special.setVisibility(View.GONE);
                    view_keyboard_alphabet.setVisibility(View.GONE);
                    view_greek_keyboard_small.setVisibility(View.GONE);
                    tv_change_num_letter.setText("abc");
                } else {
                    view_keyboard_numeric.setVisibility(View.GONE);
                    view_greek_keyboard_small.setVisibility(View.GONE);
                    ll_special.setVisibility(View.VISIBLE);
                    view_keyboard_alphabet.setVisibility(View.VISIBLE);
                    tv_change_num_letter.setText("123");
                }

           /* default:
                appendValueToEditText(keyNumericValues.get(v.getId()));*/
        }
    }

    private void setCapsOnOff(ImageView view) {

        if (view.getTag().toString().equals("caps_off")) {
            view.setTag("caps_on");
            view.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_caps_on, null));
            keyA.setText(R.string.a_caps);
            keyB.setText(R.string.b_caps);
            keyC.setText(R.string.c_caps);
            keyD.setText(R.string.d_caps);
            keyE.setText(R.string.e_caps);
            keyF.setText(R.string.f_caps);
            keyG.setText(R.string.g_caps);
            keyH.setText(R.string.h_caps);
            keyI.setText(R.string.i_caps);
            keyJ.setText(R.string.j_caps);
            keyK.setText(R.string.k_caps);
            keyL.setText(R.string.l_caps);
            keyM.setText(R.string.m_caps);
            keyN.setText(R.string.n_caps);
            keyO.setText(R.string.o_caps);
            keyP.setText(R.string.p_caps);
            keyQ.setText(R.string.q_caps);
            keyR.setText(R.string.r_caps);
            keyS.setText(R.string.s_caps);
            keyT.setText(R.string.t_caps);
            keyU.setText(R.string.u_caps);
            keyV.setText(R.string.v_caps);
            keyW.setText(R.string.w_caps);
            keyX.setText(R.string.x_caps);
            keyY.setText(R.string.y_caps);
            keyZ.setText(R.string.z_caps);
        } else {
            view.setTag("caps_off");
            view.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_caps_off, null));
            keyA.setText(R.string.a_small);
            keyB.setText(R.string.b_small);
            keyC.setText(R.string.c_small);
            keyD.setText(R.string.d_small);
            keyE.setText(R.string.e_small);
            keyF.setText(R.string.f_small);
            keyG.setText(R.string.g_small);
            keyH.setText(R.string.h_small);
            keyI.setText(R.string.i_small);
            keyJ.setText(R.string.j_small);
            keyK.setText(R.string.k_small);
            keyL.setText(R.string.l_small);
            keyM.setText(R.string.m_small);
            keyN.setText(R.string.n_small);
            keyO.setText(R.string.o_small);
            keyP.setText(R.string.p_small);
            keyQ.setText(R.string.q_small);
            keyR.setText(R.string.r_small);
            keyS.setText(R.string.s_small);
            keyT.setText(R.string.t_small);
            keyU.setText(R.string.u_small);
            keyV.setText(R.string.v_small);
            keyW.setText(R.string.w_small);
            keyX.setText(R.string.x_small);
            keyY.setText(R.string.y_small);
            keyZ.setText(R.string.z_small);
        }
    }

    private void setCapsOnOffGreeks(ImageView view) {
        if (view.getTag().toString().equals("caps_off_greek")) {
            view.setTag("caps_on_greek");
            view.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_caps_on, null));
            keysemi.setText(":");
            keyEpsilon.setText("Ε");
            keyRho.setText("Ρ");
            keyTau.setText("Τ");
            keyUpsilon.setText("Υ");
            keyTheta.setText("Θ");
            keyIota.setText("Ι");
            keyOmicron.setText("Ο");
            keyPi.setText("Π");
            keyAlpha.setText("Α");
            keySigma1.setText("Σ");
            keyDelta.setText("Δ");
            keyPhi.setText("Φ");
            keyGamma.setText("Γ");
            keyEta.setText("Η");
            keyXi.setText("Ξ");
            keyKappa.setText("Κ");
            keyLambda.setText("Λ");
            keyZeta.setText("Ζ");
            keyChi.setText("Χ");
            keyPsi.setText("Ψ");
            keyOmega.setText("Ω");
            keyBeta.setText("Β");
            keyNu.setText("Ν");
            keyMu.setText("Μ");
        } else {
            view.setTag("caps_off_greek");
            view.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_caps_off, null));
            keyEpsilon.setText("ε");
            keysemi.setText(";");
            keyRho.setText("ρ");
            keyTau.setText("τ");
            keyUpsilon.setText("υ");
            keyTheta.setText("θ");
            keyIota.setText("ι");
            keyOmicron.setText("ο");
            keyPi.setText("π");
            keyAlpha.setText("α");
            keySigma1.setText("σ");
            keyDelta.setText("δ");
            keyPhi.setText("φ");
            keyGamma.setText("γ");
            keyEta.setText("η");
            keyXi.setText("ξ");
            keyKappa.setText("κ");
            keyLambda.setText("λ");
            keyZeta.setText("ζ");
            keyChi.setText("χ");
            keyPsi.setText("ψ");
            keyOmega.setText("ω");
            keyBeta.setText("β");
            keyNu.setText("ν");
            keyMu.setText("μ");
        }
    }

    private void appendValueToEditText(String value) {
        isUserInput = true;
        keyBoardInput.append(value);
        inputConnection.commitText(value, 1);
    }

    private void appendValueToEditText(Integer value) {
        String text = getContext().getString(value);
        isUserInput = true;
        keyBoardInput.append(text);
        inputConnection.commitText(text, 1);
    }

    // The activity (or some parent or controller) must give us
    // a reference to the current EditText's InputConnection
    public void setInputConnection(InputConnection ic) {
        this.inputConnection = ic;
    }
}