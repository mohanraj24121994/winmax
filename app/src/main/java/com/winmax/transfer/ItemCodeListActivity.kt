package com.winmax.transfer

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.winmax.transfer.adapter.ItemCodeListAdapter
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.model.getItemCode.Article
import com.winmax.transfer.model.getItemCode.GetArticlesResponse
import com.winmax.transfer.model.itemCode.ItemCodeList
import com.winmax.transfer.model.webserviceConnection.WebTestRequest
import com.winmax.transfer.webservices.ApiClient
import kotlinx.android.synthetic.main.activity_item_code_list.*
import kotlinx.android.synthetic.main.activity_item_code_list.circle_load
import kotlinx.android.synthetic.main.activity_item_code_list.ivBack
import kotlinx.android.synthetic.main.activity_item_code_list.tvVersion
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ItemCodeListActivity : AppCompatActivity() , View.OnClickListener {

    private var mContext: Context? = null
    lateinit var mItemCodeListAdapter: ItemCodeListAdapter
    var mArticleList = ArrayList<ItemCodeList>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_code_list)

        mContext = this

        setVersion()

        callArticleAPI()

        setOnClickListener()
    }

    private fun setOnClickListener() {
        ivBack.setOnClickListener(this)
    }

    private fun setVersion() {
        tvVersion.text = PrefManager(applicationContext).getStringValue(Constants.versionConst)
    }

    private fun callArticleAPI() {
        circle_load.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = WebTestRequest()
        request.companyCode = PrefManager(this).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(this).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(this).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(this).getStringValue(Constants.webUrlConst)

        val call = ApiClient.getService().getItemCode("",request)
        call.enqueue(object : Callback<ArrayList<ItemCodeList>>{
            override fun onFailure(call: Call<ArrayList<ItemCodeList>> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(applicationContext !! , getString(R.string.something_went_wrong_try))
            }

            override fun onResponse(call: Call<ArrayList<ItemCodeList>> , response: Response<ArrayList<ItemCodeList>>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    mArticleList.clear()
                }

                response.body()?.forEach {
                    mArticleList.add(it)
                }

                rvItemList !!.layoutManager = LinearLayoutManager(mContext)
                mItemCodeListAdapter = ItemCodeListAdapter(this@ItemCodeListActivity , mArticleList)
                rvItemList.adapter = mItemCodeListAdapter
            }
        })
    }

    override fun onClick(v: View?) {
        when (v !!.id) {
            R.id.ivBack -> {
                finish()
            }
        }
    }
}