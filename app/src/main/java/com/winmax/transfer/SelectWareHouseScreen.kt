package com.winmax.transfer

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.gson.internal.LinkedTreeMap
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.databinding.ActivitySelectWareHouseScreenBinding
import com.winmax.transfer.locationupdate.BGLocationService
import com.winmax.transfer.locationupdate.LocationUpdateViewModel
import com.winmax.transfer.locationupdate.NetworkChangeReceiver
import com.winmax.transfer.model.GetWarehouseData
import com.winmax.transfer.model.LogoutRequest
import com.winmax.transfer.model.getWarehouse.Warehouse
import com.winmax.transfer.model.webserviceConnection.WebTestRequest
import com.winmax.transfer.search.ItemSearchActivity
import com.winmax.transfer.webservices.ApiClient
import kotlinx.android.synthetic.main.activity_select_ware_house_screen.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SelectWareHouseScreen : AppCompatActivity() , View.OnClickListener {

    var mContext: Context? = null
    var warehouseData = ArrayList<Warehouse>()
    private var sendingSpData: ArrayList<String> = ArrayList()
    private var receiveSpData: ArrayList<String> = ArrayList()
    var sendingCode: String? = null
    var receivingCode: String? = null

    private var networkChangeReceiver: NetworkChangeReceiver? = null
    private val REQUEST_LOCATION_PERMISSION = 786
    private val IGNORE_BATTERY_OPTIMIZATION_REQUEST = 1002
    lateinit var mServiceIntent: Intent

    companion object {
        const val REQUEST_CODE_FOREGROUND = 1544
        const val REQUEST_CODE_BACKGROUND = 1545
    }

    private lateinit var viewModel: LocationUpdateViewModel
    private lateinit var binding: ActivitySelectWareHouseScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            binding = DataBindingUtil.setContentView(this , R.layout.activity_select_ware_house_screen)
            viewModel = ViewModelProviders.of(this).get(LocationUpdateViewModel::class.java)
            binding.lifecycleOwner = this
            binding.viewModel = viewModel
        } catch (e: Exception) {
            Log.e("logTag" , e.toString())
        }

        mContext = this
        if (Constants.isNetworkAvailable(mContext !!)) {
            val isVisible = PrefManager(applicationContext).getBooleanValue("createTransfer")
            if (isVisible) {
                cl_create_transfer.visibility = View.VISIBLE
                getWarehouse()
            }
        } else {
            Constants.shortToast(mContext !! , getString(R.string.check_internet))
        }
        onClickListener()
        setVersion()

        visibleWareHouse()

        if (PrefManager(this).getStringValue("designation") == "DRIVERS") {
            callBackgroundService()
        }

        tvTittle.text = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
    }

    private fun requestPermission() {
        val hasLocationPermission = ActivityCompat.checkSelfPermission(this ,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
        if (! hasLocationPermission) {
            ActivityCompat.requestPermissions(this ,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION) , REQUEST_CODE_FOREGROUND)
        } else {
            val hasBackgroundLocationPermission = ActivityCompat.checkSelfPermission(this ,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED
            if (! hasBackgroundLocationPermission) {
                ActivityCompat.requestPermissions(this ,
                        arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION) , REQUEST_CODE_FOREGROUND)
            }
            callBackgroundService()
            networkChangeReceiver = NetworkChangeReceiver()
            registerReceiver(networkChangeReceiver , IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<String> , grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_FOREGROUND -> handlePermissionForForeground()
        }
        super.onRequestPermissionsResult(requestCode , permissions , grantResults)
    }

    private fun handlePermissionForForeground() {
        // requestBackgroundPermission()
    }

    override fun onResume() {
        super.onResume()
        if (PrefManager(this).getStringValue("designation") == "DRIVERS") {
            requestPermission()
        }
        Log.e("onResume" , "onResume")
    }

    override fun onDestroy() {
        super.onDestroy()
        if (PrefManager(this).getStringValue("designation") == "DRIVERS") {
            unregisterReceiver(networkChangeReceiver)
            stopService()
        }
    }

    private fun visibleWareHouse() {

        val isTransfer = PrefManager(applicationContext).getBooleanValue("isTransfer")
        if (isTransfer) {
            btnCreateTransfer.visibility = View.VISIBLE
        } else {
            btnCreateTransfer.visibility = View.GONE
        }
        val isOrder = PrefManager(applicationContext).getBooleanValue("isOrder")
        if (isOrder) {
            btnOrder.visibility = View.VISIBLE
        } else {
            btnOrder.visibility = View.GONE
        }
        val isItemSearch = PrefManager(applicationContext).getBooleanValue("isItemSearch")
        if (isItemSearch) {
            btnSearchItem.visibility = View.VISIBLE
        } else {
            btnSearchItem.visibility = View.GONE
        }
        val isPayment = PrefManager(applicationContext).getBooleanValue("isPayment")
        if (isPayment) {
            btnCustomer_payment.visibility = View.VISIBLE
        } else {
            btnCustomer_payment.visibility = View.GONE
        }
        val isInventory = PrefManager(applicationContext).getBooleanValue("isInventory")
        if (isInventory) {
            btnInventory.visibility = View.VISIBLE
        } else {
            btnInventory.visibility = View.GONE
        }
        val isTracking = PrefManager(applicationContext).getBooleanValue("isTracking")
        if (isTracking) {
            btnTracking.visibility = View.VISIBLE
        } else {
            btnTracking.visibility = View.GONE
        }
    }

    private fun onClickListener() {
        btnCreateTransfer.setOnClickListener(this)
        btnSearchItem.setOnClickListener(this)
        btnCustomer_payment.setOnClickListener(this)
        btnInventory.setOnClickListener(this)
        btnOrder.setOnClickListener(this)
        btnTracking.setOnClickListener(this)
        ivLogout.setOnClickListener(this)
    }

    private fun setVersion() {
        tvVersion.text = PrefManager(applicationContext).getStringValue(Constants.versionConst)
    }

    private fun getWarehouse() {
        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        val request = WebTestRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)


        val call = ApiClient.getService().getGetWareHouses(request)
        call.enqueue(object : Callback<GetWarehouseData> {
            override fun onResponse(call: Call<GetWarehouseData> , response: Response<GetWarehouseData>) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getGetWarehousesResult()?.getCode().equals("0")) {
                        Constants.shortToast(mContext !! , "Choose warehouses location")
                        response.body()?.getGetWarehousesResult()?.getWarehouses()?.warehouse?.let {
                            if (it is List<*>) {
                                it.forEach {
                                    val getCodeDesignation = it as LinkedTreeMap<* , *>
                                    val warehouse = Warehouse()
                                    warehouse.setCode(getCodeDesignation["Code"].toString())
                                    warehouse.setDesignation(getCodeDesignation["Designation"].toString())
                                    warehouseData.add(warehouse)
                                }
                            } else {
                                val getCodeDesignation = it as LinkedTreeMap<* , *>
                                val warehouse = Warehouse()
                                warehouse.setCode(getCodeDesignation["Code"].toString())
                                warehouse.setDesignation(getCodeDesignation["Designation"].toString())
                                warehouseData.add(warehouse)
                            }
                        }

                        if (Singleton.instance.warehouseData.size > 0) {
                            Singleton.instance.warehouseData.clear()
                        }
                        Singleton.instance.warehouseData = warehouseData
                        loadDataToSpinner(Singleton.instance.warehouseData)
                    } else {
                        Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                    }
                } else {
                    Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<GetWarehouseData> , t: Throwable) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            }
        })
    }

    private fun callLogoutAPI() {
        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = LogoutRequest()
        request.database = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.driver = PrefManager(mContext).getStringValue("getDriverCode")
        val call = ApiClient.getService().logoutAPI(request)
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String> , response: Response<String>) {
                if (response.isSuccessful) {
                    circle_view.visibility = View.GONE
                    Toast.makeText(this@SelectWareHouseScreen , response.body() , Toast.LENGTH_SHORT).show()
                    Singleton.instance.articleListSingletonInventory.clear()
                    PrefManager(mContext).isFirstTimeLaunch = false
                    PrefManager(mContext).setBooleanValue("isLoggedIn" , false)
                    PrefManager(mContext).setBooleanValue("createTransfer" , false)
                    val to = Intent(this@SelectWareHouseScreen , MainActivity::class.java)
                    startActivity(to)
                    overridePendingTransition(R.anim.enter , R.anim.exit)
                    finish()
                }
            }

            override fun onFailure(call: Call<String> , t: Throwable) {
                circle_view.visibility = View.GONE
                Toast.makeText(this@SelectWareHouseScreen , t.message , Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun loadDataToSpinner(warehouseData: ArrayList<Warehouse>) {
        val spData: ArrayList<String> = ArrayList()
        for (i in 0 until warehouseData.size) {
            spData.add(warehouseData[i].getDesignation() !!)
        }

        sendingSpData.clear()
        receiveSpData.clear()

        sendingSpData.addAll(spData)
        receiveSpData.addAll(spData)

        sendingSpData.add(0 , "Sending Warehouse")
        receiveSpData.add(0 , "Receive Warehouse")

        val arrayAdapter = ArrayAdapter(this , R.layout.spinner_item , sendingSpData)
        sp_select.adapter = arrayAdapter

        sp_select.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*> , view: View , position: Int , id: Long) {
                var pos = 1
                if (position != 0) {
                    pos = position - 1
                    sendingCode = warehouseData[pos].getCode()
                    PrefManager(mContext).setValue("sendingCode" , sendingCode)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        //Receive Spinner
        val arrayReceiveAdapter = ArrayAdapter(this , R.layout.spinner_item , receiveSpData)
        sp_receive.adapter = arrayReceiveAdapter

        sp_receive.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*> , view: View , position: Int , id: Long) {
                var pos = 1
                if (position != 0) {
                    pos = position - 1
                    receivingCode = warehouseData[pos].getCode()
                    PrefManager(mContext).setValue("receivingCode" , receivingCode)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnCreateTransfer -> {
                if (cl_create_transfer.visibility == View.INVISIBLE) {
                    cl_create_transfer.visibility = View.VISIBLE
                    PrefManager(mContext).setBooleanValue("createTransfer" , true)
                    if (Constants.isNetworkAvailable(mContext !!)) {
                        getWarehouse()
                    } else {
                        Constants.shortToast(mContext !! , getString(R.string.check_internet))
                    }
                } else {
                    onClickButtonCreateTransfer()
                }
            }
            R.id.btnSearchItem -> {
                startActivityItemSearch()
            }
            R.id.ivLogout -> {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Info")
                builder.setMessage("Are you sure you want to logout!")
                builder.setPositiveButton("Yes") { dialog , _ ->
                    dialog.cancel()
                    if (PrefManager(mContext).getStringValue("designation") != "DRIVERS") {
                        onClickButtonLogout()
                    } else {
                        stopService()
                        callLogoutAPI()
                    }
                }
                builder.setNegativeButton("No") { dialog , _ -> dialog.cancel() }
                builder.show()
            }
            R.id.btnCustomer_payment -> {
                startActivityPayment()
            }
            R.id.btnInventory -> {
                startActivityInventory()
            }
            R.id.btnOrder -> {
                startActivityInVoice()
            }
            R.id.btnTracking -> {
                startActivityTracking()
            }
        }
    }

    private fun startActivityTracking() {
        startActivity(Intent(this , TrackingActivity::class.java))
        overridePendingTransition(R.anim.enter , R.anim.exit)
    }

    private fun startActivityInVoice() {
        startActivity(Intent(this , InvoiceActivity::class.java))
        overridePendingTransition(R.anim.enter , R.anim.exit)
    }

    private fun startActivityItemSearch() {
        startActivity(Intent(this , ItemSearchActivity::class.java))
        overridePendingTransition(R.anim.enter , R.anim.exit)
    }

    private fun startActivityPayment() {
        startActivity(Intent(this , PaymentActivity::class.java))
        overridePendingTransition(R.anim.enter , R.anim.exit)
    }

    private fun startActivityAddProductScreen() {
        val intent = Intent(this , AddProductDetailScreen::class.java)
        intent.putExtra(Constants.EXTRA_KEY_SCREEN_FOR , getString(R.string.Transfer))
        startActivity(intent)
        overridePendingTransition(R.anim.enter , R.anim.exit)
    }

    private fun startActivityInventory() {
        val intent = Intent(this , AddProductDetailScreen::class.java)
        intent.putExtra(Constants.EXTRA_KEY_SCREEN_FOR , getString(R.string.inventory))
        startActivity(intent)
        overridePendingTransition(R.anim.enter , R.anim.exit)
    }

    private fun onClickButtonLogout() {
        Singleton.instance.articleListSingletonInventory.clear()
        PrefManager(mContext).isFirstTimeLaunch = false
        PrefManager(mContext).setBooleanValue("isLoggedIn" , false)
        PrefManager(mContext).setBooleanValue("createTransfer" , false)
        val to = Intent(this , MainActivity::class.java)
        startActivity(to)
        overridePendingTransition(R.anim.enter , R.anim.exit)
        finish()
    }

    private fun onClickButtonCreateTransfer() {
        when {
            sp_select.selectedItemPosition == 0 -> {
                Constants.shortToast(mContext !! , getString(R.string.Please_select_sending_warehouse))
            }
            sp_receive.selectedItemPosition == 0 -> {
                Constants.shortToast(mContext !! , getString(R.string.pls_select_recieve_warehouse))
            }
            else -> {
                startActivityAddProductScreen()
            }
        }
    }

    private fun callBackgroundService() {
        if (checkBackgroundDataRestricted()) {
            showSettingsPopup()
        } else {
            val pm = getSystemService(Context.POWER_SERVICE) as PowerManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (pm != null && ! pm.isIgnoringBatteryOptimizations(packageName)) {
                    askIgnoreOptimization()
                } else {
                    if (isLocationEnabled(this)) {
                        callStartService()
                    }
                }
            } else {
                callStartService()
            }
        }
    }

    private fun callStartService() {
        mServiceIntent = Intent(this , BGLocationService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(this , Intent(this , BGLocationService::class.java))
        } else {
            startService(mServiceIntent)
        }
    }

    fun stopService() {
        val intent = Intent(this , BGLocationService::class.java)
        intent.action = "StopService"
        startService(intent)
    }

    private fun askIgnoreOptimization() {
        val intent: Intent
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val packageName = packageName
            val pm = getSystemService(Context.POWER_SERVICE) as PowerManager
            if (! pm.isIgnoringBatteryOptimizations(packageName)) {
                intent = Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
                intent.data = Uri.parse("package:" + getPackageName())
                startActivityForResult(intent , IGNORE_BATTERY_OPTIMIZATION_REQUEST)
            }
        }
    }

    private fun checkBackgroundDataRestricted(): Boolean {
        val connMgr: ConnectivityManager =
                getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            when (connMgr.restrictBackgroundStatus) {
                ConnectivityManager.RESTRICT_BACKGROUND_STATUS_ENABLED ->     // Background data usage and push notifications are blocked for this app
                    return true
                ConnectivityManager.RESTRICT_BACKGROUND_STATUS_WHITELISTED , ConnectivityManager.RESTRICT_BACKGROUND_STATUS_DISABLED ->     // Data Saver is disabled or the app is whitelisted
                    return false
            }
        }
        return false
    }

    private fun showSettingsPopup() {
        val li = LayoutInflater.from(this)
        val promptsView: View = li.inflate(R.layout.setting_custom_dialog , null)
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setView(promptsView)
        alertDialogBuilder.setCancelable(true)
        val alertDialog: AlertDialog = alertDialogBuilder.create()
        val yesButton = promptsView.findViewById(R.id.btn_yes) as Button
        yesButton.setOnClickListener {
            startActivityForResult(Intent(Settings.ACTION_SETTINGS) , 0)
        }
        val dialogButton: Button = promptsView.findViewById(R.id.btn_no) as Button
        dialogButton.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    /*  private fun getLocationPermission(): Boolean {
          return ContextCompat.checkSelfPermission(this ,
                  Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
      }
  */
    /* private fun requestPermission() {
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) ActivityCompat.requestPermissions(
                 this ,
                 arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION , Manifest.permission.ACCESS_FINE_LOCATION) , REQUEST_LOCATION_PERMISSION
         ) else ActivityCompat.requestPermissions(this , arrayOf(Manifest.permission.ACCESS_FINE_LOCATION) ,
                 REQUEST_LOCATION_PERMISSION
         )
     }*/

    private fun isLocationEnabled(mContext: Context): Boolean {
        val lm = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        )
    }

    /* override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<String> , grantResults: IntArray) {
         if (grantResults.isNotEmpty()) {
             if (requestCode == REQUEST_LOCATION_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                 Log.e("requestCode" , "Permission granted")
             }
         }
     }*/
}
