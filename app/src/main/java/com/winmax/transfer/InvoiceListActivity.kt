package com.winmax.transfer

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.downloader.FileDownloader
import com.winmax.transfer.model.InvoiceListModel
import com.winmax.transfer.model.createArticle.request.ArticleDetail
import com.winmax.transfer.model.createArticle.request.CreateArticleRequest
import com.winmax.transfer.model.createArticle.request.DocumentDetail
import com.winmax.transfer.model.createArticle.response.CreateArticleResponse
import com.winmax.transfer.utils.CirclesLoadingView
import com.winmax.transfer.utils.OnEditTextChanged
import com.winmax.transfer.webservices.ApiClient
import kotlinx.android.synthetic.main.activity_invoice_list.*
import kotlinx.android.synthetic.main.activity_invoice_list.ivBack
import kotlinx.android.synthetic.main.activity_payment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class InvoiceListActivity : AppCompatActivity() , View.OnClickListener {

    private var listAdapter: InvoiceListAdapter? = null
    private val itemList: ArrayList<InvoiceListModel> = ArrayList()
    private var isClickOrder: Boolean = false
    private val PERMISSION_REQUEST_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invoice_list)

        inItViews()

        setOnClickListener()

        requestPermission()

        isClickOrder = PrefManager(applicationContext).getBooleanValue("orderClick")
    }

    private fun setOnClickListener() {
        btnCreate.setOnClickListener(this)
        ivBack.setOnClickListener(this)
    }

    private fun inItViews() {
        recyclerview !!.layoutManager = LinearLayoutManager(this)
        listAdapter = InvoiceListAdapter(addAllProducts() , this , object : OnEditTextChanged {
            override fun getTotalValue(percentage: Double , amount: Double) {
                tvTotalValue.text = "Sub Total : €$amount"
                val totalAmount = (amount / 100 * PrefManager(this@InvoiceListActivity).getStringValue("vatRate").toDouble())
                txtVAT.text = "VAT : $totalAmount"
                tvGRandTotalValue.text = "Total : " + String.format("%.1f" , amount + totalAmount)
            }
        })
        recyclerview !!.adapter = listAdapter
        listAdapter !!.notifyDataSetChanged()
    }

    private fun addAllProducts(): ArrayList<InvoiceListModel> {
        for (i in 0 until Singleton.instance.invoiceListSingleton.size)
            itemList.add(InvoiceListModel(Singleton.instance.invoiceListSingleton[i].itemCode ,
                    Singleton.instance.invoiceListSingleton[i].productName ,
                    Singleton.instance.invoiceListSingleton[i].quantity , Singleton.instance.invoiceListSingleton[i].price ,
                    Singleton.instance.invoiceListSingleton[i].total , Singleton.instance.invoiceListSingleton[i].percentage))
        return itemList
    }

    override fun onClick(v: View?) {
        when (v !!.id) {
            R.id.btnCreate -> {
                if (checkPermission()) {
                    callCreateDocumentAPI()
                    /* Singleton.instance.invoiceListSingleton.clear()
                     val intent = Intent(this , TransactionReceiptActivity::class.java)
                     intent.putExtra("filename" , "W4 Rec A MINTIKIS FARM LTD:16:05:2021:09:17:45.pdf")
                     intent.putExtra("isInvoice" , true)
                     startActivity(intent)
                     overridePendingTransition(R.anim.enter , R.anim.exit)
                     finish()*/
                } else {
                    showSettingsDialog()
                }
            }
            R.id.ivBack -> {
                startActivity(Intent(this , InvoiceOrderActivity::class.java))
                overridePendingTransition(R.anim.right_to_left , R.anim.left_to_right)
                finish()
            }
        }
    }

    private fun generateCreateArticleInvoiceRequest(): CreateArticleRequest {

        val createArticleRequest = CreateArticleRequest()
        val credential = com.winmax.transfer.model.createArticle.request.Credential()
        val documentDetail = DocumentDetail()


        for (i in 0 until Singleton.instance.createDocModel.size) {
            credential.setCompanyCode(Singleton.instance.createDocModel[i].getCredential()?.getCompanyCode())
            credential.setUserLogin(Singleton.instance.createDocModel[i].getCredential()?.getUserLogin())
            credential.setUserPassword(Singleton.instance.createDocModel[i].getCredential()?.getUserPassword())
            credential.setWebServiceURL(Singleton.instance.createDocModel[i].getCredential()?.getWebServiceURL())

            createArticleRequest.setCredential(credential)

            documentDetail.setSourceWarehouseCode("1")
            documentDetail.setTargetWarehouseCode("1")

            if (isClickOrder) {
                documentDetail.setDocumentTypeCode("CO")
            } else {
                documentDetail.setDocumentTypeCode("IV")
            }

            documentDetail.setEntityCode(PrefManager(this).getStringValue("entityCode"))
            documentDetail.IsPOS = "0"
            documentDetail.IsExternal = "1"

            createArticleRequest.setDocumentDetail(documentDetail)
        }

        val artDetailList: ArrayList<ArticleDetail> = ArrayList()
        for (i in 0 until Singleton.instance.articleListRequestSingleton.size) {
            val artDetails = ArticleDetail()
            artDetails.articleCode = Singleton.instance.articleListRequestSingleton[i].articleCode
            artDetails.articleDesignation = Singleton.instance.articleListRequestSingleton[i].articleDesignation
            artDetails.quantity = Singleton.instance.articleListRequestSingleton[i].quantity
            artDetailList.add(artDetails)
        }

        createArticleRequest.setCredential(credential)
        createArticleRequest.setDocumentDetail(documentDetail)
        createArticleRequest.setArticleDetail(artDetailList)

        createArticleRequest.stateOfOrder = PrefManager(this).getStringValue("OrderState")
        createArticleRequest.shippingTypeCode = PrefManager(this).getStringValue("getDriverCode")
        createArticleRequest.Location = PrefManager(this).getStringValue("locationValue")
        createArticleRequest.ZipCode = PrefManager(this).getStringValue("zipValue")
        createArticleRequest.Address = PrefManager(this).getStringValue("addressValue")

        return createArticleRequest
    }

    private fun callCreateDocumentAPI() {
        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val call = ApiClient.getService().createDocApi(generateCreateArticleInvoiceRequest())
        call.enqueue(object : Callback<CreateArticleResponse> {

            override fun onResponse(call: Call<CreateArticleResponse> , response: Response<CreateArticleResponse>) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.winmax4Document?.isCreated.equals("true")) {
                        if (Singleton.instance.invoiceListSingleton.size > 0) {
                            Singleton.instance.invoiceListSingleton.clear()
                            itemList.clear()
                        }
                        recyclerview !!.adapter = listAdapter
                        listAdapter !!.notifyDataSetChanged()

                        val c = Calendar.getInstance()
                        val dateformat = SimpleDateFormat(":dd:MM:yyyy:hh:mm:ss")
                        val datetime = dateformat.format(c.time)

                        val filename = "W4 Rec " + response.body()?.winmax4Document?.header?.entityName.toString() + datetime

                        val pdfURL = PrefManager(this@InvoiceListActivity).getStringValue(Constants.pdfURL)

                        val documentNumber = response.body()?.winmax4Document?.header?.documentNumber.toString()

                        DownloadFile(this@InvoiceListActivity , circle_view , documentNumber).execute(pdfURL + response.body()?.winmax4Document?.header?.archivedDocumentHTTPPath , "$filename.pdf")

                    } else {
                        Constants.shortToast(this@InvoiceListActivity , getString(R.string.something_went_wrong))
                    }
                }
            }

            override fun onFailure(call: Call<CreateArticleResponse> , t: Throwable) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(this@InvoiceListActivity , "Failure")
            }
        })
    }

    private fun showSuccessDialog(docNumber: String , fileName: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.doc_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val btnDone = dialog.findViewById(R.id.btnDone) as AppCompatButton
        val docNumberTv = dialog.findViewById(R.id.tvDoc_number) as AppCompatTextView
        docNumberTv.text = docNumber

        btnDone.setOnClickListener {
            dialog.dismiss()
            Singleton.instance.invoiceListSingleton.clear()
            /* val to = Intent(this , SelectWareHouseScreen::class.java)
             to.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
             startActivity(to)*/

            val intent = Intent(this , TransactionReceiptActivity::class.java)
            intent.putExtra("filename" , fileName)
            startActivity(intent)
            overridePendingTransition(R.anim.enter , R.anim.exit)
            finish()
        }
        dialog.show()
    }

    private class DownloadFile(activity: Activity , view: CirclesLoadingView , private var documentNumber: String) : AsyncTask<String? , String? , String?>() {

        val activity = activity
        private var filename: String? = null
        val pg = view
        var folder: File? = null

        private fun showSuccessDialog(docNumber: String) {
            val dialog = Dialog(activity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.doc_dialog)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val btnDone = dialog.findViewById(R.id.btnDone) as AppCompatButton
            val docNumberTv = dialog.findViewById(R.id.tvDoc_number) as AppCompatTextView
            docNumberTv.text = docNumber

            btnDone.setOnClickListener {
                dialog.dismiss()
                Singleton.instance.invoiceListSingleton.clear()
                val intent = Intent(activity , TransactionReceiptActivity::class.java)
                intent.putExtra("filename" , filename)
                intent.putExtra("isInvoice" , true)
                activity.startActivity(intent)
                activity.overridePendingTransition(R.anim.enter , R.anim.exit)
                activity.finish()
            }
            dialog.show()
        }

        override fun onPostExecute(file_url: String?) {
            activity.runOnUiThread {
                pg.visibility = View.GONE
            }
            showSuccessDialog(documentNumber)
        }

        override fun doInBackground(vararg params: String?): String? {
            activity.runOnUiThread {
                pg.visibility = View.VISIBLE
            }
            val fileUrl = params[0]
            filename = params[1]
            //val extStorageDirectory = Environment.getExternalStorageDirectory().toString()
           // folder = File(extStorageDirectory , "Winmax")
            folder = File(activity.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) , "Winmax")

            folder?.mkdir()
            val pdfFile = File(folder , filename ?: "")
            try {
                pdfFile.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            FileDownloader.downloadFile(fileUrl , pdfFile)
            return null
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<String> , grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("value" , "Permission Granted, Now you can use local drive .")
            } else {
                Toast.makeText(this , "Cancelling, required permissions are not granted" , Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this , arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE) , PERMISSION_REQUEST_CODE)
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this , Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs storage permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS") { dialog , which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialog , which -> dialog.cancel() }
        builder.show()
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package" , packageName , null)
        intent.data = uri
        startActivityForResult(intent , PERMISSION_REQUEST_CODE)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this , InvoiceOrderActivity::class.java))
        overridePendingTransition(R.anim.right_to_left , R.anim.left_to_right)
        finish()
    }
}