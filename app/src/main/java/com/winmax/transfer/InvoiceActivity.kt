package com.winmax.transfer

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.winmax.transfer.adapter.AddressListAdapter
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.databinding.ActivityInvoiceBinding
import com.winmax.transfer.model.createentity.AdditionalAddress
import com.winmax.transfer.model.createentity.CreateEntity
import com.winmax.transfer.model.createentity.Credential
import com.winmax.transfer.model.createentity.response.CreateEntityResponse
import com.winmax.transfer.model.getentitiesbyphone.Address
import com.winmax.transfer.model.getentitiesbyphone.Entity
import com.winmax.transfer.model.getentitiesbyphone.GetEntitiesByPhone
import com.winmax.transfer.model.getentitiesbyphone.request.GetEntitiesByPhoneRequest
import com.winmax.transfer.model.getentitydescription.GetEntityDescription
import com.winmax.transfer.model.getentitydescription.PhoneNumberRequest
import com.winmax.transfer.model.payment.EntityDetail
import com.winmax.transfer.model.payment.entityrequest.EntityRequest
import com.winmax.transfer.webservices.ApiClient
import kotlinx.android.synthetic.main.activity_invoice.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InvoiceActivity : AppCompatActivity() , View.OnClickListener {

    lateinit var viewBinding: ActivityInvoiceBinding
    private lateinit var viewModel: InvoiceViewModel
    private var mContext: Context? = null
    private var isCreateEntity: Boolean = false
    var entityData = ArrayList<Entity>()
    var itemListData = ArrayList<Address>()
    var entityspData: ArrayList<String> = ArrayList()
    private var editAPI: Boolean = false
    private var isCreate: Boolean = true
    var position: Int = 0
    var getPosition: Int = 0

    private var listAdapter: AddressListAdapter? = null
    private var isScroll: Boolean = true
    private var isAPICAll: Boolean = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_invoice)
        viewBinding = DataBindingUtil.setContentView(this , R.layout.activity_invoice)
        viewModel = ViewModelProviders.of(this).get(InvoiceViewModel::class.java)
        viewBinding.viewModel = viewModel

        mContext = this

        setVersion()

        onClickListener()

        phoneNumberDoneClick()

        buttonDisable()

        val phoneNumber = intent.getStringExtra("PhoneNumber")
        if (phoneNumber != null) {
            view_edit_text_phone_num.setText(phoneNumber)
            view_edit_text_phone_num.setSelection(phoneNumber.length)
            getEntityByPhoneNumber(view_edit_text_phone_num.text.toString())
        }
        if (Constants.isNetworkAvailable(mContext !!)) {
            // getEntityDetail()
            getEntityDescriptionList()
        }
    }

    private fun phoneNumberDoneClick() {

        view_edit_text_phone_num.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                // if (s !!.isNotEmpty()){
                edittextDisable()
                etAddress.setText("")
                etDesignation.setText("")
                etLocality.setText("")
                etPostalCode.setText("")
                etRemarks.setText("")
                //  sp_selec.setText("")
                buttonDisable()
                tvEdit.visibility = View.GONE
                editAPI = false
                // }
            }

            override fun beforeTextChanged(s: CharSequence? , start: Int , count: Int , after: Int) {
            }

            override fun onTextChanged(s: CharSequence? , start: Int , before: Int , count: Int) {
            }
        })

        sp_selec.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

                etAddress.setText("")
                etDesignation.setText("")
                etLocality.setText("")
                etPostalCode.setText("")
                etRemarks.setText("")
                buttonDisable()
                tvEdit.visibility = View.GONE
                editAPI = false
            }

            override fun beforeTextChanged(p0: CharSequence? , p1: Int , p2: Int , p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence? , p1: Int , p2: Int , p3: Int) {
            }
        })

        view_edit_text_phone_num.setOnEditorActionListener { _ , actionId , event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                // if (! isCreateEntity) {
                getEntityByPhoneNumber(view_edit_text_phone_num.text.toString())
                //}
            }
            false
        }
    }

    private fun getEntityByPhoneNumber(phoneNumber: String) {
        circle_load.visibility = View.VISIBLE

        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = GetEntitiesByPhoneRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)
        request.PhoneNumber = phoneNumber

        val call = ApiClient.getService().getEntityByPhoneNumber(request)
        call.enqueue(object : Callback<GetEntitiesByPhone> {

            override fun onResponse(call: Call<GetEntitiesByPhone> , response: Response<GetEntitiesByPhone>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    isAPICAll = true
                    if (response.body() !!.getEntitiesResult !!.code.equals("0") ||
                            response.body() !!.getEntitiesResult !!.code == null) {
                        if (response.body() !!.getEntitiesResult != null) {
                            if (response.body() !!.getEntitiesResult !!.entities != null) {

                                Singleton.instance.showAddress.clear()
                                cvAddress.visibility = View.GONE
                                rvAddress.visibility = View.VISIBLE

                                if (response.body() !!.getEntitiesResult !!.entities?.entity != null) {
                                    //  val showAddress = ArrayList<Address>()
                                    val addAddress = Address()
                                    addAddress.location = response.body() !!.getEntitiesResult !!.entities?.entity?.location
                                    addAddress.zipCode = response.body() !!.getEntitiesResult !!.entities?.entity?.zipCode
                                    addAddress.address = response.body() !!.getEntitiesResult !!.entities?.entity?.address
                                    addAddress.remarks = response.body() !!.getEntitiesResult !!.entities?.entity?.remarks
                                    addAddress.phone = response.body() !!.getEntitiesResult !!.entities?.entity?.mobilePhone
                                    //  showAddress.add(addAddress)
                                    Singleton.instance.showAddress.add(addAddress)
                                }

                                setInvoiceOrderValue(response.body() !!.getEntitiesResult !!.entities?.entity)

                                PrefManager(mContext).setValue("entityCode" , response.body() !!.getEntitiesResult !!.entities?.entity !!.code)
                                setButtonEnable()
                                edittextDisable()

                                tvEdit.visibility = View.GONE

                                if (response.body() !!.getEntitiesResult !!.entities?.entity?.aditionalAddresses != null) {
                                    response.body() !!.getEntitiesResult !!.entities?.entity?.aditionalAddresses?.address?.forEach {
                                        //if(Singleton.instance.showAddress.size > 3)
                                        Singleton.instance.showAddress.add(it !!)
                                    }
                                }

                                if (Singleton.instance.showAddress.size > 0) {

                                    //clearOnScrollListeners();
                                    //setOnFlingListener(null);
                                    rvAddress.onFlingListener = null

                                    //if (rvAddress.onFlingListener == null) {
                                    val mSnapHelper = PagerSnapHelper()
                                    // mSnapHelper.attachToRecyclerView(null)
                                    mSnapHelper.attachToRecyclerView(rvAddress)
                                    // }
                                    val recylerViewLayoutManager = LinearLayoutManager(mContext , LinearLayoutManager.HORIZONTAL , false)

                                    // rvAddress !!.layoutManager = LinearLayoutManager(mContext , LinearLayoutManager.HORIZONTAL , false)
                                    rvAddress !!.layoutManager = recylerViewLayoutManager
                                    //linearLayoutManager.scrollToPositionWithOffset(desiredindex, 0);
                                    //listAdapter = AddressListAdapter(this@InvoiceActivity , Singleton.instance.showAddress.take(5)) { itemList , i , view ->
                                    listAdapter = AddressListAdapter(this@InvoiceActivity , Singleton.instance.showAddress) { itemList , i , view ->
                                        when (view.id) {
                                            R.id.tvAdd -> {
                                                // if (Singleton.instance.showAddress.size < 3) {
                                                isCreate = true
                                                editAPI = true
                                                isScroll = false

                                                rvAddress.visibility = View.GONE
                                                cvAddress.visibility = View.VISIBLE
                                                enableEdittext()
                                                etAddress.setText("")
                                                etDesignation.setText("")
                                                etLocality.setText("")
                                                etPostalCode.setText("")
                                                etRemarks.setText("")
                                                buttonDisable()

                                                if (Singleton.instance.showAddress.size > 0) {
                                                    tvRemarks.visibility = View.GONE
                                                    etRemarks.visibility = View.GONE

                                                    etDesignation.visibility = View.VISIBLE
                                                    tvetDesignation.visibility = View.VISIBLE
                                                }


                                                // }
                                            }
                                            R.id.tvEdit -> {
                                                isCreate = false
                                                isScroll = false


                                                rvAddress.visibility = View.GONE
                                                cvAddress.visibility = View.VISIBLE

                                                etAddress.isEnabled = true
                                                etLocality.isEnabled = true
                                                etPostalCode.isEnabled = true
                                                etRemarks.isEnabled = true

                                                etAddress.setText(itemList.get(i).address)

                                                position = i

                                                if (i != 0) {
                                                    tvRemarks.visibility = View.GONE
                                                    etRemarks.visibility = View.GONE
                                                    etDesignation.visibility = View.VISIBLE
                                                    tvetDesignation.visibility = View.VISIBLE
                                                    etDesignation.setText(itemList.get(i).designation)
                                                } else {
                                                    tvRemarks.visibility = View.VISIBLE
                                                    etRemarks.visibility = View.VISIBLE
                                                    etDesignation.visibility = View.GONE
                                                    tvetDesignation.visibility = View.GONE
                                                    etRemarks.setText(itemList.get(i).remarks)
                                                }
                                                etLocality.setText(itemList.get(i).location)
                                                etPostalCode.setText(itemList.get(i).zipCode)
                                                btnCreateEntity.visibility = View.VISIBLE
                                                btnCreateEntity.text = "Update"
                                                buttonDisable()
                                            }
                                        }
                                    }

                                    rvAddress !!.adapter = listAdapter
                                    rvAddress.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                        override fun onScrolled(recyclerView: RecyclerView , dx: Int , dy: Int) {
                                            super.onScrolled(recyclerView , dx , dy)
                                            val currentPosition = recylerViewLayoutManager.findLastVisibleItemPosition()
                                            if (currentPosition != - 1) {
                                                getPosition = currentPosition
                                                Log.e("firstVisibleItem" , "3==>" + getPosition)
                                            }
                                        }
                                    })
                                    listAdapter !!.notifyDataSetChanged()
                                }

                            } else {
                                Singleton.instance.showAddress.clear()
                                sp_selec.setText("")
                                createEntityDialog()
                                //Constants.shortToast(mContext !! , "Entered Mobile number not available, kindly create new entity!")
                                // isCreateEntity = true
                                // enableEdittext()
                            }
                        }
                    } else {
                        Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                    }
                } else {
                    Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<GetEntitiesByPhone> , t: Throwable) {
                isAPICAll = true
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Singleton.instance.showAddress.clear()
                sp_selec.setText("")
                createEntityDialog()
                //Constants.shortToast(mContext !! , "Entered Mobile number not available, kindly create new entity!")
                //isCreateEntity = true
                //enableEdittext()
            }
        })
    }

    private fun setValuePreference(positon: Int) {
        PrefManager(mContext).setValue("addressValue" , Singleton.instance.showAddress[positon].address)
        PrefManager(mContext).setValue("zipValue" , Singleton.instance.showAddress[positon].zipCode)
        PrefManager(mContext).setValue("locationValue" , Singleton.instance.showAddress[positon].location)

        navigateToOrder()
    }

    private fun enableEdittext() {
        etAddress.isEnabled = true
        etLocality.isEnabled = true
        etPostalCode.isEnabled = true
        etRemarks.isEnabled = true
        etDesignation.isEnabled = true
        btnCreateEntity.visibility = View.VISIBLE
    }

    private fun setInvoiceOrderValue(entity: Entity?) {
        etentity.isFocusable = false
        etentity.setText(entity !!.name)
        sp_selec.setText(entity.name)
        sp_selec.setSelection(entity.name !!.length)
        etAddress.setText(entity.address)
        etPostalCode.setText(entity.zipCode)
        etLocality.setText(entity.location)
        etRemarks.setText(entity.remarks)
    }

    private fun setButtonEnable() {
        btnOrder.isEnabled = true
        btnInVoice.isEnabled = true
        btnOrder.alpha = 1.0f
        btnInVoice.alpha = 1.0f
    }

    private fun buttonDisable() {
        btnOrder.isEnabled = false
        btnInVoice.isEnabled = false
        btnOrder.alpha = 0.5f
        btnInVoice.alpha = 0.5f
    }

    private fun onClickListener() {
        btnOrder.setOnClickListener(this)
        btnInVoice.setOnClickListener(this)
        btnCreateEntity.setOnClickListener(this)
        ivBack.setOnClickListener(this)
        tvEdit.setOnClickListener(this)

        view_edit_text_phone_num.setOnLongClickListener {
            val intemn = Intent(this , CallHistoryActivity::class.java)
            startActivity(intemn)
            return@setOnLongClickListener false
        }
    }

    private fun setVersion() {
        tvVersion.text = PrefManager(applicationContext).getStringValue(Constants.versionConst)
    }

    private fun navigateToOrder() {
        PrefManager(mContext).setBooleanValue("orderClick" , true)
        startActivity(Intent(this , InvoiceOrderActivity::class.java))
        overridePendingTransition(R.anim.enter , R.anim.exit)
        finish()
    }

    private fun navigateToInVoice() {
        PrefManager(mContext).setBooleanValue("orderClick" , false)
        startActivity(Intent(this , InvoiceOrderActivity::class.java))
        overridePendingTransition(R.anim.enter , R.anim.exit)
        finish()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnOrder -> {
                Singleton.instance.articleListRequestSingleton.clear()
                Singleton.instance.invoiceListSingleton.clear()
                setValuePreference(getPosition)
            }
            R.id.btnInVoice -> {
                Singleton.instance.articleListRequestSingleton.clear()
                Singleton.instance.invoiceListSingleton.clear()
                navigateToInVoice()
            }
            R.id.btnCreateEntity -> {
                if (isCreate) {
                    if (validateDetails())
                        createEntityAPI()
                } else {
                    updateEntityAPI()
                }
            }
            R.id.ivBack -> {
                finish()
            }
            R.id.tvEdit -> {
                editAPI = true
                btnCreateEntity.text = "Update"
                enableEdittext()
                buttonDisable()
            }
        }
    }

    private fun updateEntityAPI() {

        circle_load.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = CreateEntity()

        Singleton.instance.showAddress.get(position).address = etAddress.text.toString().trim()
        Singleton.instance.showAddress.get(position).zipCode = etPostalCode.text.toString().trim()
        Singleton.instance.showAddress.get(position).designation = etDesignation.text.toString().trim()
        Singleton.instance.showAddress.get(position).location = etLocality.text.toString().trim()

        val additionalAddress = ArrayList<AdditionalAddress>()

        if (position == 0) {
            request.phone = view_edit_text_phone_num.text.toString().trim()
            request.name = sp_selec.text.toString().trim()
            request.address = etAddress.text.toString().trim()
            request.zipCode = etPostalCode.text.toString().trim()
            request.location = etLocality.text.toString().trim()
            request.remarks = etRemarks.text.toString().trim()
            request.entityType = "Customer"
        } else {
            request.phone = Singleton.instance.showAddress[0].phone
            request.name = etentity.text.toString().trim()
            request.address = Singleton.instance.showAddress[0].address
            request.zipCode = Singleton.instance.showAddress[0].zipCode
            request.location = Singleton.instance.showAddress[0].location
            request.remarks = Singleton.instance.showAddress[0].remarks
            request.entityType = "Customer"
        }

        for (i in 1 until Singleton.instance.showAddress.size) {
            val addAddress = AdditionalAddress()
            addAddress.address = Singleton.instance.showAddress.get(i).address
            addAddress.designation = Singleton.instance.showAddress.get(i).designation
            addAddress.location = Singleton.instance.showAddress.get(i).location
            addAddress.zipCode = Singleton.instance.showAddress.get(i).zipCode
            additionalAddress.add(addAddress)
        }

        val createEntityCredential = Credential()
        createEntityCredential.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        createEntityCredential.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        createEntityCredential.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        createEntityCredential.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)

        request.credential = createEntityCredential

        request.additionalAddress = additionalAddress

        val call = ApiClient.getService().createEntity(request)
        call.enqueue(object : Callback<CreateEntityResponse> {
            override fun onResponse(call: Call<CreateEntityResponse> , response: Response<CreateEntityResponse>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.winmax4Entity?.isCreated.equals("true")) {

                        Constants.shortToast(mContext !! , "Your address updated successfully : ==> " + response.body()?.winmax4Entity?.entityCode !!)

                        PrefManager(mContext).setValue("entityCode" , response.body()?.winmax4Entity?.entityCode !!)
                        getEntityByPhoneNumber(view_edit_text_phone_num.text.toString())
                        setButtonEnable()
                    } else {
                        Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                    }
                }
            }

            override fun onFailure(call: Call<CreateEntityResponse> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , getString(R.string.failure))
            }
        })
    }

    private fun validateDetails(): Boolean {

        val phone = view_edit_text_phone_num.text.toString().trim()
        // val entityname = etentity.text.toString().trim()
        val entityname = sp_selec.text.toString().trim()
        val address = etAddress.text.toString().trim()
        val postalCode = etPostalCode.text.toString().trim()
        val location = etLocality.text.toString().trim()
        val remarks = etRemarks.text.toString().trim()
        val etDesignation = etDesignation.text.toString().trim()

        when {
            phone.isEmpty() -> {
                Toast.makeText(this , "Please enter Phone Number!" , Toast.LENGTH_LONG).show()
            }
            phone.length > 15 -> {
                Toast.makeText(this , "Please enter valid Phone Number!" , Toast.LENGTH_LONG).show()
            }
            entityname.isEmpty() -> {
                Toast.makeText(this , "Please enter Entity Name!" , Toast.LENGTH_LONG).show()
            }
            address.isEmpty() -> {
                Toast.makeText(this , "Please enter address!" , Toast.LENGTH_LONG).show()
            }

            /* Singleton.instance.showAddress.size > 0 &&
                     postalCode.isEmpty() -> {
                 Toast.makeText(this , "Please enter postal code!" , Toast.LENGTH_LONG).show()
             }
             Singleton.instance.showAddress.size > 0 &&
                     location.isEmpty() -> {
                 Toast.makeText(this , "Please enter location!" , Toast.LENGTH_LONG).show()
             }*/
            Singleton.instance.showAddress.size > 0 &&
                    etDesignation.isEmpty() -> {
                Toast.makeText(this , "Please enter designation!" , Toast.LENGTH_LONG).show()
            }
            Singleton.instance.showAddress.size > 1 -> {
                if (Singleton.instance.showAddress.flatMap { listOf(it.designation) }.contains(etDesignation)) {
                    Toast.makeText(this , "Please enter different designation!" , Toast.LENGTH_LONG).show()
                } else {
                    return true
                }
            }
            else -> {
                return true
            }
        }

        return false
    }

    private fun createEntityAPI() {

        circle_load.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE , WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        val request = CreateEntity()
        if (Singleton.instance.showAddress.size == 0) {
            request.phone = view_edit_text_phone_num.text.toString().trim()
            //request.name = etentity.text.toString().trim()
            request.name = sp_selec.text.toString().trim()
            request.address = etAddress.text.toString().trim()
            request.zipCode = etPostalCode.text.toString().trim()
            request.location = etLocality.text.toString().trim()
            request.remarks = etRemarks.text.toString().trim()
            request.entityType = "Customer"
        } else {
            request.phone = Singleton.instance.showAddress[0].phone
            request.name = etentity.text.toString().trim()
            request.address = Singleton.instance.showAddress[0].address
            request.zipCode = Singleton.instance.showAddress[0].zipCode
            request.location = Singleton.instance.showAddress[0].location
            request.remarks = Singleton.instance.showAddress[0].remarks
            request.entityType = "Customer"

        }

        val createEntityCredential = Credential()
        createEntityCredential.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        createEntityCredential.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        createEntityCredential.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        createEntityCredential.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)

        request.credential = createEntityCredential

        val additionalAddress = ArrayList<AdditionalAddress>()

        when (Singleton.instance.showAddress.size) {
            0 -> {
            }
            1 -> {
                val addAddress = AdditionalAddress()
                addAddress.address = etAddress.text.toString().trim()
                addAddress.designation = etDesignation.text.toString().trim()
                addAddress.location = etLocality.text.toString().trim()
                addAddress.zipCode = etPostalCode.text.toString().trim()
                additionalAddress.add(addAddress)
            }
            else -> {
                for (i in 1 until Singleton.instance.showAddress.size) {
                    val addAddress = AdditionalAddress()
                    addAddress.address = Singleton.instance.showAddress.get(i).address
                    addAddress.designation = Singleton.instance.showAddress.get(i).designation
                    addAddress.location = Singleton.instance.showAddress.get(i).location
                    addAddress.zipCode = Singleton.instance.showAddress.get(i).zipCode
                    additionalAddress.add(addAddress)
                }
                val addAddress = AdditionalAddress()
                addAddress.address = etAddress.text.toString().trim()
                addAddress.designation = etDesignation.text.toString().trim()
                addAddress.location = etLocality.text.toString().trim()
                addAddress.zipCode = etPostalCode.text.toString().trim()
                additionalAddress.add(addAddress)
            }
        }
        request.additionalAddress = additionalAddress

        val call = ApiClient.getService().createEntity(request)
        call.enqueue(object : Callback<CreateEntityResponse> {
            override fun onResponse(call: Call<CreateEntityResponse> , response: Response<CreateEntityResponse>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.winmax4Entity?.isCreated.equals("true")) {
                        if (editAPI) {
                            Constants.shortToast(mContext !! , "Your additional address added successfully : ==> " + response.body()?.winmax4Entity?.entityCode !!)
                            editAPI = false
                        } else {
                            Constants.shortToast(mContext !! , "Your entity is created : ==> " + response.body()?.winmax4Entity?.entityCode !!)
                        }
                        PrefManager(mContext).setValue("entityCode" , response.body()?.winmax4Entity?.entityCode !!)
                        getEntityByPhoneNumber(view_edit_text_phone_num.text.toString())
                        //edittextDisable()
                        setButtonEnable()
                    } else {
                        Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                    }
                }
            }

            override fun onFailure(call: Call<CreateEntityResponse> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , getString(R.string.failure))
            }
        })
    }

    private fun edittextDisable() {
        etAddress.isEnabled = false
        etLocality.isEnabled = false
        etPostalCode.isEnabled = false
        etRemarks.isEnabled = false
        etDesignation.isEnabled = false
        btnCreateEntity.visibility = View.GONE
    }

    private fun createEntityDialog() {
        AlertDialog.Builder(this)
                .setTitle("Info")
                .setMessage("Your mobile number not exist, please create new entity!")
                .setPositiveButton("ok") { v , _ ->
                    isCreateEntity = true
                    rvAddress.visibility = View.GONE
                    cvAddress.visibility = View.VISIBLE
                    enableEdittext()
                    v.dismiss()
                }
                .show()
    }

    private fun getEntityDetail() {
        circle_load.visibility = View.VISIBLE

        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = EntityRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)

        val call = ApiClient.getService().getEntityDetail(request)
        call.enqueue(object : Callback<EntityDetail> {
            override fun onResponse(call: Call<EntityDetail> , response: Response<EntityDetail>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getGetEntitiesResult()?.getCode().equals("0")) {
                        for (i in 0 until response.body() !!.getGetEntitiesResult()?.getEntities()?.getEntity()?.size !!) {
                            val entity = Entity()
                            entity.code = response.body() !!.getGetEntitiesResult()?.getEntities()?.getEntity()?.get(i)?.getCode()
                            entity.entityType = response.body() !!.getGetEntitiesResult()?.getEntities()?.getEntity()?.get(i)?.getEntityType()
                            entity.name = response.body() !!.getGetEntitiesResult()?.getEntities()?.getEntity()?.get(i)?.getName()

                            entity.phone = response.body() !!.getGetEntitiesResult()?.getEntities()?.getEntity()?.get(i)?.getPhone()
                            entity.address = response.body() !!.getGetEntitiesResult()?.getEntities()?.getEntity()?.get(i)?.getAddress()
                            entity.zipCode = response.body() !!.getGetEntitiesResult()?.getEntities()?.getEntity()?.get(i)?.getZipCode()
                            entity.location = response.body() !!.getGetEntitiesResult()?.getEntities()?.getEntity()?.get(i)?.getLocation()
                            entityData.add(entity)
                        }

                        if (Singleton.instance.entityData.size > 0) {
                            Singleton.instance.entityData.clear()

                        }

                        /*if (entityData.size > 0) {
                            loadtoSpinner(entityData)
                        }*/
                        // Singleton.instance.entityData = entityData

                    } else {
                        Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                    }
                } else {
                    Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                }
            }

            override fun onFailure(call: Call<EntityDetail> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , getString(R.string.failure))
            }
        })
    }

    private fun getEntityDescriptionList() {
        circle_load.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = EntityRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)

        val call = ApiClient.getService().getEntityDescription(request)
        call.enqueue(object : Callback<ArrayList<GetEntityDescription>> {
            override fun onFailure(call: Call<ArrayList<GetEntityDescription>> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , getString(R.string.failure))
            }

            override fun onResponse(call: Call<ArrayList<GetEntityDescription>> , response: Response<ArrayList<GetEntityDescription>>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        Singleton.instance.entityDescription.clear()
                        Singleton.instance.entityDescription.addAll(response.body() !!)

                        if (Singleton.instance.entityDescription.size > 0) {
                            loadEntityDescription(Singleton.instance.entityDescription)
                        }
                    } else {
                        Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                    }

                } else {
                    Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                }
            }
        })
    }

    private fun getPhoneNumber(entityCode: Int) {
        circle_load.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = PhoneNumberRequest()
        request.companyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        request.userLogin = PrefManager(mContext).getStringValue(Constants.userNameConst)
        request.userPassword = PrefManager(mContext).getStringValue(Constants.passwordConst)
        request.webServiceURL = PrefManager(mContext).getStringValue(Constants.webUrlConst)
        request.EntityCode = entityCode

        val call = ApiClient.getService().getPhoneNumber(request)
        call.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String> , t: Throwable) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , getString(R.string.failure))
            }

            override fun onResponse(call: Call<String> , response: Response<String>) {
                circle_load.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.isNotEmpty() !!) {
                        view_edit_text_phone_num.setText(response.body().toString())
                        getEntityByPhoneNumber(response.body().toString())
                    } else {
                        //Constants.shortToast(mContext !! , response.message())
                        showDialog()
                    }
                }
            }
        })
    }

    private fun showDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Info")
        builder.setCancelable(false)
        builder.setMessage("Mobile number not mapped in the selected entity!")
        builder.setNegativeButton("ok") { dialog , which ->
            dialog.cancel()
        }
        builder.show()
    }

    private fun loadEntityDescription(entityDescription: ArrayList<GetEntityDescription>) {
        val spData: ArrayList<String> = ArrayList()
        for (i in 0 until entityDescription.size) {
            spData.add(entityDescription[i].entityName !!)
        }

        entityspData.clear()
        entityspData.addAll(spData)

        val arrayAdapter = ArrayAdapter(this , R.layout.spinner_item , entityspData)
        sp_selec.threshold = 1
        sp_selec.setAdapter(arrayAdapter)

        sp_selec.onItemClickListener = AdapterView.OnItemClickListener { _ , _ , position , _ ->
            val name = arrayAdapter.getItem(position)
            Singleton.instance.entityDescription.forEach {
                if (it.entityName?.equals(name) !!) {
                    val getEntityCode = it.code
                    PrefManager(mContext).setValue("entityCode" , getEntityCode.toString())
                    if (isAPICAll) {
                        isAPICAll = false
                        getPhoneNumber(getEntityCode !!)
                    }
                }
            }
        }
    }

    private fun loadtoSpinner(entityData: ArrayList<Entity>) {
        val spData: ArrayList<String> = ArrayList()
        for (i in 0 until entityData.size) {
            spData.add(entityData[i].name !!)
        }

        entityspData.clear()
        entityspData.addAll(spData)

        //entityspData.add(0 , "Choose Entity")

        val arrayAdapter = ArrayAdapter(this , R.layout.spinner_item , entityspData)
        sp_selec.threshold = 1
        sp_selec.setAdapter(arrayAdapter)

        sp_selec.onItemClickListener = AdapterView.OnItemClickListener { _ , _ , position , _ ->
            PrefManager(mContext).setValue("entityCode" , entityData[position].code)

            Log.e("entityCode" , "entityCode" + entityData[position].code)
            //view_edit_text_phone_num.setText(entityData[position].phone)
            etAddress.setText(entityData[position].address)
            etPostalCode.setText(entityData[position].zipCode)
            etLocality.setText(entityData[position].location)

            setButtonEnable()

            val name = arrayAdapter.getItem(position)
            entityData.forEach {
                if (it.name?.equals(name) !!) {
                    val getDriverCode = it.phone
                    view_edit_text_phone_num.setText(getDriverCode)
                    getEntityByPhoneNumber(getDriverCode.toString())
                }
            }
        }
    }
}