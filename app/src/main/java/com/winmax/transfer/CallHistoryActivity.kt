package com.winmax.transfer

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.winmax.transfer.call.CallLogAdapter
import com.winmax.transfer.call.CallLogUtils
import kotlinx.android.synthetic.main.activity_call_history.*

class CallHistoryActivity : AppCompatActivity() {

    private var adapter: CallLogAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call_history)

        if (getRuntimePermission()) {
            initComponents()
        }

        ivBack.setOnClickListener {
            finish()
        }
    }

    private fun getRuntimePermission(): Boolean {
        if (ContextCompat.checkSelfPermission(this , Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this , arrayOf(Manifest.permission.READ_CALL_LOG) , 123)
            return false
        }
        return true
    }

    private fun initComponents() {
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        listView.layoutManager = mLayoutManager
        listView.itemAnimator = DefaultItemAnimator()
        adapter = CallLogAdapter(this)
        listView.adapter = adapter
        loadData()
    }

    private fun loadData() {
        val callLogUtils: CallLogUtils = CallLogUtils.getInstance(this)
        adapter !!.addAllCallLog(callLogUtils.readCallLogs())
        adapter !!.notifyDataSetChanged()
    }

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<String> , grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode , permissions , grantResults)
        if (requestCode == 123) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initComponents()
            } else {
                AlertDialog.Builder(this)
                        .setMessage("The app needs these permissions to work, Exit?")
                        .setTitle("Permission Denied")
                        .setCancelable(false)
                        .setPositiveButton("Retry") { dialog , _ ->
                            dialog.dismiss()
                            getRuntimePermission()
                        }
                        .setNegativeButton("Exit App") { dialog , _ ->
                            dialog.dismiss()
                            finish()
                        }.show()
            }
        }
    }
}