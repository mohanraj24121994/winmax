package com.winmax.transfer.utils

interface IsDriverChecked {
    fun isDriverChecked(position: Int , isCheck: Boolean , driverName: String?)
}