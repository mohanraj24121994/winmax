package com.winmax.transfer.utils.imageShare

import android.graphics.Bitmap
import android.graphics.Matrix
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by CIPL0349 on 12/15/2017.
 */
object CommonUtils {

    fun getDateTime(): String {
        val timeStamp = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().time)
        println(timeStamp)
        return timeStamp
    }

    fun getResizedBitmap(bm: Bitmap? , newWidth: Int , newHeight: Int , rotate: Boolean): Bitmap {
        val width = bm?.width
        val height = bm?.height
        val scaleWidth = newWidth.toFloat() / width !!
        val scaleHeight = newHeight.toFloat() / height !!
        // CREATE A MATRIX FOR THE MANIPULATION
        val matrix = Matrix()
        if (rotate) {
            // Rotate
            matrix.postRotate(90f)
        }
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth , scaleHeight)

        // "RECREATE" THE NEW BITMAP
        val resizedBitmap = Bitmap.createBitmap(bm , 0 , 0 , width , height , matrix , false)
        bm.recycle()
        return resizedBitmap
    }
}