package com.winmax.transfer.utils

interface OnEditTextChanged {
    fun getTotalValue(percentage: Double , amount: Double )
}