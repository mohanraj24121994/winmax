package com.winmax.transfer.utils.imageShare

import android.content.Context
import android.graphics.Bitmap
import android.os.Environment
import com.winmax.transfer.BuildConfig
import com.winmax.transfer.R
import com.winmax.transfer.constant.Constants
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*


/**
 * Created by CIPL0349 on 1/2/2018.
 */
class ConvertBitmapToFile {

    private var mListener: IConvertBitmapToFile? = null
    private var mContext: Context? = null
    private var mBitmap: Bitmap? = null
    private var mCurrentUploadType: Int = -1
    private var mWidth: Double = 0.0
    private var mHeight: Double = 0.0
    private var mRotate: Boolean = false


    fun setConvertBitmapToFileListener(aListener: IConvertBitmapToFile, aBitmap: Bitmap, aContext: Context?, aCurrentUploadType: Int) {
        this.mListener = aListener
        this.mContext = aContext
        this.mBitmap = aBitmap
        this.mCurrentUploadType = aCurrentUploadType
        bitmapToFile()
    }

    private fun bitmapToFile(): File {
        //create a file to write bitmap data
        val fileName = getName(mCurrentUploadType)
        val file = File(mContext?.getExternalFilesDir(Environment.DIRECTORY_PICTURES), fileName)
        try {
            file.createNewFile()
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            return file
        }

        getSizes(mCurrentUploadType)

        //mBitmap = CommonUtils.getResizedBitmap(mBitmap, mWidth.toInt(), mHeight.toInt(), mRotate)

        val bos = ByteArrayOutputStream()
        mBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, bos)
        val bitmapData = bos.toByteArray()

        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(file)
            fos.write(bitmapData)
            fos.flush()
            fos.close()
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            mListener?.onFailure()
            return file
        } finally {
            try {
                fos!!.close()
                mBitmap = null
                mListener?.onSuccess(file)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return file
    }

    private fun getSizes(currentUploadType: Int) {
        when (currentUploadType) {
            Constants.signature -> {
              /*  this.mWidth = 560.0 //mBitmap?.width!!.toDouble()
                this.mHeight = 260.0 //mBitmap?.height!!.toDouble()*/
                this.mRotate = false
            }
            else -> {
           /*     this.mWidth = 816.0 + 40.0
                this.mHeight = 612.0 + 30.0*/
                this.mRotate = false
            }
        }
    }

    private fun getName(currentUploadType: Int): String {
        return when (currentUploadType) {
            Constants.signature -> {
                mContext?.getString(R.string.app_name) + UUID.randomUUID().toString().replace("[^a-zA-Z0-9\\:\\.\\/]".toRegex(), "_") + ".jpg"
            }
            else -> {
                mContext?.getString(R.string.app_name) + CommonUtils.getDateTime().replace("[^a-zA-Z0-9\\:\\.\\/]".toRegex(), "_") + ".jpg"
            }
        }
    }
}