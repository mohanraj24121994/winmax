package com.winmax.transfer.utils.modelprovider

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.winmax.transfer.model.getArticals.response.Articles
import com.winmax.transfer.search.itemdetail.ItemSearchDetailViewModel

class ItemSearchDetailViewModelFactory
(private val application: Application, private val articles: Articles):
        ViewModelProvider.Factory {


    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ItemSearchDetailViewModel::class.java)) {

            return ItemSearchDetailViewModel(application, articles) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
