package com.winmax.transfer.utils

interface LoadingIndicatorListener {

    fun onStarted()

    fun onSuccess()

    fun onFailure(message: String?)

}