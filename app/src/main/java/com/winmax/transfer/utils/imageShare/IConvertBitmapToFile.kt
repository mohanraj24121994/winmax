package com.winmax.transfer.utils.imageShare

import java.io.File

/**
 * Created by CIPL0349 on 1/2/2018.
 */
interface IConvertBitmapToFile {
    fun onSuccess(aFilePath: File)
    fun onFailure()
}