package com.winmax.transfer


import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.model.InvoiceListModel
import com.winmax.transfer.utils.OnEditTextChanged
import kotlinx.android.synthetic.main.invoice_list_items.view.*
import kotlinx.android.synthetic.main.invoice_list_items.view.item_code
import kotlinx.android.synthetic.main.invoice_list_items.view.item_product_name
import kotlinx.android.synthetic.main.invoice_list_items.view.item_qty
import kotlinx.android.synthetic.main.transfer_list_items.view.*


class InvoiceListAdapter(private var itemList: ArrayList<InvoiceListModel> , private var mContext: Context? ,
                         var onEditTextChanged: OnEditTextChanged) : RecyclerView.Adapter<InvoiceListAdapter.ViewHolder>() {

    var totalRefcashAmount: Double = 0.0
    var totalPercentage: Double = 0.0

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.invoice_list_items , parent , false)

        return ViewHolder(v , mContext)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        holder.bindItems(itemList , position)
    }

    inner class ViewHolder(itemView: View , private var mContext: Context?) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(itemListData: ArrayList<InvoiceListModel> , position: Int) {

            itemView.item_code.text = itemListData[position].itemCode
            itemView.item_product_name.text = itemListData[position].productName
            itemView.item_qty.setText(itemListData[position].quantity.toString())
            itemView.tvPrice.text = "€ " + itemListData[position].price.toString()
            itemView.tvTotal.text = "€ " + itemListData[position].total.toString()

            totalRefcashAmount += itemListData[position].total.toString().toDouble()

            totalPercentage = totalRefcashAmount / 100 * itemListData[position].percentage

            Log.e("percentage" , "percentage$totalPercentage")

            itemView.item_qty.setSelection(itemView.item_qty.text.toString().length)
            itemView.item_qty.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    if (s !!.isNotEmpty()) {

                        Singleton.instance.invoiceListSingleton[position].quantity = itemView.item_qty.text.toString().trim()

                        val total = (Singleton.instance.invoiceListSingleton[position].quantity.toString().toDouble() *
                                itemView.tvPrice.text.toString().replace("€ " , "").toDouble()).toString()
                        Singleton.instance.invoiceListSingleton[position].total = total

                        itemView.tvTotal.text = "€ " + Singleton.instance.invoiceListSingleton[position].total

                        val totalQuantity = Singleton.instance.invoiceListSingleton.map { it.total.toString().toDouble() }.sum()

                        val percentage = (total.toDouble() / 100 * itemListData[position].percentage)
                        Singleton.instance.invoiceListSingleton[position].percentage = percentage

                        val totalPercent = Singleton.instance.invoiceListSingleton.map { it.percentage.toString().toDouble() }.sum()

                        Log.e("percentage" , "percentage$totalPercent")

                        onEditTextChanged.getTotalValue(totalPercent , totalQuantity)

                    } else {
                        Toast.makeText(mContext , "Please enter valid quantity!" , Toast.LENGTH_LONG).show()
                        Singleton.instance.invoiceListSingleton[position].quantity = itemListData[position].quantity.toString()
                    }
                }

                override fun beforeTextChanged(s: CharSequence? , start: Int , count: Int , after: Int) {
                }

                override fun onTextChanged(s: CharSequence? , start: Int , before: Int , count: Int) {
                }
            })

            onEditTextChanged.getTotalValue(totalPercentage , totalRefcashAmount)
        }
    }
}



