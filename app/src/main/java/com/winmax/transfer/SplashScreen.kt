package com.winmax.transfer

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import kotlinx.android.synthetic.main.activity_splash_screen.*


class SplashScreen : AppCompatActivity() {

    private val SPLASH_TIME_OUT: Long = 3000
    var versionStr: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        try {
            val pInfo: PackageInfo = applicationContext.getPackageManager().getPackageInfo(packageName, 0)
            versionStr = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        versionStr = "Version $versionStr"
        tvVersion.text = versionStr

        PrefManager(applicationContext).setValue(Constants.versionConst, versionStr)
        val logIn = PrefManager(applicationContext).getBooleanValue("isLoggedIn")
        val isConfigured = PrefManager(applicationContext).getBooleanValue("isConfigured")

        Handler().postDelayed({

            if (logIn) {
                startActivity(Intent(this, SelectWareHouseScreen::class.java))
                finish()
            } else if(!isConfigured){
                startActivity(Intent(this, ConfigurationScreen::class.java))
                finish()
            } else{
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }


        }, SPLASH_TIME_OUT)
    }
}
