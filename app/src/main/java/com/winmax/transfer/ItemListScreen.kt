package com.winmax.transfer


import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider.getUriForFile
import androidx.recyclerview.widget.LinearLayoutManager
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.Singleton
import com.winmax.transfer.model.ItemListModel
import com.winmax.transfer.model.createArticle.request.ArticleDetail
import com.winmax.transfer.model.createArticle.request.CreateArticleRequest
import com.winmax.transfer.model.createArticle.request.Credential
import com.winmax.transfer.model.createArticle.request.DocumentDetail
import com.winmax.transfer.model.createArticle.response.CreateArticleResponse
import com.winmax.transfer.webservices.ApiClient
import kotlinx.android.synthetic.main.activity_item_list_screen.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ItemListScreen : AppCompatActivity() , View.OnClickListener {

    private val PERMISSION_REQUEST_CODE = 100

    private var listAdapter: ItemListAdapter? = null
    private var mContext: Context? = null
    private val itemList: ArrayList<ItemListModel> = ArrayList()
    private var artDetailList: ArrayList<ArticleDetail> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_list_screen)
        inItViews()

        requestPermission()
    }

    private fun inItViews() {
        val screenFrom = intent.extras?.getString(Constants.EXTRA_KEY_SCREEN_FOR)
        if (screenFrom.equals(getString(R.string.inventory))) {
            //view setup for inventory
            btnAddMore.visibility = View.GONE
            btnConfirm.visibility = View.GONE
            btnExport.visibility = View.VISIBLE
        } else {
            //view set up for transfer
            btnAddMore.visibility = View.VISIBLE
            btnConfirm.visibility = View.VISIBLE
            btnExport.visibility = View.GONE
        }

        mContext = this
        onClickListener()

        recyclerview !!.layoutManager = LinearLayoutManager(mContext)
        listAdapter = ItemListAdapter(mContext , addAllProducts()) { itemList , i , view ->
            when (view.id) {
                R.id.itemContainer -> {
                    Constants.shortToast(mContext !! , itemList[i].itemCode)
                    val to = Intent(mContext , ItemDetailActivity::class.java)
                    to.putExtra("select_item_code" , itemList[i].itemCode.toString())
                    startActivity(to)

                    overridePendingTransition(R.anim.enter , R.anim.exit)

                }
            }
        }
        recyclerview !!.adapter = listAdapter
        listAdapter !!.notifyDataSetChanged()
    }

    private fun onClickListener() {
        btnConfirm.setOnClickListener(this)
        ivLogout.setOnClickListener(this)
        ivBack.setOnClickListener(this)
        btnAddMore.setOnClickListener(this)
        btnExport.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnConfirm -> {
                if (Constants.internetCheck(mContext !!)) {
                    if (itemList.size > 0) {
                        callApi()
                    } else {

                    }
                } else {
                    Constants.shortToast(mContext !! , getString(R.string.check_internet))
                }


            }
            R.id.ivLogout -> {
                /* PrefManager(mContext).setBooleanValue("isLoggedIn" , false)
                 PrefManager(mContext).isFirstTimeLaunch = false
                 //PrefManager(mContext).clear()
                 val to = Intent(this , MainActivity::class.java)
                 startActivity(to);
                 overridePendingTransition(R.anim.enter , R.anim.exit)
                 finish()*/

                alertInventoryList()
            }
            R.id.ivBack -> {
                finish()
            }

            R.id.btnAddMore -> {
                val to = Intent(this , AddProductDetailScreen::class.java)
                startActivity(to);
                overridePendingTransition(R.anim.enter , R.anim.exit)
                finish()
            }
            R.id.btnExport -> {
                if (checkPermission()) {
                    getExportData()
                } else {
                    requestPermission()
                }
            }
        }
    }

    private fun alertInventoryList() {
        val alertBox = AlertDialog.Builder(this)
                .setTitle("Info")
                .setMessage("Do you want to initialize the inventory list?")
                .setPositiveButton("Yes") { _ , _ ->
                    Singleton.instance.articleListSingletonInventory.clear()
                    val to = Intent(this , SelectWareHouseScreen::class.java)
                    to.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(to);
                    overridePendingTransition(R.anim.enter , R.anim.exit)
                    finish()
                }
                .setNegativeButton("No") { _ , _ ->
                    val to = Intent(this , SelectWareHouseScreen::class.java)
                    to.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(to);
                    overridePendingTransition(R.anim.enter , R.anim.exit)
                    finish()
                }
                .show()
    }

    private fun getExportData() {
        try {
            //create & get file directory
            val uri = writeDataToFile()
            //share file
            shareTextData(uri)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun writeDataToFile(): Uri {
        //create file name
        val sdf = SimpleDateFormat("yyyy-MM-dd_HH-mm")
        val mTimeStamp = sdf.format(Date())
        val fileName = "export_$mTimeStamp.txt"
        //create directory
        val extStorageDirectory = Environment.getExternalStorageDirectory().toString()
        //val filePath = File(extStorageDirectory , "Winmax")
        val filePath = File(this.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) , "Winmax")
        if (! filePath.exists()) {
            filePath.mkdirs()
        }
        //write data to the file
        val newFile = File(filePath , fileName)
        newFile.createNewFile()
        val contentUri: Uri = getUriForFile(this , "$packageName.fileprovider" , newFile);
        var data = ""
        for (item in itemList) {
            data = data + item.itemCode + "," + item.quantity + "\n"
        }
        var bos: BufferedOutputStream? = null
        try {
            val fos = FileOutputStream(newFile);
            bos = BufferedOutputStream(fos);
            bos.write(data.toByteArray());
        } finally {
            if (bos != null) {
                try {
                    bos.flush();
                    bos.close();
                } catch (e: Exception) {
                }
            }
        }
        return contentUri
    }

    private fun shareTextData(uri: Uri) {
        try {
            // val mUri = FileProvider.getUriForFile(this, "$packageName.fileprovider", file.absoluteFile)
            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/*"
            sharingIntent.putExtra(Intent.EXTRA_STREAM , uri)
            startActivity(Intent.createChooser(sharingIntent , "Export file with"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun callApi() {
        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        if (itemList.isNotEmpty()) {

            val createArticleRequest = CreateArticleRequest()
            val credential = Credential()
            val documentDetail = DocumentDetail()

            for (i in 0 until Singleton.instance.createDocModel.size) {
                credential.setCompanyCode(Singleton.instance.createDocModel[i].getCredential()?.getCompanyCode())
                credential.setUserLogin(Singleton.instance.createDocModel[i].getCredential()?.getUserLogin())
                credential.setUserPassword(Singleton.instance.createDocModel[i].getCredential()?.getUserPassword())
                credential.setWebServiceURL(Singleton.instance.createDocModel[i].getCredential()?.getWebServiceURL())

                createArticleRequest.setCredential(credential)

                documentDetail.setSourceWarehouseCode(Singleton.instance.createDocModel[i].getDocumentDetail()?.getSourceWarehouseCode())
                documentDetail.setTargetWarehouseCode(Singleton.instance.createDocModel[i].getDocumentDetail()?.getTargetWarehouseCode())
                documentDetail.setDocumentTypeCode(Singleton.instance.createDocModel[i].getDocumentDetail()?.getDocumentTypeCode())
                documentDetail.setEntityCode(Singleton.instance.createDocModel[i].getDocumentDetail()?.getEntityCode())
                //documentDetail.setEntityCode("1")

                createArticleRequest.setDocumentDetail(documentDetail)


            }

            val artDetailList: ArrayList<ArticleDetail> = ArrayList()

            for (i in 0 until Singleton.instance.articleListSingleton.size) {
                val artDetails = ArticleDetail()
                artDetails.articleCode = Singleton.instance.articleListSingleton[i].articleCode
                artDetails.articleDesignation = Singleton.instance.articleListSingleton[i].articleDesignation
                artDetails.quantity = Singleton.instance.articleListSingleton[i].quantity
                artDetailList.add(artDetails)


            }
            createArticleRequest.setArticleDetail(artDetailList)

//            for (i in 0 until itemList.size) {
//                var artDetails = ArticleDetail()
//                artDetails.articleCode = itemList[i].itemCode
//                artDetails.articleDesignation = itemList[i].productName
//                artDetails.quantity = itemList[i].quantity.toString()
//                artDetailList.add(artDetails)
//            }
//
//            request.articleDetail = artDetailList

            val call = ApiClient.getService().createDocApi(createArticleRequest)
            call.enqueue(object : Callback<CreateArticleResponse> {

                override fun onResponse(call: Call<CreateArticleResponse> , response: Response<CreateArticleResponse>) {
                    circle_view.visibility = View.GONE
                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    if (response.isSuccessful) {
                        if (response.body()?.winmax4Document?.isCreated.equals("true")) {
                            if (Singleton.instance.articleListSingleton.size > 0) {
                                Singleton.instance.articleListSingleton.clear()
                                itemList.clear()
                            }
                            listAdapter = ItemListAdapter(mContext , addAllProducts()) { itemList , i , view ->
                                //when (view.id) {

                                //}
                            }
                            recyclerview !!.adapter = listAdapter
                            listAdapter !!.notifyDataSetChanged()
                            showSuccessDialog(response.body()?.winmax4Document?.header?.documentNumber.toString())
                        } else {
                            Constants.shortToast(mContext !! , getString(R.string.something_went_wrong))
                        }
                    }
                }

                override fun onFailure(call: Call<CreateArticleResponse> , t: Throwable) {
                    circle_view.visibility = View.GONE
                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    Constants.shortToast(mContext !! , "Failure")
                }
            })

        }

    }

    fun addAllProducts(): ArrayList<ItemListModel> {
        val screenFrom = intent.extras?.getString(Constants.EXTRA_KEY_SCREEN_FOR)
        if (screenFrom.equals(getString(R.string.inventory))) {
            for (i in 0 until Singleton.instance.articleListSingletonInventory.size)
                itemList.add(ItemListModel(Singleton.instance.articleListSingletonInventory[i].articleCode !! ,
                        Singleton.instance.articleListSingletonInventory[i].articleDesignation !! ,
                        Singleton.instance.articleListSingletonInventory[i].quantity !!))
        } else {
            for (i in 0 until Singleton.instance.articleListSingleton.size)
                itemList.add(ItemListModel(Singleton.instance.articleListSingleton[i].articleCode !! ,
                        Singleton.instance.articleListSingleton[i].articleDesignation !! ,
                        Singleton.instance.articleListSingleton[i].quantity !!.toInt()))
        }
        return itemList
    }


    private fun showSuccessDialog(docNumber: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.doc_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val btnDone = dialog.findViewById(R.id.btnDone) as AppCompatButton
        val docNumberTv = dialog.findViewById(R.id.tvDoc_number) as AppCompatTextView
        if (docNumber != null) {
            docNumberTv.text = docNumber
        }

//        body.text = title
//        val yesBtn = dialog .findViewById(R.id.yesBtn) as Button
//        val noBtn = dialog .findViewById(R.id.noBtn) as TextView
        btnDone.setOnClickListener {
            dialog.dismiss()
            Singleton.instance.articleListSingleton.clear()
            val to = Intent(this , SelectWareHouseScreen::class.java)
            startActivity(to);
            overridePendingTransition(R.anim.enter , R.anim.exit)
            finish()
        }
        dialog.show()


    }

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<String> , grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("value" , "Permission Granted, Now you can use local drive .")
            } else {
                Log.e("value" , "Permission Denied, You cannot use local drive .")
            }
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this , arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE) , PERMISSION_REQUEST_CODE)
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this , Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }
}
