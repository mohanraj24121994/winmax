package com.winmax.transfer

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.model.authenticateUser.request.AuthenticateUser
import com.winmax.transfer.model.authenticateUser.request.Credential
import com.winmax.transfer.model.authenticateUser.request.Request
import com.winmax.transfer.model.authenticateUser.response.AuthResponse
import com.winmax.transfer.model.authenticateUser.response.AuthenticateUserResult
import com.winmax.transfer.model.createArticle.request.CreateArticleRequest
import com.winmax.transfer.webservices.ApiClient
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() , View.OnClickListener {

    private var mContext: Context? = null
    private var mCompanyCode: String? = ""
    private var mWebUrl: String? = ""

    private val REQUEST_LOCATION_PERMISSION = 786

    val REQUEST_CODE_FOREGROUND = 1544
    val REQUEST_CODE_BACKGROUND = 1545

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mContext = this
        onClickListener()

        mCompanyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        mWebUrl = PrefManager(mContext).getStringValue(Constants.webUrlConst)

        setVersion()
    }

    override fun onResume() {
        super.onResume()
        mCompanyCode = PrefManager(mContext).getStringValue(Constants.companyCodeConst)
        mWebUrl = PrefManager(mContext).getStringValue(Constants.webUrlConst)
    }

    private fun setVersion() {
        tvVersion.text = PrefManager(applicationContext).getStringValue(Constants.versionConst)
    }

    private fun onClickListener() {
        btnLogin.setOnClickListener(this)
        tvWebConfiguration.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnLogin -> {
                if (Constants.internetCheck(mContext !!)) {
                    callLoginMethod(etUserName.text.toString().trim() , etPassword.text.toString().trim())
                } else {
                    Constants.shortToast(mContext !! , getString(R.string.check_internet))
                }
            }
            R.id.tvWebConfiguration -> {
                PrefManager(mContext).setBooleanValue("isConfigured" , false)
                val to = Intent(this , ConfigurationScreen::class.java)
                startActivity(to);
                overridePendingTransition(R.anim.enter , R.anim.exit)
                finish()
            }
        }
    }

    private fun callLoginMethod(userNameStr: String , pswStr: String) {
        if (userNameStr.isEmpty() || userNameStr.isNullOrBlank()) {
            Constants.shortToast(mContext !! , getString(R.string.enter_username))
        } else if (pswStr.isEmpty() || pswStr.isNullOrBlank()) {
            Constants.shortToast(mContext !! , getString(R.string.enter_password))
        } else {
            mCompanyCode?.let { callLoginApi(it , userNameStr , pswStr , mWebUrl) }
        }
    }

    private fun callLoginApi(mCompanyCode: String , userNameStr: String , pswStr: String , mWebUrl: String?) {
        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val request = Request()

        val credential = Credential()
        credential.setCompanyCode(mCompanyCode)
        credential.setUserLogin(PrefManager(mContext).getStringValue(Constants.userNameConst))
        credential.setUserPassword(PrefManager(mContext).getStringValue(Constants.passwordConst))
        credential.WebServiceURL = mWebUrl

        val authUser = AuthenticateUser()
        authUser.setAuthenticationUser(userNameStr)
        authUser.setAuthenticationPassword(pswStr)
        authUser.setOptionsToCheckAccess("")

        request.setAuthenticateUser(authUser)
        request.setCredential(credential)

        val call = ApiClient.getService().authenticateUser(request)
        call.enqueue(object : Callback<AuthResponse> {
            override fun onResponse(call: Call<AuthResponse> , response: Response<AuthResponse>) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    if (response.body()?.getAuthenticateUserResult()?.getCode().equals("0")) {
                        Constants.shortToast(mContext !! , "Login successfully")
                        PrefManager(mContext).isFirstTimeLaunch = true
                        PrefManager(mContext).setBooleanValue("isLoggedIn" , true)
                        PrefManager(mContext).setValue("username" , response.body()?.getAuthenticateUserResult()?.getUserAccount()?.getUserName())
                        PrefManager(mContext).setValue("designation" , response.body()?.getAuthenticateUserResult()?.getUserAccount()?.getUserGroupDesignation())
                        PrefManager(mContext).setValue("getDriverCode" , response.body()?.getAuthenticateUserResult()?.getUserAccount()?.getUserLogin())

                        saveData(mCompanyCode , response.body()?.getAuthenticateUserResult()?.getUserAccount()?.getUserLogin() !! , pswStr , response.body()?.getAuthenticateUserResult() , mWebUrl)

                    } else if (response.body()?.getAuthenticateUserResult()?.getCode().equals("3")) {
                        Constants.shortToast(mContext !! , "Please enter valid Username or Password")
                    } else {
                        Constants.shortToast(mContext !! , response.body()?.getAuthenticateUserResult()?.getMessage().toString())
                    }
                } else {
                    Constants.shortToast(mContext !! , "Please enter valid Username or Password")
                }
            }

            override fun onFailure(call: Call<AuthResponse> , t: Throwable) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Constants.shortToast(mContext !! , getString(R.string.something_went_wrong_try))
            }
        })
    }

    private fun saveData(mCompanyCode: String , userNameStr: String , pswStr: String , authenticateUserResult: AuthenticateUserResult? , mWebUrl: String?) {
        val createArticleRequest = CreateArticleRequest()
        val credential = com.winmax.transfer.model.createArticle.request.Credential()
        credential.setCompanyCode(mCompanyCode)
        credential.setUserLogin(userNameStr)
        credential.setUserPassword(pswStr)
        credential.setWebServiceURL(mWebUrl)
        createArticleRequest.setCredential(credential)

        PrefManager(mContext).setValue(Constants.companyCodeConst , mCompanyCode)
        PrefManager(mContext).setValue(Constants.userNameAuthConst , userNameStr)
        PrefManager(mContext).setValue(Constants.passwordAuthConst , pswStr)
        PrefManager(mContext).setValue(Constants.webUrlConst , mWebUrl)

        val to = Intent(mContext , SelectWareHouseScreen::class.java)
        startActivity(to)
        overridePendingTransition(R.anim.enter , R.anim.exit);
        finish()
    }
}
