package com.winmax.transfer

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatCheckBox
import com.crashlytics.android.Crashlytics
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.winmax.transfer.constant.Constants
import com.winmax.transfer.constant.Constants.adminPassword
import com.winmax.transfer.constant.Constants.companyCodeConst
import com.winmax.transfer.constant.Constants.passwordConst
import com.winmax.transfer.constant.Constants.pdfURL
import com.winmax.transfer.constant.Constants.userNameConst
import com.winmax.transfer.constant.Constants.webUrlConst
import com.winmax.transfer.constant.PrefManager
import com.winmax.transfer.model.authenticateUser.request.AuthenticateUser
import com.winmax.transfer.model.authenticateUser.request.Credential
import com.winmax.transfer.model.authenticateUser.request.Request
import com.winmax.transfer.model.webserviceConnection.WebTestResponse.WebTestResponse
import com.winmax.transfer.webservices.ApiClient
import kotlinx.android.synthetic.main.activity_configuration_screen.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ConfigurationScreen : AppCompatActivity() , View.OnClickListener {

    private var mContext: Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configuration_screen)
        mContext = this

        setVersion()

        onClickListener()

        //Crashlytics.getInstance().crash()
        Crashlytics.log("ConfigurationScreen")
        val isAvailable = PrefManager(mContext).getStringValue(adminPassword)
        if (isAvailable != null && isAvailable.isNotEmpty()) {
            askPasswordDialog()
        }

        val company = PrefManager(mContext).getStringValue(companyCodeConst)
        val username = PrefManager(mContext).getStringValue(userNameConst)
        val password = PrefManager(mContext).getStringValue(passwordConst)

        if (! company.isNullOrEmpty() && ! password.isNullOrEmpty() && ! username.isNullOrEmpty()) {
            etCompanyCode.setText(company)
            etPassword.setText(password)
            etUserName.setText(username)
        }

        val url = PrefManager(mContext).getStringValue(webUrlConst)

        if (! url.isNullOrEmpty()) {
            tvUrl.setText(url)
        } else {
            // tvUrl.setText("http://online.restuspos.com/Winmax4/webservices/generic.asmx")
            tvUrl.setText("http://winmax4.restuspos.com/webservices/generic.asmx")
        }

        visibleWareHouse()

        /*FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this) { instanceIdResult ->
            val newToken = instanceIdResult.token
            Log.e("newToken" ,"newToken"+ newToken)
        }*/

       FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.e( "token Failed",""+ task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            val token = task.result
            Log.e("newToken" ,"==>"+ token)
        })
    }

    private fun setVersion() {
        tvVersion.text = PrefManager(applicationContext).getStringValue(Constants.versionConst)
    }

    private fun visibleWareHouse() {

        val isTransfer = PrefManager(applicationContext).getBooleanValue("isTransfer")
        rb_transfer.isChecked = isTransfer

        val isOrder = PrefManager(applicationContext).getBooleanValue("isOrder")
        rb_order.isChecked = isOrder

        val isItemSearch = PrefManager(applicationContext).getBooleanValue("isItemSearch")
        rb_item_search.isChecked = isItemSearch

        val isPayment = PrefManager(applicationContext).getBooleanValue("isPayment")
        rb_payment.isChecked = isPayment

        val isInventory = PrefManager(applicationContext).getBooleanValue("isInventory")
        rb_inventory.isChecked = isInventory

        val isTracking = PrefManager(applicationContext).getBooleanValue("isTracking")
        rb_tracking.isChecked = isTracking
    }

    private fun askPasswordDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.ask_password_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val btnDone = dialog.findViewById(R.id.btnDone) as AppCompatButton
        val etPasswordDoc = dialog.findViewById(R.id.etPasswordAsk) as TextInputEditText
        val passwordAdminStr = etPasswordDoc.text.toString().trim()

        btnDone.setOnClickListener {
            if (etPasswordDoc.text.toString().trim().isEmpty() || etPasswordDoc.text.toString().trim().isNullOrBlank()) {
                Constants.shortToast(mContext !! , "Please enter password!!")
            } else if (etPasswordDoc.text.toString().trim().equals(PrefManager(mContext).getStringValue(adminPassword))) {
                dialog.dismiss()
            } else {
                Constants.shortToast(mContext !! , "You have entered wrong password!")

            }
        }
        dialog.show()
    }

    private fun onClickListener() {
        btnConfirm.setOnClickListener(this)
        btnCancel.setOnClickListener(this)
        outlinedButton.setOnClickListener(this)

        btnConfirm.isEnabled = false
        btnConfirm.alpha = 0.5f

        etTerminal.setText("1")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnConfirm -> {
                //startActivity(Intent(this,MainActivity::class.java))
                PrefManager(mContext).setBooleanValue("isConfigured" , true)
                val to = Intent(this , MainActivity::class.java)
                //overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right)
                overridePendingTransition(R.anim.left_to_right , R.anim.right_to_left)
                //to.putExtra("CompanyCode", etCompanyCode.text.toString().trim())
                startActivity(to)
                finish()

            }
            R.id.btnCancel -> {
                finish()
            }
            R.id.outlinedButton -> {
                if (Constants.internetCheck(mContext !!)) {
                    if (etCompanyCode.text.toString().trim().isEmpty() || etCompanyCode.text.toString().trim().isNullOrBlank()) {
                        Constants.shortToast(mContext !! , "Please enter company code!!")
                    } else if (etUserName.text.toString().trim().isEmpty() || etUserName.text.toString().trim().isNullOrBlank()) {
                        Constants.shortToast(mContext !! , getString(R.string.enter_username))
                    } else if (etPassword.text.toString().trim().isEmpty() || etUserName.text.toString().trim().isNullOrBlank()) {
                        Constants.shortToast(mContext !! , getString(R.string.enter_password))
                    } else {
                        callWebServiceApi(etCompanyCode.text.toString().trim() , etTerminal.text.toString().trim() , etUserName.text.toString().trim() , etPassword.text.toString().trim())
                    }

                } else {
                    Constants.shortToast(mContext !! , getString(R.string.check_internet))
                }
            }
        }
        if (view is AppCompatCheckBox) {
            val checked: Boolean = view.isChecked
            when (view.id) {
                R.id.rb_transfer -> {
                    if (checked) {
                        PrefManager(mContext).setBooleanValue("isTransfer" , true)
                    } else {
                        PrefManager(mContext).setBooleanValue("isTransfer" , false)
                    }
                }
                R.id.rb_order -> {
                    if (checked) {
                        PrefManager(mContext).setBooleanValue("isOrder" , true)
                    } else {
                        PrefManager(mContext).setBooleanValue("isOrder" , false)
                    }
                }
                R.id.rb_item_search -> {
                    if (checked) {
                        PrefManager(mContext).setBooleanValue("isItemSearch" , true)
                    } else {
                        PrefManager(mContext).setBooleanValue("isItemSearch" , false)
                    }
                }
                R.id.rb_payment -> {
                    if (checked) {
                        PrefManager(mContext).setBooleanValue("isPayment" , true)
                    } else {
                        PrefManager(mContext).setBooleanValue("isPayment" , false)
                    }
                }
                R.id.rb_inventory -> {
                    if (checked) {
                        PrefManager(mContext).setBooleanValue("isInventory" , true)
                    } else {
                        PrefManager(mContext).setBooleanValue("isInventory" , false)
                    }
                }
                R.id.rb_tracking -> {
                    if (checked) {
                        PrefManager(mContext).setBooleanValue("isTracking" , true)
                    } else {
                        PrefManager(mContext).setBooleanValue("isTracking" , false)
                    }
                }
            }
        }
    }

    private fun callWebServiceApi(companyCode: String , terminalStr: String , userNameStr: String , passwordStr: String) {
        circle_view.visibility = View.VISIBLE
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE ,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        val request = Request()
        val credential = Credential()
        val authUser = AuthenticateUser()
        credential.setCompanyCode(companyCode)
        credential.setUserLogin(userNameStr)
        credential.setUserPassword(passwordStr)
        credential.WebServiceURL = tvUrl.text.toString().trim()

        authUser.setAuthenticationUser(userNameStr)
        authUser.setAuthenticationPassword(passwordStr)
        authUser.setOptionsToCheckAccess("")

        request.setAuthenticateUser(authUser)
        request.setCredential(credential)

        val call = ApiClient.getService().webServiceConnectionTest(request)
        call.enqueue(object : Callback<WebTestResponse> {
            override fun onResponse(call: Call<WebTestResponse> , response: Response<WebTestResponse>) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                if (response.isSuccessful) {
                    btnConfirm.isEnabled = true
                    btnConfirm.alpha = 1.0f
                    if (response.body()?.authenticateUserResult?.getCode().equals("0")) {
                        Constants.shortToast(mContext !! , response.body()?.authenticateUserResult?.getMessage().toString())

                        PrefManager(mContext).setValue(companyCodeConst , companyCode)
                        PrefManager(mContext).setValue(userNameConst , userNameStr)
                        PrefManager(mContext).setValue(passwordConst , passwordStr)
                        PrefManager(mContext).setValue(webUrlConst , tvUrl.text.toString().trim())
                        showDialogAdmin()

                        val url = PrefManager(mContext).getStringValue(webUrlConst)
                        val currentstring = url.split(".com")
                        val getURL = currentstring[0] + ".com"

                        PrefManager(mContext).setValue(pdfURL , getURL)

                    } else if (response.body()?.authenticateUserResult?.getCode().equals("3")) {
                        Constants.shortToast(mContext !! , response.body()?.authenticateUserResult?.getMessage().toString())
                    } else {
                        Constants.shortToast(mContext !! , response.body()?.authenticateUserResult?.getMessage().toString())
                    }
                } else {
                    Constants.shortToast(mContext !! , response.body()?.authenticateUserResult?.getMessage().toString())
                }
            }

            override fun onFailure(call: Call<WebTestResponse> , t: Throwable) {
                circle_view.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                // Constants.shortToast(mContext !! , t.message !!)
            }
        })
    }

    private fun showDialogAdmin() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.admin_dialog)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val btnDone = dialog.findViewById(R.id.btnDone) as AppCompatButton
        val etPasswordDoc = dialog.findViewById(R.id.etPasswordDoc) as TextInputEditText
        val etConfPasswordDoc = dialog.findViewById(R.id.etConfPas) as TextInputEditText
        val adminPsw = etPasswordDoc.text.toString().trim()
        val adminConfPsw = etConfPasswordDoc.text.toString().trim()

        btnDone.setOnClickListener {

            if (etPasswordDoc.text.toString().trim().isEmpty() || etPasswordDoc.text.toString().trim().isNullOrBlank()) {
                Constants.shortToast(mContext !! , "Please enter password!!")
            } else if (etConfPasswordDoc.text.toString().trim().isEmpty() || etConfPasswordDoc.text.toString().trim().isNullOrBlank()) {
                Constants.shortToast(mContext !! , "Please enter conform password")
            } else if (etPasswordDoc.text.toString().trim().equals(etConfPasswordDoc.text.toString().trim())) {

                PrefManager(mContext).setValue(adminPassword , etConfPasswordDoc.text.toString().trim())
                Constants.shortToast(mContext !! , "Password is saved")
                dialog.dismiss()
            } else {
                Constants.shortToast(mContext !! , "Password and Conform password mismatch!!")
            }

            btnConfirm.isEnabled = true
        }
        dialog.show()
    }
}
