package com.winmax.transfer.webservices


import android.app.Activity
import android.content.Context
import com.google.gson.GsonBuilder
import okhttp3.Cookie
import okhttp3.CookieJar
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyManagementException
import java.security.KeyStore
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.cert.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.*

class ApiClient {
    companion object {

        val BASE_URL = "https://winmax4api.colanonline.net/api/v1/"
        private var retrofit: Retrofit? = null
        var client = OkHttpClient()
        private const val CONNECT_TIMEOUT = 15
        private const val READ_TIMEOUT = 60
        private const val WRITE_TIMEOUT = 60
        //var internetConnectionListener: InternetConnectionListener? = null

        fun getService(): APInterface {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            /*val tmf = TrustManagerFactory.getInstance(
                    TrustManagerFactory.getDefaultAlgorithm())
            tmf.init(null as KeyStore?)

            val trustManagers = tmf.trustManagers
            val origTrustmanager = trustManagers[0] as X509TrustManager

            val wrappedTrustManagers = arrayOf<TrustManager>(
                    object : X509TrustManager {
                        override fun getAcceptedIssuers(): Array<X509Certificate?>? {
                            return origTrustmanager.acceptedIssuers
                        }

                        override fun checkClientTrusted(certs: Array<X509Certificate?>? , authType: String?) {
                            origTrustmanager.checkClientTrusted(certs , authType)
                        }

                        override fun checkServerTrusted(certs: Array<X509Certificate?>? , authType: String?) {
                            try {
                                origTrustmanager.checkServerTrusted(certs , authType)
                            } catch (e: CertificateExpiredException) {
                            }
                        }
                    }
            )

            val sc = SSLContext.getInstance("TLS")
            sc.init(null , wrappedTrustManagers , null)
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)*/

            client = OkHttpClient.Builder()
                    .connectTimeout(CONNECT_TIMEOUT.toLong() , TimeUnit.SECONDS)
                    .writeTimeout(WRITE_TIMEOUT.toLong() , TimeUnit.SECONDS)
                    .readTimeout(READ_TIMEOUT.toLong() , TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .cookieJar(UvCookieJar())
                    .hostnameVerifier { _ , _ -> true }
                    .build()

           /* getTrustAllHostsSSLSocketFactory().let {
                client.sslSocketFactory()
            }*/

            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory
                            .create(GsonBuilder().setLenient().create()))
                    /* .addCallAdapterFactory(RxJava2CallAdapterFactory.create())*/
                    .client(client).build()

            return retrofit.create(APInterface::class.java)
        }

/*        fun getSslContextForCertificateFile(context: Context , fileName: String): SSLContext {
            try {
                val keyStore = getKeyStore(context , fileName)
                val sslContext = SSLContext.getInstance("SSL")
                val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
                trustManagerFactory.init(keyStore)
                sslContext.init(null , trustManagerFactory.trustManagers , SecureRandom())
                return sslContext
            } catch (e: Exception) {
                val msg = "Error during creating SslContext for certificate from assets"
                e.printStackTrace()
                throw RuntimeException(msg)
            }
        }

        private fun getKeyStore(context: Context , fileName: String): KeyStore? {
            var keyStore: KeyStore? = null
            try {
                val assetManager = context.assets
                val cf = CertificateFactory.getInstance("X.509")
                val caInput = assetManager.open(fileName)
                val ca: Certificate
                try {
                    ca = cf.generateCertificate(caInput)
                } finally {
                    caInput.close()
                }

                val keyStoreType = KeyStore.getDefaultType()
                keyStore = KeyStore.getInstance(keyStoreType)
                keyStore !!.load(null , null)
                keyStore.setCertificateEntry("ca" , ca)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return keyStore
        }

        fun getTrustAllHostsSSLSocketFactory(): SSLSocketFactory? {
            try {
                // Create a trust manager that does not validate certificate chains
                val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {

                    override fun getAcceptedIssuers(): Array<X509Certificate> {
                        return arrayOf()
                    }

                    @Throws(CertificateException::class)
                    override fun checkClientTrusted(chain: Array<X509Certificate> , authType: String) {
                    }

                    @Throws(CertificateException::class)
                    override fun checkServerTrusted(chain: Array<X509Certificate> , authType: String) {
                    }
                })

                // Install the all-trusting trust manager
                val sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null , trustAllCerts , SecureRandom())
                // Create an ssl socket factory with our all-trusting manager

                return sslContext.socketFactory
            } catch (e: KeyManagementException) {
                e.printStackTrace()
                return null
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
                return null
            }

        }*/

//        fun getClientWithIntercepter(): Retrofit {
//            val interceptor = HttpLoggingInterceptor()
//            interceptor.level = HttpLoggingInterceptor.Level.BODY
//            client = OkHttpClient.Builder().connectTimeout(240, TimeUnit.SECONDS)
//                    .readTimeout(240, TimeUnit.SECONDS)
//                    .addInterceptor(interceptor)
//                    //  .addNetworkInterceptor(StethoInterceptor())// do comment while release or do config in gradle
//                    .addInterceptor(AuthenticationInterceptor()).build()
//            retrofit = Retrofit.Builder()
//                    .baseUrl(BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .client(client)
//                    .build()
//
//            return retrofit!!
//        }

        /*   fun setSSLFactoryForClient(client: OkHttpClient): OkHttpClient {
               try {
                   // Create a trust manager that does not validate certificate chains
                   val trustAllCerts: Array<TrustManager> = arrayOf(
                           object : X509TrustManager {
                               override fun checkClientTrusted(chain: Array<out X509Certificate>? , authType: String?) {
                               }

                               override fun checkServerTrusted(chain: Array<out X509Certificate>? , authType: String?) {
                               }

                               @Throws(CertificateException::class)
                               override fun getAcceptedIssuers(): Array<X509Certificate>? {
                                   return null
                               }
                           }
                   )

                   // Install the all-trusting trust manager
                   val sslContext: SSLContext = SSLContext.getInstance("SSL")
                   sslContext.init(null , trustAllCerts , SecureRandom())
                   client.sslSocketFactory()
                   client.hostnameVerifier()
               } catch (e: Exception) {
                   throw RuntimeException(e)
               }
               return client
           }*/
    }

    private class UvCookieJar : CookieJar {

        private val cookies = mutableListOf<Cookie>()

        override fun saveFromResponse(url: HttpUrl , cookieList: List<Cookie>) {
            cookies.clear()
            cookies.addAll(cookieList)
        }

        override fun loadForRequest(url: HttpUrl): List<Cookie> =
                cookies
    }
}