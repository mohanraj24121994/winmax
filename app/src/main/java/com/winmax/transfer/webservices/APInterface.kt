package com.winmax.transfer.webservices

import com.winmax.transfer.constant.Constants.GetWareHouses
import com.winmax.transfer.locationupdate.LocationUpdateRequest
import com.winmax.transfer.model.GetWarehouseData
import com.winmax.transfer.model.LogoutRequest
import com.winmax.transfer.model.authenticateUser.request.Request
import com.winmax.transfer.model.authenticateUser.response.AuthResponse
import com.winmax.transfer.model.balance.GetBalanceAmount
import com.winmax.transfer.model.createArticle.request.CreateArticleRequest
import com.winmax.transfer.model.createArticle.response.CreateArticleResponse
import com.winmax.transfer.model.createentity.CreateEntity
import com.winmax.transfer.model.createentity.response.CreateEntityResponse
import com.winmax.transfer.model.family.FamilyResponse
import com.winmax.transfer.model.getArticals.response.ArticleResponse
import com.winmax.transfer.model.getItemCode.GetArticlesResponse
import com.winmax.transfer.model.getShippingTypesResult.GetDriverList
import com.winmax.transfer.model.getentitiesbyphone.GetEntitiesByPhone
import com.winmax.transfer.model.getentitiesbyphone.request.GetEntitiesByPhoneRequest
import com.winmax.transfer.model.getentitydescription.GetEntityDescription
import com.winmax.transfer.model.getentitydescription.PhoneNumberRequest
import com.winmax.transfer.model.itemCode.ItemCodeList
import com.winmax.transfer.model.payment.EntityDetail
import com.winmax.transfer.model.payment.entitycoderequest.EntityCodeRequest
import com.winmax.transfer.model.payment.entitycoderesponse.EntityCodeDetail
import com.winmax.transfer.model.payment.entityrequest.EntityRequest
import com.winmax.transfer.model.paymentresult.PaymentResultResponse
import com.winmax.transfer.model.paymentresult.paymentrequest.CredentialRequest
import com.winmax.transfer.model.tracking.TrackingListRequest
import com.winmax.transfer.model.tracking.TrackingListResponse
import com.winmax.transfer.model.tracking.TrackingUpdateRequest
import com.winmax.transfer.model.tracking.TrackingUpdateResponse
import com.winmax.transfer.model.webserviceConnection.WebTestRequest
import com.winmax.transfer.model.webserviceConnection.WebTestResponse.WebTestResponse
import retrofit2.Call
import retrofit2.http.*

interface APInterface {
    @Headers("Content-Type: application/json")
    @POST(GetWareHouses)
    fun getGetWareHouses(@Body requestData: WebTestRequest): Call<GetWarehouseData>

    @Headers("Content-Type: application/json")
    @POST("AuthenticateUser")
    fun authenticateUser(@Body requestData: Request): Call<AuthResponse>

    @Headers("Content-Type: application/json")
    @POST("WebServiceConnnectionTest")
    fun webServiceConnectionTest(@Body requestData: Request): Call<WebTestResponse>

    @Headers("Content-Type: application/json")
    @POST("GetArticles")
    fun getArticle(@Query("ArticleCode") ArticleCode: String , @Body requestData: WebTestRequest): Call<ArticleResponse>

    @Headers("Content-Type: application/json")
    @POST("GetFamilyDesignation")
    fun GetFamilyDesignation(@Query("familyCode") familyCode: String , @Body requestData: WebTestRequest): Call<FamilyResponse>

    @Headers("Content-Type: application/json")
    @POST("CreateDocument")
    fun createDocApi(@Body requestData: CreateArticleRequest): Call<CreateArticleResponse>

    @Headers("Content-Type: application/json")
    @POST("GetEntities")
    fun getEntityDetail(@Body requestData: EntityRequest): Call<EntityDetail>

    @Headers("Content-Type: application/json")
    @POST("GetEntitiesbycode")
    fun getEntityCodeDetail(@Body requestData: EntityCodeRequest): Call<EntityCodeDetail>

    @Headers("Content-Type: application/json")
    @POST("Paydocuments")
    fun paymentDetail(@Body credentialRequest: CredentialRequest): Call<PaymentResultResponse>

    @Headers("Content-Type: application/json")
    @POST("GetEntitiesCurrentAccount")
    fun getBalanceAmount(@Body requestData: EntityCodeRequest): Call<GetBalanceAmount>

    @Headers("Content-Type: application/json")
    @POST("GetEntitiesByPhone")
    fun getEntityByPhoneNumber(@Body requestData: GetEntitiesByPhoneRequest): Call<GetEntitiesByPhone>

    @Headers("Content-Type: application/json")
    @POST("CreateEntities")
    fun createEntity(@Body createEntity: CreateEntity): Call<CreateEntityResponse>

    @Headers("Content-Type: application/json")
    @POST("GetOrderList")
    fun getTrackingOrderList(@Body trackingListRequest: TrackingListRequest): Call<ArrayList<TrackingListResponse>>

    @Headers("Content-Type: application/json")
    @POST("UpdateOrderState")
    fun updateOrderState(@Body trackingListRequest: TrackingUpdateRequest): Call<TrackingUpdateResponse>

    @Headers("Content-Type: application/json")
    @POST("Getshippingtypes")
    fun getDriverList(@Body requestData: WebTestRequest): Call<GetDriverList>

    @Headers("Content-Type: application/json")
    @POST("GetArticles")
    fun getArticleAPI(@Body requestData: WebTestRequest): Call<GetArticlesResponse>

    @Headers("Content-Type: application/json")
    @POST("GetArticleList")
    fun getItemCode(@Query("Designation") Designation: String , @Body requestData: WebTestRequest): Call<ArrayList<ItemCodeList>>

    @Headers("Content-Type: application/json")
    @POST("GetEntityName")
    fun getEntityDescription(@Body requestData: EntityRequest): Call<ArrayList<GetEntityDescription>>

    @Headers("Content-Type: application/json")
    @POST("GetPhonenumber")
    fun getPhoneNumber(@Body requestData: PhoneNumberRequest): Call<String>

    @Headers("Content-Type: application/json")
    @POST("LogoutDriver")
    fun logoutAPI(@Body requestData: LogoutRequest): Call<String>

    @Headers("Content-Type: application/json")
    @POST("LocationUpdate")
    fun locationUpdate(@Body requestData: LocationUpdateRequest): Call<String>
}