package com.winmax.transfer.call;

import android.app.Activity;
import android.content.Intent;
import android.provider.CallLog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.winmax.transfer.InvoiceActivity;
import com.winmax.transfer.R;
import com.winmax.transfer.databinding.ListRowBinding;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CallLogAdapter extends RecyclerView.Adapter<CallLogAdapter.CallLogViewHolder>{

    ArrayList<CallLogInfo> callLogInfoArrayList;
    Activity context;

    public CallLogAdapter(Activity context){
        callLogInfoArrayList = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public CallLogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListRowBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_row,parent,false);
        return new CallLogViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CallLogViewHolder holder, int position) {
        holder.bind(callLogInfoArrayList.get(position));
    }

    public void addAllCallLog(ArrayList<CallLogInfo> list){
        callLogInfoArrayList.clear();
        callLogInfoArrayList.addAll(list);
    }

    @Override
    public int getItemCount() {
        return callLogInfoArrayList.size();
    }

    class CallLogViewHolder extends RecyclerView.ViewHolder{
        ListRowBinding itemBinding;
        public CallLogViewHolder(ListRowBinding binding) {
            super(binding.getRoot());
            itemBinding = binding;
        }

        public void bind(final CallLogInfo callLog){

            if (Integer.parseInt(callLog.getCallType()) == CallLog.Calls.MISSED_TYPE) {
                itemBinding.textViewName.setTextColor(context.getColor(R.color.red));
            }

            if(TextUtils.isEmpty(callLog.getName()))
                itemBinding.textViewName.setText(callLog.getNumber());
            else
                itemBinding.textViewName.setText(callLog.getName());
            Date dateObj = new Date(callLog.getDate());
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy  hh:mm a");
            itemBinding.textViewCallNumber.setText(callLog.getNumber());
            itemBinding.textViewCallDate.setText(formatter.format(dateObj));

            itemBinding.getRoot().setOnClickListener(view -> {
                Intent intent = new Intent(context, InvoiceActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("PhoneNumber",callLog.getNumber());
                context.startActivity(intent);
                context.finish();
            });
        }
    }
}